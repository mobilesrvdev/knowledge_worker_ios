//
//  QIKeychain.m
//  QIFoundation
//
//  Created by Quintet on 01/08/2017.
//  Copyright © 2017 Polared. All rights reserved.
//

#import "QIKeychain.h"
#import "Valet.h"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wincomplete-implementation"

@implementation QIKeychain
{
    VALValet *valet;
}

+ (Class)class
{
    return [VALValet class];
}

+ (id)forwardingTargetForSelector:(SEL)aSelector
{
    return [VALValet class];
}

- (void)forwardInvocation:(NSInvocation *)anInvocation
{
    SEL selector = [anInvocation selector];
    
    if ([valet respondsToSelector:selector])
        [anInvocation invokeWithTarget:valet];
}

- (BOOL)respondsToSelector:(SEL)aSelector
{
    return [valet respondsToSelector:aSelector];
}

- (NSMethodSignature*)methodSignatureForSelector:(SEL)selector
{
    return [valet methodSignatureForSelector:selector];
}

- (nullable instancetype)initWithIdentifier:(nonnull NSString *)identifier accessibility:(QIAccessibility)accessibility
{
    valet = [[VALValet alloc] initWithIdentifier:identifier accessibility:(VALAccessibility)accessibility];
    
    return valet ? self : nil;
}

- (nullable instancetype)initWithSharedAccessGroupIdentifier:(nonnull NSString *)sharedAccessGroupIdentifier accessibility:(QIAccessibility)accessibility
{
    valet = [[VALValet alloc] initWithSharedAccessGroupIdentifier:sharedAccessGroupIdentifier accessibility:(VALAccessibility)accessibility];
    
    return valet ? self : nil;
}

- (void)dealloc
{
    valet = nil;
}

@end

#pragma clang diagnostic pop
