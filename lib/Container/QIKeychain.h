//
//  QIKeychain.h
//  QIFoundation
//
//  Created by Quintet on 01/08/2017.
//  Copyright © 2017 Polared. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, QIAccessibility) {
    /// Valet data can only be accessed while the device is unlocked. This attribute is recommended for data that only needs to be accessible while the application is in the foreground. Valet data with this attribute will migrate to a new device when using encrypted backups.
    QIAccessibilityWhenUnlocked = 1,
    /// Valet data can only be accessed once the device has been unlocked after a restart. This attribute is recommended for data that needs to be accessible by background applications. Valet data with this attribute will migrate to a new device when using encrypted backups.
    QIAccessibilityAfterFirstUnlock,
    /// Valet data can always be accessed regardless of the lock state of the device. This attribute is not recommended. Valet data with this attribute will migrate to a new device when using encrypted backups.
    QIAccessibilityAlways,
    
    /// Valet data can only be accessed while the device is unlocked. This class is only available if a passcode is set on the device. This is recommended for items that only need to be accessible while the application is in the foreground. Valet data with this attribute will never migrate to a new device, so these items will be missing after a backup is restored to a new device. No items can be stored in this class on devices without a passcode. Disabling the device passcode will cause all items in this class to be deleted.
    QIAccessibilityWhenPasscodeSetThisDeviceOnly NS_ENUM_AVAILABLE(10_10, 8_0),
    /// Valet data can only be accessed while the device is unlocked. This is recommended for data that only needs to be accessible while the application is in the foreground. Valet data with this attribute will never migrate to a new device, so these items will be missing after a backup is restored to a new device.
    QIAccessibilityWhenUnlockedThisDeviceOnly,
    /// Valet data can only be accessed once the device has been unlocked after a restart. This is recommended for items that need to be accessible by background applications. Valet data with this attribute will never migrate to a new device, so these items will be missing after a backup is restored to a new device.
    QIAccessibilityAfterFirstUnlockThisDeviceOnly,
    /// Valet data can always be accessed regardless of the lock state of the device. This option is not recommended. Valet data with this attribute will never migrate to a new device, so these items will be missing after a backup is restored to a new device.
    QIAccessibilityAlwaysThisDeviceOnly,
};

@interface QIKeychain : NSProxy

- (nonnull NSString *)identifier;
- (BOOL)sharedAcrossApplications;
- (QIAccessibility)accessibility;

/// Creates a Valet that reads/writes keychain elements with the desired accessibility.
/// @see VALAccessibility
- (nullable instancetype)initWithIdentifier:(nonnull NSString *)identifier accessibility:(QIAccessibility)accessibility;

/// Creates a Valet that reads/writes keychain elements that can be shared across applications written by the same development team.
/// @param sharedAccessGroupIdentifier This must correspond with the value for keychain-access-groups in your Entitlements file.
/// @see VALAccessibility
- (nullable instancetype)initWithSharedAccessGroupIdentifier:(nonnull NSString *)sharedAccessGroupIdentifier accessibility:(QIAccessibility)accessibility;

/// @return YES if the keychain is accessible for reading and writing, NO otherwise.
/// @note Determined by writing a value to the keychain and then reading it back out.
- (BOOL)canAccessKeychain;

/// @param value An NSData value to be inserted into the keychain.
/// @return NO if the keychain is not accessible.
- (BOOL)setObject:(nonnull NSData *)value forKey:(nonnull NSString *)key;
/// @return The data currently stored in the keychain for the provided key. Returns nil if no object exists in the keychain for the specified key, or if the keychain is inaccessible.
- (nullable NSData *)objectForKey:(nonnull NSString *)key;

/// @param string An NSString value to store in the keychain for the provided key.
/// @return NO if the keychain is not accessible.
- (BOOL)setString:(nonnull NSString *)string forKey:(nonnull NSString *)key;
/// @return The string currently stored in the keychain for the provided key. Returns nil if no string exists in the keychain for the specified key, or if the keychain is inaccessible.
- (nullable NSString *)stringForKey:(nonnull NSString *)key;

/// @param key The key to look up in the keychain.
/// @return YES if a value has been set for the given key, NO otherwise.
- (BOOL)containsObjectForKey:(nonnull NSString *)key;
/// @return The set of all (NSString) keys currently stored in this Valet instance.
- (nonnull NSSet *)allKeys;

/// Removes a key/object pair from the keychain.
/// @return NO if the keychain is not accessible.
- (BOOL)removeObjectForKey:(nonnull NSString *)key;
/// Removes all key/object pairs accessible by this Valet instance from the keychain.
/// @return NO if the keychain is not accessible.
- (BOOL)removeAllObjects;

@end
