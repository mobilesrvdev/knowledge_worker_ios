//
//  QIContainer.h
//  QIFoundation
//
//  Created by Quintet on 24/07/2017.
//  Copyright © 2017 Polared. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "QIKeychain.h"
#import "QIUserDefaults.h"

extern NSString * _Nonnull const QIContainerUserInfoDidSetNotification;

@interface QIContainer : NSObject

@property (nonatomic, strong) NSDictionary *launchOptions;
@property (nonatomic, strong, setter=setUserInfo:) NSDictionary *userInfo;

+ (instancetype)sharedContainer;

+ (QIKeychain *)sharedKeychain;
+ (QIUserDefaults *)sharedUserDefaults;

@end

