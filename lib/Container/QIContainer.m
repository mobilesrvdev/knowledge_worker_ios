//
//  QIContainer.m
//  QIFoundation
//
//  Created by Quintet on 24/07/2017.
//  Copyright © 2017 Polared. All rights reserved.
//

#import "QIContainer.h"

NSString * const kQIKeychainIdentifier = @"com.polared.container";
NSString * const kQISharedAccessGroupIdentifier = @"com.polared.shared.container";

NSString * const QIContainerUserInfoDidSetNotification = @"com.polared.container.userinfo.did.set.notification";

@interface QIContainer ()
@property (nonatomic, strong) QIKeychain *keychain;
@property (nonatomic, strong) QIUserDefaults *userDefaults;

@end

@implementation QIContainer

+ (instancetype)sharedContainer
{
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init
{
    if (self = [super init])
    {
        _keychain = [[QIKeychain alloc] initWithIdentifier:kQIKeychainIdentifier accessibility:QIAccessibilityAlways];
        _userDefaults = [QIUserDefaults standardUserDefaults];
    }
    return self;
}

+ (QIKeychain *)sharedKeychain
{
    return [[QIContainer sharedContainer] keychain];
}

+ (QIUserDefaults *)sharedUserDefaults
{
    return [[QIContainer sharedContainer] userDefaults];
}

- (void)setUserInfo:(NSDictionary *)userInfo
{
    _userInfo = userInfo;
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:QIContainerUserInfoDidSetNotification object:userInfo];
    });
}

@end
