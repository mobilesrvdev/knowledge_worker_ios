//
//  QIUserDefaults.h
//  QIFoundation
//
//  Created by Quintet on 01/08/2017.
//  Copyright © 2017 Polared. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QIUserDefaults : NSProxy

+ (instancetype)standardUserDefaults;

- (void)synchronize;

@end
