//
//  QIUserDefaults.m
//  QIFoundation
//
//  Created by Quintet on 01/08/2017.
//  Copyright © 2017 Polared. All rights reserved.
//

#import "QIUserDefaults.h"
#import "GVUserDefaults.h"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wincomplete-implementation"

NSString * const kQIPropertiesGroupName  = @"group.com.polared.properies";
NSString * const kQIPropertiesPrefix = @"QIProperties_";
NSString * const kQIUserDefaultNameInPlist = @"QIProperties";

@interface GVUserDefaults (QI_UserDefault_PRIVATE)

- (NSUserDefaults *)userDefaults;

- (NSString *)suiteName;
- (NSDictionary *)setupDefaults;
- (NSString *)transformKey:(NSString *)key;

@end

@implementation QIUserDefaults
{
    GVUserDefaults *userDefaults;
}

+ (Class)class
{
    return [GVUserDefaults class];
}

+ (id)forwardingTargetForSelector:(SEL)aSelector
{
    return [GVUserDefaults class];
}

- (void)forwardInvocation:(NSInvocation *)anInvocation
{
    SEL selector = [anInvocation selector];
    if ([userDefaults respondsToSelector:selector])
        [anInvocation invokeWithTarget:userDefaults];
}

- (BOOL)respondsToSelector:(SEL)aSelector
{
    return [userDefaults respondsToSelector:aSelector];
}

- (NSMethodSignature *)methodSignatureForSelector:(SEL)selector
{
    return [userDefaults methodSignatureForSelector:selector];
}

+ (instancetype)standardUserDefaults
{
    QIUserDefaults *_userDefaults = [[QIUserDefaults alloc] init];
    return _userDefaults;
}

- (nullable instancetype)init
{
    userDefaults = [GVUserDefaults standardUserDefaultsWithClass:[self class]];
    
    return userDefaults ? self : nil;
}

- (NSString *)suiteName {
    return kQIPropertiesGroupName;
}

- (NSDictionary *)setupDefaults {
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:kQIUserDefaultNameInPlist];
}

- (NSString *)transformKey:(NSString *)key {
    key = [key stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[key substringToIndex:1] uppercaseString]];
    NSString *transformedKey = [NSString stringWithFormat:@"%@%@", kQIPropertiesPrefix, key];
    return transformedKey;
}

- (void)synchronize
{
    [[userDefaults userDefaults] synchronize];
}

- (void)dealloc
{
    userDefaults = nil;
}

@end

#pragma clang diagnostic pop
