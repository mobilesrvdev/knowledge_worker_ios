//
//  QINetwork.m
//  QIFoundation
//
//  Created by Quintet on 27/07/2017.
//  Copyright © 2017 Polared. All rights reserved.
//

#import "QINetwork.h"
#import <SystemConfiguration/CaptiveNetwork.h>
#import "AFNetworking.h"

static NSString * const kQINetworkManagerBackgroundIdentifier = @"com.polared.network.background-id";
static NSString * const kQINetworkManagerContainerIdentifier = @"com.polared.network.container-id";
static const char* kQINetworkManagerProcessingIdentifier = "com.polared.network.process-queue";

static NSString * const kQINetworkRetryCountKey = @"count";
static NSString * const kQINetworkRequestCurrentlyKey = @"request";

NSString * const QINetworkErrorDomain = @"QINetworkError";
NSString * const QINetworkErrorUserInfoResponseKey = @"com.polared.network.error.userinfo.response";
NSString * const QINetworkErrorUserInfoResponseDataKey = @"com.polared.network.error.userinfo.response.data";
NSString * const QINetworkErrorUserInfoURLKey = @"com.polared.network.error.userinfo.url";

const NSUInteger     kQINetworkDefaultRetryCount        = 3;
const NSTimeInterval kQINetworkDefaultRetryInterval     = 2;
const NSTimeInterval kQINetworkDefaultTimeoutInterval   = 5;

static dispatch_queue_t ic_network_processing_queue() {
    static dispatch_queue_t ic_network_processing_queue;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        ic_network_processing_queue = dispatch_queue_create(kQINetworkManagerProcessingIdentifier, dispatch_queue_attr_make_with_qos_class(DISPATCH_QUEUE_CONCURRENT, QOS_CLASS_USER_INITIATED, QOS_MIN_RELATIVE_PRIORITY));
    });
    
    return ic_network_processing_queue;
}

@interface QINetwork ()
@property (readwrite, nonatomic, strong) AFNetworkReachabilityManager *reachabilityManager;
@property (nonatomic, strong) AFHTTPSessionManager *dataTaskSessionManager;
@property (nonatomic, strong) NSMutableDictionary *networkIdentifier;

@end

@implementation QINetwork

+ (NSURLSessionConfiguration *)defaultSessionConfiguration
{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    configuration.networkServiceType = NSURLNetworkServiceTypeDefault;
    configuration.allowsCellularAccess = YES;
    configuration.discretionary = NO;
    configuration.timeoutIntervalForRequest = kQINetworkDefaultTimeoutInterval;
    configuration.timeoutIntervalForResource = kQINetworkDefaultTimeoutInterval;
    
    return configuration;
}

+ (NSURLSessionConfiguration *)defaultBackgroundSessionConfiguration
{
    NSURLSessionConfiguration *configuration = nil;
    configuration = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:kQINetworkManagerBackgroundIdentifier];
    [configuration setSharedContainerIdentifier:kQINetworkManagerContainerIdentifier];
    
    configuration.networkServiceType = NSURLNetworkServiceTypeBackground;
    configuration.allowsCellularAccess = YES;
    configuration.discretionary = YES;
    configuration.timeoutIntervalForRequest = kQINetworkDefaultTimeoutInterval;
    configuration.timeoutIntervalForResource = kQINetworkDefaultTimeoutInterval;
    
    return configuration;
}

__attribute__((constructor))
static void initializeClass()
{
    [QINetwork manager];
}

+ (void)initialize
{
    if (self == [QINetwork class]) {
        initializeClass();
    }
}

+ (instancetype)manager
{
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

+ (instancetype)managerWithBaseURL:(NSURL *)baseURL pathBlock:(NSDictionary *(^) (void))serviceURLMapDictionaryBlock
{
    return [[[self class] manager] managerWithBaseURL:baseURL pathBlock:serviceURLMapDictionaryBlock];
}

- (instancetype)init
{
    if (self = [super init]) {
        
    }
    return self;
}

- (instancetype)managerWithBaseURL:(NSURL *)baseURL pathBlock:(NSDictionary *(^) (void))serviceURLMapDictionaryBlock
{
    if ([[self class] manager])
    {
        self->_baseURL = [baseURL copy];
        self->_serviceURLMapDictionaryBlock = [serviceURLMapDictionaryBlock copy];
        self.dataTaskSessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:_baseURL sessionConfiguration:[QINetwork defaultSessionConfiguration]];
        self.dataTaskSessionManager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
        self.dataTaskSessionManager.securityPolicy.allowInvalidCertificates = YES;
        self.dataTaskSessionManager.securityPolicy.validatesDomainName = NO;
        if ([_baseURL isKindOfClass:[NSURL class]] && [[_baseURL absoluteString] length]) {
            self.reachabilityManager = [AFNetworkReachabilityManager managerForDomain:[_baseURL host]];
        }
    }
    return self;
}

- (void)prepareForReuse
{
    self->_baseURL = nil;
    self->_serviceURLMapDictionaryBlock = nil;
    self.dataTaskSessionManager = nil;
    [self.reachabilityManager stopMonitoring];
    self.reachabilityManager = nil;
}

- (void)startMonitoringReachability
{
    [self.reachabilityManager startMonitoring];
}

#pragma mark - Suspend/Resume methods

- (void)suspend
{
    [[[self dataTaskSessionManager] tasks] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[NSURLSessionTask class]])
        {
            [(NSURLSessionTask *)obj suspend];
        }
    }];
}

- (void)resume
{
    [[[self dataTaskSessionManager] tasks] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[NSURLSessionTask class]])
        {
            [(NSURLSessionTask *)obj resume];
        }
    }];
}

- (void)cancel
{
    [[[self dataTaskSessionManager] tasks] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[NSURLSessionTask class]])
        {
            [(NSURLSessionTask *)obj cancel];
        }
    }];
}

#pragma mark - Status methods

- (BOOL)isReqeusting
{
    DDLogDebug(@"networkIdentifier : %lu || dataTaskSessionManager : %lu", (unsigned long)[[self networkIdentifier] count], (unsigned long)[[[self dataTaskSessionManager] tasks] count]);
    DDLogDebug(@"networkIdentifier : %@", [self networkIdentifier]);
    return [[self networkIdentifier] count] || [[[self dataTaskSessionManager] tasks] count];
}

#pragma mark - Block non-nil return

- (NSUInteger)retryCount
{
    return _retryCountBlock ? _retryCountBlock() : kQINetworkDefaultRetryCount;
}

- (NSTimeInterval)retryInterval
{
    return _retryIntervalBlock ? _retryIntervalBlock() : kQINetworkDefaultRetryInterval;
}

- (NSTimeInterval)timeoutInterval
{
    return _timeoutIntervalBlock ? _timeoutIntervalBlock() : kQINetworkDefaultTimeoutInterval;
}

- (NSDictionary *)serviceURLMapDictionary
{
    return _serviceURLMapDictionaryBlock ? _serviceURLMapDictionaryBlock() ?: @{} : @{};
}

- (BOOL)shouldSucceed:(NSDictionary *)response
{
    return _shouldSucceed ? _shouldSucceed(response) ?: NO : NO;
}

- (NSError *)generateErrorWithResponse:(NSDictionary *)response orError:(NSError *)error
{
    return _generateError ? _generateError(response, error) ?: [self unknownError] : [self unknownError];
}

- (NSError *)unknownError
{
    return [NSError errorWithDomain:QINetworkErrorDomain code:kQINetworkErrorNoDefinition userInfo:@{NSLocalizedDescriptionKey:@"There is no definition of this error"}];
}

#pragma mark - Processing methods

- (NSNumber *)networkTryCountForIdentifier:(NSString *)identifier
{
    NSNumber *__block count = nil;
    dispatch_sync(ic_network_processing_queue(), ^{
        if ([[self.networkIdentifier objectForKey:identifier] isKindOfClass:[NSDictionary class]])
        {
            count = [self.networkIdentifier objectForKey:identifier][kQINetworkRetryCountKey] ?: [NSNumber numberWithInteger:0];
        }
        else
        {
            self.networkIdentifier = self.networkIdentifier ?: [[NSMutableDictionary alloc] init];
            [self.networkIdentifier setObject:@{kQINetworkRetryCountKey:[NSNumber numberWithInteger:0], kQINetworkRequestCurrentlyKey: [NSNumber numberWithBool:NO]} forKey:identifier];
            count = [NSNumber numberWithInteger:0];
        }
    });
    
    return count;
}

- (void)removeNetworkStatusForIdentifier:(NSString *)identifier
{
    dispatch_barrier_async(ic_network_processing_queue(), ^{
        self.networkIdentifier = self.networkIdentifier ?: [[NSMutableDictionary alloc] init];
        [self.networkIdentifier removeObjectForKey:identifier];
    });
}

- (BOOL)isNetworkContainsIdentifier:(NSString *)identifier
{
    BOOL __block isContain = NO;
    dispatch_sync(ic_network_processing_queue(), ^{
        isContain = [self.networkIdentifier objectForKey:identifier] && [[self.networkIdentifier objectForKey:identifier][kQINetworkRequestCurrentlyKey] boolValue] ? YES : NO;
    });
    return isContain;
}

- (void)startNetworkIdentifier:(NSString *)identifier
{
    NSNumber *count = [NSNumber numberWithInteger:[[self networkTryCountForIdentifier:identifier] integerValue] + 1];
    dispatch_barrier_async(ic_network_processing_queue(), ^{
        self.networkIdentifier = self.networkIdentifier ?: [[NSMutableDictionary alloc] init];
        [self.networkIdentifier setObject:@{kQINetworkRetryCountKey:count, kQINetworkRequestCurrentlyKey:@YES} forKey:identifier];
    });
}

- (void)endNetworkIdentifier:(NSString *)identifier
{
    [self removeNetworkStatusForIdentifier:identifier];
}

- (void)retrySelector:(SEL)selector withArguments:(NSArray *)arguments identifier:(NSString *)identifier failureBlock:(void (^)(void))failureBlock
{
    NSNumber *count = [self networkTryCountForIdentifier:identifier];
    if ([count integerValue] < self.retryCount) {
        NSTimeInterval interval = powf([count floatValue], (float)self.retryInterval) ?: 0.1f;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(interval * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            dispatch_barrier_async(ic_network_processing_queue(), ^{
                self.networkIdentifier = self.networkIdentifier ?: [[NSMutableDictionary alloc] init];
                [self.networkIdentifier setObject:@{kQINetworkRetryCountKey:count, kQINetworkRequestCurrentlyKey:@NO} forKey:identifier];
            });
            
            [self invoke:selector arguments:arguments];
        });
    }
    else if (failureBlock)
    {
        failureBlock();
    }
}

- (void)invoke:(SEL)selector arguments:(NSArray *)arguments
{
    NSInvocation *inv = [NSInvocation invocationWithMethodSignature:[self  methodSignatureForSelector:selector]];
    [inv setSelector:selector];
    [inv setTarget:self];
    for (int i = 0; i < [arguments count]; i++)
    {
        id obj = [arguments objectAtIndex:i];
        if ([obj isKindOfClass:[NSNumber class]] && ![obj isKindOfClass:[NSDecimalNumber class]])
        {
            NSNumber *number = obj;
            CFNumberType numberType = CFNumberGetType((__bridge CFNumberRef)number);
            BOOL ok = NO;
            
            if (numberType == kCFNumberFloat32Type ||
                numberType == kCFNumberFloat64Type ||
                numberType == kCFNumberCGFloatType)
            {
                double value;
                ok = CFNumberGetValue((__bridge CFNumberRef)number, kCFNumberFloat64Type, &value);
                if (ok) {
                    [inv setArgument:&value atIndex:(i + 2)];
                }
            } else {
                SInt64 value;
                ok = CFNumberGetValue((__bridge CFNumberRef)number, kCFNumberSInt64Type, &value);
                if (ok) {
                    [inv setArgument:&value atIndex:(i + 2)];
                }
            }
        }
        else
        {
            [inv setArgument:&obj atIndex:(i + 2)];
        }
    }
    
    [inv invoke];
}

#pragma mark - Inner call

- (BOOL)authCheckWithURL:(NSString *)urlString response:(NSDictionary *)response orError:(NSError *)error
{
    if (self.authBlock) {
        return ![self.authChallengeExceptionPaths containsObject:urlString] && self.authChallengeBlock && self.authChallengeBlock(response, error);
    }
    return NO;
}

- (void)postReqestWithParameter:(NSDictionary *)param
                 selectorString:(NSString *)selectorString
                 concurrentMode:(QINetworkConcurrentMode)concurrentMode
                   successBlock:(void (^)(NSDictionary *))successBlock
                   failureBlock:(void (^)(NSError *))failureBlock
                authFailedBlock:(void(^)(void(^)(void)))authFailedBlock
{
    [self postReqestWithHeader:nil parameter:param selectorString:selectorString concurrentMode:concurrentMode successBlock:successBlock failureBlock:failureBlock authFailedBlock:authFailedBlock];
}

- (void)postReqestWithHeader:(NSDictionary *)header
                   parameter:(NSDictionary *)param
              selectorString:(NSString *)selectorString
              concurrentMode:(QINetworkConcurrentMode)concurrentMode
                successBlock:(void (^)(NSDictionary *))successBlock
                failureBlock:(void (^)(NSError *))failureBlock
             authFailedBlock:(void(^)(void(^)(void)))authFailedBlock
{
    if (!([self baseURL] && [self baseURL].scheme && [self baseURL].host)) {
        NSError *error = [NSError errorWithDomain:QINetworkErrorDomain code:kQINetworkErrorInvalidBaseURL userInfo:@{NSLocalizedDescriptionKey:@"The base URL is invalid."}];
        if (failureBlock) {
            failureBlock(error);
        }
        if (self.errorHandler) {
            self.errorHandler(error);
        }
        return ;
    }
    
    NSString *networkIdentifier = @"";
    switch (concurrentMode) {
        case QINetworkConcurrentModeByURL:
        {
            networkIdentifier = [self->_baseURL absoluteString] ?: networkIdentifier;
        }break;
        case QINetworkConcurrentModeByPath:
        {
            networkIdentifier = selectorString ?: networkIdentifier;
        }break;
        case QINetworkConcurrentModeByParameter:
        {
            networkIdentifier = [selectorString stringByAppendingString:[param description] ?: [header description] ?: @""] ?: networkIdentifier;
        }break;
        case QINetworkConcurrentModeAlways:
        {
            networkIdentifier = [[NSUUID UUID] UUIDString];
        }
        default:
            break;
    }
    [self endNetworkIdentifier:networkIdentifier];
    if (![self isNetworkContainsIdentifier:networkIdentifier])
    {
        AFHTTPSessionManager *manager = [self dataTaskSessionManager];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        manager.requestSerializer.timeoutInterval = [self timeoutInterval];
        
        NSDictionary *serviceURLMap = [self serviceURLMapDictionary];
        if ([serviceURLMap count] && [selectorString length])
        {
            NSString *urlString = [serviceURLMap valueForKey:selectorString];
            NSLog(@"urlString: %@", urlString);
            if ([urlString isKindOfClass:[NSString class]] && [urlString length] > 0)
            {
                DDLogVerbose(@"network request : %@ (%@)", urlString, param);
                [self startNetworkIdentifier:networkIdentifier];
                NSNumber *retryCount = [self networkTryCountForIdentifier:networkIdentifier];
                NSTimeInterval timeoutInterval = [retryCount integerValue] * self.timeoutInterval;
                NSTimeInterval retryInterval = powf([retryCount floatValue], (float)self.retryInterval) ?: 0.1f;
                
                __weak __typeof(self) weakSelf = self;
                void (^retryBlock)(NSError *error) = ^(NSError *error){
                    DDLogError(@"%d retrying....after %f seconds\n%@", (int)[retryCount integerValue], timeoutInterval, error);
                    
                    NSArray *arguments = @[(header?:@{}), (param?:@{}), selectorString, @(concurrentMode), successBlock?:^(NSDictionary *empty){}, failureBlock?:^(NSError *empty){}, authFailedBlock?:^(void(^empty)(void)){}];
                    [weakSelf retrySelector:_cmd withArguments:arguments identifier:networkIdentifier failureBlock:^{
                        DDLogError(@"retrying....also failed..!!!");
                        
                        [weakSelf endNetworkIdentifier:networkIdentifier];
                        
                        if (failureBlock) {
                            failureBlock(error);
                        }
                    }];
                };
                
                QIAuthCompleteHandler authCompleteHandler = ^(QINetworkAuthResult result) {
                    NSArray *arguments = @[(header?:@{}), (param?:@{}), selectorString, @(concurrentMode), successBlock?:^(NSDictionary *empty){}, failureBlock?:^(NSError *empty){}, authFailedBlock?:^(void(^empty)(void)){}];
                    [weakSelf endNetworkIdentifier:networkIdentifier];
                    if (result == QINetworkAuthResultSuccess) {
                        [weakSelf invoke:_cmd arguments:arguments];
                    } else {
                        if (authFailedBlock) {
                            QIAfterAuthBlock afterAuth = ^{
                                [weakSelf invoke:_cmd arguments:arguments];
                            };
                            authFailedBlock(afterAuth);
                        } else if (failureBlock) {
                            failureBlock([NSError errorWithDomain:QINetworkErrorDomain code:kQINetworkErrorAuthFailed userInfo:@{NSLocalizedDescriptionKey:@"Auth Failed!"}]);
                        }
                    }
                };
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(retryInterval * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [header enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
                        [manager.requestSerializer setValue:obj == [NSNull null] ? nil : obj forHTTPHeaderField:key];
                    }];
                    [manager POST:urlString parameters:([param count]?param:nil) headers:header progress:^(NSProgress * _Nonnull uploadProgress) {
                        
                    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                        __strong __typeof(weakSelf)strongSelf = weakSelf;
                        
                        DDLogVerbose(@"network response of %@ (%@) : \n%@", urlString, param, responseObject);
                        
                        if ([strongSelf authCheckWithURL:urlString response:responseObject orError:nil])
                        {
                            strongSelf.authBlock(authCompleteHandler);
                        }
                        else if ([strongSelf shouldSucceed:responseObject])
                        {
                            NSDictionary *resDict = responseObject;
                            int errorType = [resDict[@"errorType"] intValue];
                            DDLogVerbose(@"success! : %@, errorType: %d", [task description], errorType);
                            
                            if (errorType != 200) {
                                if (errorType == 0) {
                                    if (successBlock) {
                                        successBlock(responseObject);
                                        [strongSelf endNetworkIdentifier:networkIdentifier];
                                    }
                                    return;
                                }
                                NSError *error = [strongSelf generateErrorWithResponse:responseObject orError:nil];
                                
                                if (failureBlock) {
                                    failureBlock(error);
                                }
                            }
                            else if (successBlock) {
                                successBlock(responseObject);
                            }
                        }
                        else
                        {
                            NSError *error = [strongSelf generateErrorWithResponse:responseObject orError:nil];
                            
                            if (failureBlock) {
                                failureBlock(error);
                            }
                            
                            if (strongSelf.errorHandler) {
                                strongSelf.errorHandler(error);
                            }
                        }
                        
                        [strongSelf endNetworkIdentifier:networkIdentifier];
                    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                        __strong __typeof(weakSelf)strongSelf = weakSelf;
                        
                        NSMutableDictionary *errorUserInfo = nil;
                        
                        @try {
                            errorUserInfo = [NSMutableDictionary dictionaryWithDictionary:error.userInfo];
                            [errorUserInfo setValue:error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey] forKey:QINetworkErrorUserInfoURLKey];
                            [errorUserInfo setValue:error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] forKey:QINetworkErrorUserInfoResponseDataKey];
                            [errorUserInfo setValue:[NSJSONSerialization JSONObjectWithData:error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] options:kNilOptions error:NULL] forKey:QINetworkErrorUserInfoResponseKey];
                        } @catch (NSException *exception) {
                            DDLogError(@"error from parsing : %@", exception);
                        }
                        
                        NSError *generatedError = [strongSelf generateErrorWithResponse:nil orError:[NSError errorWithDomain:QINetworkErrorDomain code:error.code userInfo:[errorUserInfo copy]]];
                        
                        DDLogError(@"failure! : %@", [task description]);
                        switch (error.code)
                        {
                            case kCFHostErrorHostNotFound:
                            case kCFHostErrorUnknown:
                            case kCFErrorHTTPConnectionLost:
                            case kCFErrorHTTPProxyConnectionFailure:
                            case kCFErrorHTTPBadProxyCredentials:
                            case kCFErrorHTTPSProxyConnectionFailure:
                            case kCFStreamErrorHTTPSProxyFailureUnexpectedResponseToCONNECTMethod:
                            case kCFURLErrorBackgroundSessionInUseByAnotherProcess:
                            case kCFURLErrorBackgroundSessionWasDisconnected:
                            case kCFURLErrorUnknown:
                            case kCFURLErrorTimedOut:
                            case kCFURLErrorCannotFindHost:
                            case kCFURLErrorCannotConnectToHost:
                            case kCFURLErrorNetworkConnectionLost:
                            case kCFURLErrorDNSLookupFailed:
                            case kCFURLErrorNotConnectedToInternet:
                            case kCFURLErrorCallIsActive:
                            case kCFURLErrorDataNotAllowed:
                            {
                                retryBlock(generatedError);
                            }break;
                                
                            default:
                            {
                                if ([strongSelf authCheckWithURL:urlString response:nil orError:generatedError])
                                {
                                    strongSelf.authBlock(authCompleteHandler);
                                }
                                else
                                {
                                    DDLogError(@"no retry for network! %@", error.description);
                                    if ([error.description containsString:@"unacceptable content-type: text/html"]) {
                                        DDLogError(@"Auth Failed!");
                                        NSArray *arguments = @[(header?:@{}), (param?:@{}), selectorString, @(concurrentMode), successBlock?:^(NSDictionary *empty){}, failureBlock?:^(NSError *empty){}, authFailedBlock?:^(void(^empty)(void)){}];
                                        if (authFailedBlock) {
                                            QIAfterAuthBlock afterAuth = ^{
                                                [weakSelf invoke:_cmd arguments:arguments];
                                            };
                                            authFailedBlock(afterAuth);
                                        }
                                    }
                                    else if (failureBlock) {
                                        failureBlock(generatedError);
                                    }
                                    
                                    if (strongSelf.errorHandler) {
                                        strongSelf.errorHandler(generatedError);
                                    }
                                }
                                [strongSelf endNetworkIdentifier:networkIdentifier];
                            }break;
                        }
                    }];
//                    [manager POST:urlString parameters:([param count]?param:nil) progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
//                        __strong __typeof(weakSelf)strongSelf = weakSelf;
//
//                        DDLogVerbose(@"network response of %@ (%@) : \n%@", urlString, param, responseObject);
//
//                        if ([strongSelf authCheckWithURL:urlString response:responseObject orError:nil])
//                        {
//                            strongSelf.authBlock(authCompleteHandler);
//                        }
//                        else if ([strongSelf shouldSucceed:responseObject])
//                        {
//                            DDLogVerbose(@"success! : %@", [task description]);
//
//                            if (successBlock) {
//                                successBlock(responseObject);
//                            }
//                        }
//                        else
//                        {
//                            NSError *error = [strongSelf generateErrorWithResponse:responseObject orError:nil];
//
//                            if (failureBlock) {
//                                failureBlock(error);
//                            }
//
//                            if (strongSelf.errorHandler) {
//                                strongSelf.errorHandler(error);
//                            }
//                        }
//
//                        [strongSelf endNetworkIdentifier:networkIdentifier];
//                    } failure:^(NSURLSessionDataTask *task, NSError *error) {
//                        __strong __typeof(weakSelf)strongSelf = weakSelf;
//
//                        NSMutableDictionary *errorUserInfo = nil;
//
//                        @try {
//                            errorUserInfo = [NSMutableDictionary dictionaryWithDictionary:error.userInfo];
//                            [errorUserInfo setValue:error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey] forKey:QINetworkErrorUserInfoURLKey];
//                            [errorUserInfo setValue:error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] forKey:QINetworkErrorUserInfoResponseDataKey];
//                            [errorUserInfo setValue:[NSJSONSerialization JSONObjectWithData:error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] options:kNilOptions error:NULL] forKey:QINetworkErrorUserInfoResponseKey];
//                        } @catch (NSException *exception) {
//                            DDLogError(@"error from parsing : %@", exception);
//                        }
//
//                        NSError *generatedError = [strongSelf generateErrorWithResponse:nil orError:[NSError errorWithDomain:QINetworkErrorDomain code:error.code userInfo:[errorUserInfo copy]]];
//
//                        DDLogError(@"failure! : %@", [task description]);
//                        switch (error.code)
//                        {
//                            case kCFHostErrorHostNotFound:
//                            case kCFHostErrorUnknown:
//                            case kCFErrorHTTPConnectionLost:
//                            case kCFErrorHTTPProxyConnectionFailure:
//                            case kCFErrorHTTPBadProxyCredentials:
//                            case kCFErrorHTTPSProxyConnectionFailure:
//                            case kCFStreamErrorHTTPSProxyFailureUnexpectedResponseToCONNECTMethod:
//                            case kCFURLErrorBackgroundSessionInUseByAnotherProcess:
//                            case kCFURLErrorBackgroundSessionWasDisconnected:
//                            case kCFURLErrorUnknown:
//                            case kCFURLErrorTimedOut:
//                            case kCFURLErrorCannotFindHost:
//                            case kCFURLErrorCannotConnectToHost:
//                            case kCFURLErrorNetworkConnectionLost:
//                            case kCFURLErrorDNSLookupFailed:
//                            case kCFURLErrorNotConnectedToInternet:
//                            case kCFURLErrorCallIsActive:
//                            case kCFURLErrorDataNotAllowed:
//                            {
//                                retryBlock(generatedError);
//                            }break;
//
//                            default:
//                            {
//                                if ([strongSelf authCheckWithURL:urlString response:nil orError:generatedError])
//                                {
//                                    strongSelf.authBlock(authCompleteHandler);
//                                }
//                                else
//                                {
//                                    DDLogError(@"no retry for network!");
//                                    if (failureBlock) {
//                                        failureBlock(generatedError);
//                                    }
//                                    if (strongSelf.errorHandler) {
//                                        strongSelf.errorHandler(generatedError);
//                                    }
//                                }
//                                [strongSelf endNetworkIdentifier:networkIdentifier];
//                            }break;
//                        }
//                    }];
                });
            }
            else
            {
                NSAssert(NO, @"No Mapping URL. -[QINetwork setServiceURLMapDictionaryBlock:]\n %@", serviceURLMap);
            }
        }
        else
        {
            NSAssert(NO, @"No URL Defined. Check -[QINetwork setServiceURLMapDictionaryBlock:]\n %@ or No SEL %@", serviceURLMap, selectorString);
        }
    }
    else
    {
        DDLogWarn(@"[Network] Previous request has been made already.");
        //        failureBlock([NSError errorWithDomain:QINetworkErrorDomain code:kQINetworkErrorConcurrentCall userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"The network request is already in processing : %@", networkIdentifier]}]);
    }
}

- (void)reqestWithMethod:(NSString *)method
                  header:(NSDictionary *)header
               parameter:(NSDictionary *)param
          selectorString:(NSString *)selectorString
          concurrentMode:(QINetworkConcurrentMode)concurrentMode
            successBlock:(void (^)(NSDictionary *))successBlock
            failureBlock:(void (^)(NSError *))failureBlock
         authFailedBlock:(void (^)(void(^)(void)))authFailedBlock
           progressBlock:(void (^)(NSProgress *))progressBlock
       constructingBlock:(void (^)(id <QIMultipartFormData> formData))constructingBlock
{
    
}


@end
