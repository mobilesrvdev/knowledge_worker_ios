//
//  QINetwork.h
//  QIFoundation
//
//  Created by Quintet on 27/07/2017.
//  Copyright © 2017 Polared. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * _Nonnull const QINetworkErrorDomain;
extern NSString * _Nonnull const QINetworkErrorUserInfoResponseKey;
extern NSString * _Nonnull const QINetworkErrorUserInfoResponseDataKey;
extern NSString * _Nonnull const QINetworkErrorUserInfoURLKey;

@protocol QIMultipartFormData;

typedef NS_ENUM(NSInteger, QINetworkError){
    kQINetworkErrorNotCorrectLogin                 = 100500,
    kQINetworkErrorAuthFailed                      = 200410,
    kQINetworkErrorConcurrentCall                  = -1111,
    kQINetworkErrorInvalidBaseURL                  = -2222,
    kQINetworkErrorNoDefinition                    = -99999,
};

typedef NS_ENUM(NSInteger, QINetworkConcurrentMode) {
    QINetworkConcurrentModeByURL     = 0,
    QINetworkConcurrentModeByPath,
    QINetworkConcurrentModeByParameter,
    QINetworkConcurrentModeAlways,
};

typedef NS_ENUM(NSInteger, QINetworkAuthResult) {
    QINetworkAuthResultFailed     = 0,
    QINetworkAuthResultSuccess,
};

typedef NS_ENUM(NSInteger, QINetworkReachabilityStatus) {
    QINetworkReachabilityStatusUnknown          = -1,
    QINetworkReachabilityStatusNotReachable     = 0,
    QINetworkReachabilityStatusReachableViaWWAN = 1,
    QINetworkReachabilityStatusReachableViaWiFi = 2,
};

typedef void (^QIAuthCompleteHandler) (QINetworkAuthResult result);
typedef void (^QIAfterAuthBlock) (void);

@interface QINetwork : NSObject
@property (nullable, readonly, nonatomic, strong) NSURL *baseURL;
@property (nullable, readwrite, nonatomic, strong) NSArray <NSString *> *authChallengeExceptionPaths;
@property (nullable, readwrite, nonatomic, copy) BOOL (^authChallengeBlock)(NSDictionary *_Nullable response, NSError *_Nullable error);
@property (nullable, readwrite, nonatomic, copy) BOOL (^shouldSucceed)(NSDictionary *_Nullable response);
@property (nullable, readwrite, nonatomic, copy) NSError * _Nullable (^generateError)(NSDictionary *_Nullable response, NSError *_Nullable error);
@property (nullable, readwrite, nonatomic, copy) void (^errorHandler) (NSError *_Nonnull error);
@property (nullable, readwrite, nonatomic, copy) void (^authBlock)(QIAuthCompleteHandler _Nonnull completeHandler);
@property (nullable, readwrite, nonatomic, copy) NSUInteger (^retryCountBlock) (void);
@property (nullable, readwrite, nonatomic, copy) NSTimeInterval (^retryIntervalBlock) (void);
@property (nullable, readwrite, nonatomic, copy) NSTimeInterval (^timeoutIntervalBlock) (void);
@property (nullable, readwrite, nonatomic, copy) NSDictionary * _Nullable (^serviceURLMapDictionaryBlock) (void);
@property (nullable, readwrite, nonatomic, copy) void (^reachabilityStatusChangeBlock) (QINetworkReachabilityStatus status);

+ (nullable NSURLSessionConfiguration *)defaultSessionConfiguration;
+ (nullable NSURLSessionConfiguration *)defaultBackgroundSessionConfiguration;

+ (instancetype _Nonnull )manager;
+ (instancetype _Nonnull)managerWithBaseURL:(NSURL * _Nullable )baseURL pathBlock:(NSDictionary *_Nullable (^_Nullable) (void))serviceURLMapDictionaryBlock;

- (void)suspend;
- (void)resume;
- (void)cancel;
- (BOOL)isReqeusting;

- (void)prepareForReuse;

- (void)startMonitoringReachability;

- (NSUInteger)retryCount;
- (NSTimeInterval)retryInterval;
- (NSTimeInterval)timeoutInterval;
- (nullable NSDictionary *)serviceURLMapDictionary;

- (BOOL)shouldSucceed:(NSDictionary * _Nullable)response;
- (nonnull NSError *)generateErrorWithResponse:(NSDictionary * _Nullable)response orError:(NSError * _Nullable)error;

- (void)setServiceURLMapDictionaryBlock:(NSDictionary * _Nullable (^ _Nullable)(void))serviceURLMapDictionaryBlock;
- (void)setShouldSucceed:(BOOL (^ _Nullable)(NSDictionary * _Nullable response))shouldSucceed;
- (void)setGenerateError:(NSError * _Nullable (^ _Nullable)(NSDictionary * _Nullable response, NSError *_Nullable error))generateError;
- (void)setReachabilityStatusChangeBlock:(void (^ _Nullable)(QINetworkReachabilityStatus))reachabilityStatusChangeBlock;
- (void)setRetryCountBlock:(NSUInteger (^ _Nullable)(void))retryCountBlock;
- (void)setRetryIntervalBlock:(NSTimeInterval (^ _Nullable)(void))retryIntervalBlock;
- (void)setTimeoutIntervalBlock:(NSTimeInterval (^ _Nullable)(void))timeoutIntervalBlock;
- (void)setErrorHandler:(void (^ _Nullable)(NSError * _Nonnull error))errorHandler;
- (void)setAuthChallengeBlock:(BOOL (^ _Nullable)(NSDictionary * _Nullable response, NSError *_Nullable error))authChallengeBlock;
- (void)setAuthBlock:(void (^ _Nullable)(QIAuthCompleteHandler _Nonnull completeHandler))authBlock;

- (void)postReqestWithParameter:(NSDictionary <NSString *,id> *_Nullable)param
                 selectorString:(NSString *_Nullable)selectorString
                 concurrentMode:(QINetworkConcurrentMode)concurrentMode
                   successBlock:(void (^_Nullable)(NSDictionary *_Nullable response))successBlock
                   failureBlock:(void (^_Nullable)(NSError *_Nonnull error))failureBlock
                authFailedBlock:(void (^_Nullable)(void(^_Nonnull afterAuthBlock)(void)))authFailedBlock;

- (void)postReqestWithHeader:(NSDictionary <NSString *,NSString *> *_Nullable)header
                   parameter:(NSDictionary <NSString *,id>* _Nullable)param
              selectorString:(NSString * _Nullable)selectorString
              concurrentMode:(QINetworkConcurrentMode)concurrentMode
                successBlock:(void (^_Nullable)(NSDictionary *_Nullable response))successBlock
                failureBlock:(void (^_Nullable)(NSError *_Nonnull error))failureBlock
             authFailedBlock:(void (^_Nullable)(void(^_Nonnull afterAuthBlock)(void)))authFailedBlock;

#warning Not implement yet, for all-round.
/**

- (void)reqestWithMethod:(NSString *_Nonnull)method
                  header:(NSDictionary *_Nullable)header
               parameter:(NSDictionary *_Nullable)param
          selectorString:(NSString *_Nullable)selectorString
          concurrentMode:(QINetworkConcurrentMode)concurrentMode
            successBlock:(void (^_Nullable)(NSDictionary *_Nullable))successBlock
            failureBlock:(void (^_Nullable)(NSError *_Nonnull))failureBlock
         authFailedBlock:(void (^_Nullable)(void(^_Nonnull afterAuthBlock)(void)))authFailedBlock
           progressBlock:(void (^_Nullable)(NSProgress *_Nonnull))progressBlock
       constructingBlock:(void (^_Nullable)(id <QIMultipartFormData>_Nonnull formData))constructingBlock;

*/
 
@end

#pragma mark -

/**
 The `AFMultipartFormData` protocol defines the methods supported by the parameter in the block argument of `AFHTTPRequestSerializer -multipartFormRequestWithMethod:URLString:parameters:constructingBodyWithBlock:`.
 */
@protocol QIMultipartFormData

/**
 Appends the HTTP header `Content-Disposition: file; filename=#{generated filename}; name=#{name}"` and `Content-Type: #{generated mimeType}`, followed by the encoded file data and the multipart form boundary.
 
 The filename and MIME type for this data in the form will be automatically generated, using the last path component of the `fileURL` and system associated MIME type for the `fileURL` extension, respectively.
 
 @param fileURL The URL corresponding to the file whose content will be appended to the form. This parameter must not be `nil`.
 @param name The name to be associated with the specified data. This parameter must not be `nil`.
 @param error If an error occurs, upon return contains an `NSError` object that describes the problem.
 
 @return `YES` if the file data was successfully appended, otherwise `NO`.
 */
- (BOOL)appendPartWithFileURL:(NSURL * _Nonnull)fileURL
                         name:(NSString * _Nonnull)name
                        error:(NSError * _Nullable __autoreleasing * _Nonnull)error;

/**
 Appends the HTTP header `Content-Disposition: file; filename=#{filename}; name=#{name}"` and `Content-Type: #{mimeType}`, followed by the encoded file data and the multipart form boundary.
 
 @param fileURL The URL corresponding to the file whose content will be appended to the form. This parameter must not be `nil`.
 @param name The name to be associated with the specified data. This parameter must not be `nil`.
 @param fileName The file name to be used in the `Content-Disposition` header. This parameter must not be `nil`.
 @param mimeType The declared MIME type of the file data. This parameter must not be `nil`.
 @param error If an error occurs, upon return contains an `NSError` object that describes the problem.
 
 @return `YES` if the file data was successfully appended otherwise `NO`.
 */
- (BOOL)appendPartWithFileURL:(NSURL * _Nonnull)fileURL
                         name:(NSString * _Nonnull)name
                     fileName:(NSString * _Nonnull)fileName
                     mimeType:(NSString * _Nonnull)mimeType
                        error:(NSError * _Nullable __autoreleasing * _Nonnull)error;

/**
 Appends the HTTP header `Content-Disposition: file; filename=#{filename}; name=#{name}"` and `Content-Type: #{mimeType}`, followed by the data from the input stream and the multipart form boundary.
 
 @param inputStream The input stream to be appended to the form data
 @param name The name to be associated with the specified input stream. This parameter must not be `nil`.
 @param fileName The filename to be associated with the specified input stream. This parameter must not be `nil`.
 @param length The length of the specified input stream in bytes.
 @param mimeType The MIME type of the specified data. (For example, the MIME type for a JPEG image is image/jpeg.) For a list of valid MIME types, see http://www.iana.org/assignments/media-types/. This parameter must not be `nil`.
 */
- (void)appendPartWithInputStream:(nullable NSInputStream *)inputStream
                             name:(NSString * _Nonnull)name
                         fileName:(NSString * _Nonnull)fileName
                           length:(int64_t)length
                         mimeType:(NSString * _Nonnull)mimeType;

/**
 Appends the HTTP header `Content-Disposition: file; filename=#{filename}; name=#{name}"` and `Content-Type: #{mimeType}`, followed by the encoded file data and the multipart form boundary.
 
 @param data The data to be encoded and appended to the form data.
 @param name The name to be associated with the specified data. This parameter must not be `nil`.
 @param fileName The filename to be associated with the specified data. This parameter must not be `nil`.
 @param mimeType The MIME type of the specified data. (For example, the MIME type for a JPEG image is image/jpeg.) For a list of valid MIME types, see http://www.iana.org/assignments/media-types/. This parameter must not be `nil`.
 */
- (void)appendPartWithFileData:(NSData * _Nonnull)data
                          name:(NSString * _Nonnull)name
                      fileName:(NSString * _Nonnull)fileName
                      mimeType:(NSString * _Nonnull)mimeType;

/**
 Appends the HTTP headers `Content-Disposition: form-data; name=#{name}"`, followed by the encoded data and the multipart form boundary.
 
 @param data The data to be encoded and appended to the form data.
 @param name The name to be associated with the specified data. This parameter must not be `nil`.
 */

- (void)appendPartWithFormData:(NSData * _Nonnull)data
                          name:(NSString * _Nonnull)name;


/**
 Appends HTTP headers, followed by the encoded data and the multipart form boundary.
 
 @param headers The HTTP headers to be appended to the form data.
 @param body The data to be encoded and appended to the form data. This parameter must not be `nil`.
 */
- (void)appendPartWithHeaders:(nullable NSDictionary <NSString *, NSString *> *)headers
                         body:(NSData * _Nonnull)body;

/**
 Throttles request bandwidth by limiting the packet size and adding a delay for each chunk read from the upload stream.
 
 When uploading over a 3G or EDGE connection, requests may fail with "request body stream exhausted". Setting a maximum packet size and delay according to the recommended values (`kAFUploadStream3GSuggestedPacketSize` and `kAFUploadStream3GSuggestedDelay`) lowers the risk of the input stream exceeding its allocated bandwidth. Unfortunately, there is no definite way to distinguish between a 3G, EDGE, or LTE connection over `NSURLConnection`. As such, it is not recommended that you throttle bandwidth based solely on network reachability. Instead, you should consider checking for the "request body stream exhausted" in a failure block, and then retrying the request with throttled bandwidth.
 
 @param numberOfBytes Maximum packet size, in number of bytes. The default packet size for an input stream is 16kb.
 @param delay Duration of delay each time a packet is read. By default, no delay is set.
 */
- (void)throttleBandwidthWithPacketSize:(NSUInteger)numberOfBytes
                                  delay:(NSTimeInterval)delay;

@end

