//
//  MainViewController.m
//  management
//
//  Created by Quintet on 25/06/2018.
//  Copyright © 2018 Pulmuone. All rights reserved.
//

#import "MainViewController.h"
#import "QIContainer+Pulmuone.h"
#import "ComponentStyler.h"
#import "Navigator.h"
#import "QINetwork+Pulmuone.h"
#import "SVProgressHUD.h"
#import <CoreLocation/CoreLocation.h>
#import <Contacts/Contacts.h>
/**
 
 Show user info
 Show present/absence info
 In/Out
 Navigation to schedule list or detail(editing)
 
 **/

@interface MainViewController () <CLLocationManagerDelegate>
@property (strong, nonatomic) IBOutlet UIView *dashboardView;
@property (strong, nonatomic) IBOutlet UIView *absenceView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *divisionLabel;
@property (strong, nonatomic) IBOutlet UIButton *logoutButton;
@property (strong, nonatomic) IBOutlet UIView *infoView;
@property (strong, nonatomic) IBOutlet UILabel *infoDateLabel;
@property (strong, nonatomic) IBOutlet UILabel *infoTimeLabel;
@property (strong, nonatomic) IBOutlet UILabel *messageLabel;
@property (strong, nonatomic) IBOutlet UIButton *listButton;
@property (strong, nonatomic) IBOutlet UIButton *addButton;
@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray<NSLayoutConstraint*> *constraintsWithAbsence;
@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray<NSLayoutConstraint*> *constraintsWithOutAbsence;

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) CLGeocoder *geocoder;
@property (copy, nonatomic) NSString *workInOutType;
@end

@implementation MainViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self hideNavigationBar];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSDate *startDate = [NSDate date];
    NSDate *endDate = [startDate dateByAddingTimeInterval:600];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(userInfoUpdated:)
                                                 name:QIContainerUserInfoDidSetNotification
                                               object:nil];
    
    [self initializeSubviews];
    [self initializeLocationManager];
    
    [self fetchTaskFromDate:startDate toDate:endDate];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)initializeSubviews {
    [ComponentStyler mainBackgroundColor:self.view];
    [ComponentStyler roundedView:self.dashboardView];
    
    [ComponentStyler mainHeaderLabel:self.nameLabel];
    [ComponentStyler mainSubHeaderLabel:self.divisionLabel];
    [ComponentStyler mainWorkTimeLabel:self.infoDateLabel];
    [ComponentStyler mainWorkTimeLabel:self.infoTimeLabel];
    [ComponentStyler mainMessageLabel:self.messageLabel];
    
    self.infoDateLabel.text = stringFromDateForDisplay(nil);
    
    [self.listButton setTitle:AMLocalizedString(@"내 일정 조회") forState:UIControlStateNormal];
    [self.addButton setTitle:AMLocalizedString(@"내 일정 등록") forState:UIControlStateNormal];
    
    [self hideNavigationBar];
    [self userInfoUpdated:[[QIContainer sharedContainer] userInfo]];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self showAbsenceView];
    });
}

- (void)initializeLocationManager {
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    self.geocoder = [[CLGeocoder alloc] init];
}

- (void)showAbsenceView {
    [self absenceViewAnimation:YES];
}

- (void)hideAbsenceView {
    [self absenceViewAnimation:NO];
}

- (void)absenceViewAnimation:(BOOL)show {
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        __strong typeof(weakSelf) strongSelf = self;
        [strongSelf.view layoutIfNeeded];
        [UIView animateWithDuration:0.35f delay:0.0f usingSpringWithDamping:0.75f initialSpringVelocity:0.0f options:UIViewAnimationOptionAllowUserInteraction|UIViewAnimationOptionBeginFromCurrentState animations:^{
            NSArray *deactiveArray = show ? strongSelf.constraintsWithOutAbsence : strongSelf.constraintsWithAbsence;
            NSArray *activeArray = show ? strongSelf.constraintsWithAbsence : strongSelf.constraintsWithOutAbsence;
            [deactiveArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                NSLayoutConstraint *layoutConstraint = (NSLayoutConstraint *)obj;
                [layoutConstraint setActive:NO];
            }];
            [activeArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                NSLayoutConstraint *layoutConstraint = (NSLayoutConstraint *)obj;
                [layoutConstraint setActive:YES];
            }];
            [strongSelf.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            [strongSelf.view layoutIfNeeded];
        }];
    });
}

- (void)hideNavigationBar {
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}

- (void)userInfoUpdated:(id)noti {
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        NSDictionary *userInfo = [noti isKindOfClass:[NSNotification class]] ? [noti userInfo] : [noti isKindOfClass:[NSDictionary class]] ? noti : nil;
        DDLogDebug(@"[MainVC] user info : %@", userInfo);
        
        if (userInfo[@"userInfo"]) {
            NSString *nameWithPosition = [NSString stringWithFormat:@"%@ %@", [userInfo[@"userInfo"][@"empName"] isKindOfClass:[NSString class]] ? userInfo[@"userInfo"][@"empName"] : @"", [userInfo[@"userInfo"][@"position"] isKindOfClass:[NSString class]] ? userInfo[@"userInfo"][@"position"] : @""];
            
            weakSelf.nameLabel.text = nameWithPosition;
            weakSelf.divisionLabel.text = [userInfo[@"userInfo"][@"prmryDivNm"] isKindOfClass:[NSString class]] ? userInfo[@"userInfo"][@"prmryDivNm"] : weakSelf.divisionLabel.text;
        }
        
        [weakSelf updateWorkInOut:userInfo[@"workInOutList"]];
        
        [weakSelf.dashboardView setNeedsDisplay];
        [weakSelf.dashboardView setNeedsLayout];
    });
}

- (void)updateWorkInOut:(NSArray *)workInOut {
    if ([workInOut isKindOfClass:[NSArray class]]) {
        __weak typeof(self) weakSelf = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            NSDate *date = dateFromString([[workInOut valueForKeyPath:@"wrkInOutDate"] firstObject]);
            weakSelf.infoDateLabel.text = stringFromDateForDisplay(date);
            
            NSString *__block inTimeString = nil;
            NSString *__block outTimeString = nil;
            [workInOut enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (obj[@"wrkInOutStatus"] && [obj[@"wrkInOutStatus"] isKindOfClass:[NSString class]]) {
                    if ([obj[@"wrkInOutStatus"] isEqualToString:@"IN"]) {
                        inTimeString = obj[@"wrkInOutTime"];
                    } else if ([obj[@"wrkInOutStatus"] isEqualToString:@"OUT"]) {
                        outTimeString = obj[@"wrkInOutTime"];
                    }
                }
            }];
            
            weakSelf.infoTimeLabel.text = inTimeString || outTimeString ? [NSString stringWithFormat:@"%@ ~ %@", stringFromTimeNumber(inTimeString), stringFromTimeNumber(outTimeString)] : @"";
            weakSelf.messageLabel.text = outTimeString ? AMLocalizedString(@"오늘도 수고하셨습니다!") : inTimeString ? AMLocalizedString(@"오늘도 힘내세요!") : @"";
        });
    }
}

// MARK: - 출퇴근 타각 요청
- (void)saveWorkInOutLocation:(CLLocation *)location address:(NSString *)address {
    if (self.workInOutType) {
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusRestricted || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied) {
            [self showLocationWarningMessage];
        }
        
        [SVProgressHUD show];
        self.messageLabel.layer.cornerRadius = 0;
        self.messageLabel.layer.backgroundColor = [ColorRGBA(255, 255, 255, 1) CGColor];
        BOOL isIn = [self.workInOutType isEqualToString:@"IN"];
        __weak typeof(self) weakSelf = self;
        [QINetwork workInOutWithType:self.workInOutType latitude:(location ? location.coordinate.latitude : 0) longitude:(location ? location.coordinate.longitude : 0) address:address successBlock:^(NSDictionary *responseDictionary) {
            [SVProgressHUD showSuccessWithStatus:isIn ? AMLocalizedString(@"출근되었습니다.") : AMLocalizedString(@"퇴근되었습니다.")];
            [weakSelf updateWorkInOut:responseDictionary[@"workInOutList"]];
        } failureBlock:^(NSError *error) {
            [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        } authFailedBlock:^(void (^completion)(void)) {
            [SVProgressHUD dismiss];
            //[Navigator modalPresentLoginViewControllerWithCompletionBlock:completion];
            [Navigator toMainViewControllerAfterRetryLogin];
        }];
        self.workInOutType = nil;
    }
}

- (void)showLocationWarningMessage {
    __weak typeof(self) weakSelf = self;
    NSString *previousString = self.messageLabel.text;
    NSString *warningString = AMLocalizedString(@"위치 정보가 꺼져있어 출근 시간만 확인 됩니다!");
    [UIView animateWithDuration:5.0f delay:0.0f usingSpringWithDamping:0.75f initialSpringVelocity:0.0f options:UIViewAnimationOptionAllowUserInteraction|UIViewAnimationOptionBeginFromCurrentState animations:^{
        weakSelf.messageLabel.font = FONT_NORMAL_SIZE(13.0f);
        weakSelf.messageLabel.text = warningString;
        weakSelf.messageLabel.textColor = ColorRGBA(125, 125, 125, 1);
        weakSelf.messageLabel.layer.cornerRadius = 21.0f;
        weakSelf.messageLabel.layer.backgroundColor = [ColorRGBA(247, 247, 247, 1) CGColor];
        [weakSelf.messageLabel setNeedsLayout];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:5.0f delay:0.0f usingSpringWithDamping:0.75f initialSpringVelocity:0.0f options:UIViewAnimationOptionAllowUserInteraction|UIViewAnimationOptionBeginFromCurrentState animations:^{
            if ([weakSelf.messageLabel.text isEqualToString:warningString]) {
                weakSelf.messageLabel.text = previousString;
            }
            [ComponentStyler mainMessageLabel:weakSelf.messageLabel];
            [weakSelf.messageLabel setNeedsLayout];
        } completion:^(BOOL finished) {
        }];
    }];
}

- (void)requestLocation {
    if (self.workInOutType) {
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) {
            [SVProgressHUD showWithStatus:AMLocalizedString(@"위치정보를 가져오는 중...")];
            [self.locationManager requestLocation];
        } else {
            [self saveWorkInOutLocation:nil address:nil];
        }
    }
}

- (void)clearUserInfo {
    self.nameLabel.text = @"";
    self.divisionLabel.text = @"";
    self.infoDateLabel.text = @"";
    self.infoTimeLabel.text = @"";
    self.messageLabel.text = @"";
}

- (IBAction)logoutButtonDidPress:(id)sender {
    DDLogDebug(@"[MainVC] %s", __PRETTY_FUNCTION__);
    feedbackImpact();
    
    __weak typeof(self) weakSelf = self;
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:AMLocalizedString(@"로그아웃") message:AMLocalizedString(@"로그아웃 하시겠습니까?") preferredStyle:UIAlertControllerStyleActionSheet];
    [actionSheet addAction:[UIAlertAction actionWithTitle:AMLocalizedString(@"로그아웃") style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [weakSelf clearUserInfo];
        saveUserID(nil);
        saveUserPassword(nil);
        
        [SVProgressHUD show];
        void(^logoutCompletionBlock)(void) = ^{
            [SVProgressHUD dismiss];
            [Navigator modalPresentLoginViewControllerWithCompletionBlock:^{
                [weakSelf userInfoUpdated:[[QIContainer sharedContainer] userInfo]];
            }];
        };
        [QINetwork logOutWithSuccessBlock:^(NSDictionary *responseDictionary) {
            logoutCompletionBlock();
        } failureBlock:^(NSError *error) {
            logoutCompletionBlock();
        }];
    }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:AMLocalizedString(@"취소") style:UIAlertActionStyleCancel handler:nil]];
    [self setModalPresentationStyle: UIModalPresentationFullScreen];
    [self presentViewController:actionSheet animated:YES completion:nil];
}

// MARK: - 출근
- (IBAction)onWorkButtonDidPress:(UIButton *)sender {
    DDLogDebug(@"[MainVC] %s", __PRETTY_FUNCTION__);
    feedbackImpact();
    self.workInOutType = @"IN";
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
        [self.locationManager requestWhenInUseAuthorization];
    } else {
        [self requestLocation];
    }
}

// MARK: - 퇴근
- (IBAction)offWorkButtonDidPress:(UIButton *)sender {
    DDLogDebug(@"[MainVC] %s", __PRETTY_FUNCTION__);
    feedbackImpact();
    self.workInOutType = @"OUT";
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
        [self.locationManager requestWhenInUseAuthorization];
    } else {
        [self requestLocation];
    }
}

- (IBAction)scheduleListButtonDidPress:(UIButton *)sender {
    DDLogDebug(@"[MainVC] %s", __PRETTY_FUNCTION__);
    feedbackImpact();
    [Navigator toScheduleListViewController:NO date:nil];
}

- (IBAction)scheduleAddButonDidPress:(UIButton *)sender {
    DDLogDebug(@"[MainVC] %s", __PRETTY_FUNCTION__);
    feedbackImpact();
    [Navigator toScheduleDetailViewControllerWithDate:[NSDate date]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [self requestLocation];
    } else if (status == kCLAuthorizationStatusRestricted || status == kCLAuthorizationStatusDenied) {
        [self showLocationWarningMessage];
    }
}

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    if (self.workInOutType) {
        CLLocation *location = [locations lastObject];
        __weak typeof(self) weakSelf = self;
        void(^completionBlock)(CLPlacemark*) = ^(CLPlacemark *placemark) {
            NSString *address = @"";
            if (@available(iOS 11.0, *)) {
                address = [[CNPostalAddressFormatter stringFromPostalAddress:[placemark postalAddress]?:[CNPostalAddress new] style:CNPostalAddressFormatterStyleMailingAddress] stringByReplacingOccurrencesOfString:@"\n" withString:@", "];
            } else {
                address = [NSString stringWithFormat:@"%@, %@, %@, %@, %@, %@",
                           placemark.thoroughfare,
                           placemark.locality,
                           placemark.subLocality,
                           placemark.administrativeArea,
                           placemark.postalCode,
                           placemark.country];
            }
            [weakSelf saveWorkInOutLocation:location address:address];
        };
        if (@available(iOS 11.0, *)) {
            [self.geocoder reverseGeocodeLocation:location preferredLocale:[NSLocale currentLocale] completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
                if (!error) {
                    completionBlock([placemarks firstObject]);
                }
            }];
        } else {
            // Fallback on earlier versions
            [self.geocoder reverseGeocodeLocation:location completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
                if (!error) {
                    completionBlock([placemarks firstObject]);
                }
            }];
        }
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"%s : %@", __PRETTY_FUNCTION__, error);
    [SVProgressHUD showErrorWithStatus:AMLocalizedString(@"위치 정보를 불러올 수 없었습니다.")];
    [self saveWorkInOutLocation:nil address:nil];
}

- (void)fetchTaskFromDate:(NSDate *)startDate toDate:(NSDate *)endDate {
    NSLog(@"self.isValidTask: %@", self.isValidTask ? @"YES" : @"NO");
    if (!self.isValidTask) {
        [self beginNetworking];
        __weak typeof(self) weakSelf = self;
        [QINetwork taskListBetweenStartDate:stringFromDate(startDate) endDate:stringFromDate(endDate) successBlock:^(NSDictionary *responseDictionary) {
            //weakSelf.taskList = responseDictionary[@"taskList"];
            [GlobalVariables singleton].taskList = responseDictionary[@"taskList"];
            NSLog(@"weakSelf.taskList: %@", [GlobalVariables singleton].taskList);
            //[weakSelf endNetworking];
            [weakSelf fetchCommonCodeAndShow];
            weakSelf.isValidTask = YES;
        } failureBlock:^(NSError *error) {
            [weakSelf warnNetworkingWithError:error];
        } authFailedBlock:^(void (^completionBlock)(void)) {
            [weakSelf endNetworking];
            [Navigator modalPresentLoginViewControllerWithCompletionBlock:completionBlock];
        }];
    }
}

- (void)fetchCommonCodeAndShow {
    //if (!self.dutyList) {
        [GlobalVariables singleton].dutyList = @[];
        //[self beginNetworking];
        __weak typeof(self) weakSelf = self;
        [QINetwork commonCodeWithGroupCode:@"WORK_TYPE_CD" successBlock:^(NSDictionary *responseDictionary) {
            [GlobalVariables singleton].dutyList = responseDictionary[@"commCodeList"];
            [weakSelf endNetworking];
        } failureBlock:^(NSError *error) {
            //weakSelf.dutyList = nil;
            [weakSelf warnNetworkingWithError:error];
        }];
    //}
}

@end
