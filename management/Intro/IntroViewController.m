//
//  IntroViewController.m
//  management
//
//  Created by Quintet on 25/06/2018.
//  Copyright © 2018 Pulmuone. All rights reserved.
//

#import "IntroViewController.h"
#import "QINetwork+Pulmuone.h"
#import "Navigator.h"
#import "SVProgressHUD.h"
#import "NSString+Utils.h"

@import FirebaseDatabase;

/**

 Version check
 Auto login
 Transition to Login/Main
 
**/


@interface IntroViewController ()

@end

@implementation IntroViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 포어그라운드 감지 옵저버
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appEnterForeground:)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
    
    //[self versionCheck];
    [self versionCheckFromFirebase];
}

- (void)appEnterForeground:(NSNotification *) notification {
    [self versionCheckFromFirebase];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self]; // 옵저버 해제
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
- (void)versionCheck {
    NSURLSessionConfiguration *sessionConfigure = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfigure.requestCachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfigure];
    __weak typeof(self) weakSelf = self;
    NSURLSessionDataTask *dataTask = [urlSession dataTaskWithURL:PLIST_URL completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (!error) {
            NSString *versionString = versionStringFromPlist(data);
            NSLog(@"versionString: %@", versionString);
            if ([versionString isKindOfClass:[NSString class]] && [versionString length] && ![versionString isEqualToString:[[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"]]) {
                [weakSelf presentUpdatePopup];
            } else {
                [weakSelf autoLogin];
            }
        } else {
            NSLog(@"error : %@", error);
            [weakSelf autoLogin];
        }
    }];
    [dataTask resume];
}

- (void)presentUpdatePopup {
    feedbackNotificationSuccess();
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:AMLocalizedString(@"새 버전 알림") message:AMLocalizedString(@"새 버전이 출시되었습니다. 업데이트 해주세요.") preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:AMLocalizedString(@"업데이트") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"itms-services://?action=download-manifest&url=%@", [PLIST_URL absoluteString]]];
            [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
            [[UIApplication sharedApplication] performSelector:@selector(suspend)];
        }]];
        [alertController addAction:[UIAlertAction actionWithTitle:AMLocalizedString(@"취소") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [weakSelf autoLogin];
        }]];
        [weakSelf setModalPresentationStyle: UIModalPresentationFullScreen];
        [weakSelf presentViewController:alertController animated:YES completion:nil];
    });
}
 */

/**
 AS-IS 버전체크 방식(plist 참조)에서 Firebase real-time database 참조 방식으로 변경
 기존에는 선택업데이트만 있었지만, 강제업데이트 기능이 추가됨.
 modify date : 22/05/02 , 모바일개발팀 박희준
 */
- (void)versionCheckFromFirebase {
    [self dismissCommonAlert];
    [[[FIRDatabase database] reference] getDataWithCompletionBlock:^(NSError * _Nullable error, FIRDataSnapshot * _Nonnull snapshot) {
        
        NSString *forceVersion      = error ? @"0.0.0" : [snapshot childSnapshotForPath:@"ios/force_version_name"].value;
        NSString *optionalVersion   = error ? @"0.0.0" : [snapshot childSnapshotForPath:@"ios/option_version_name"].value;
        
        NSComparisonResult result = [appVersion() compareVersion:forceVersion];
        if (result == NSOrderedAscending) {
            // 강제 업데이트
            [self showCommonAlertWithTitle:AMLocalizedString(@"새 버전 알림")
                                   message:AMLocalizedString(@"새 버전이 출시되었습니다. 업데이트 해주세요.")
                                btnOneName:AMLocalizedString(@"업데이트") btnOneAction:^{ [self installApp]; }
                                btnTwoName:nil btnTwoAction:^{}];
        } else {
            result = [appVersion() compareVersion:optionalVersion];
            // 선택 업데이트
            if (result == NSOrderedAscending) {
                [self showCommonAlertWithTitle:AMLocalizedString(@"새 버전 알림")
                                       message:AMLocalizedString(@"최신 버전의 업데이트가 있어요. 새로운 앱으로 업데이트하시겠습니까?")
                                    btnOneName:AMLocalizedString(@"건너뛰기") btnOneAction:^{ [self autoLogin]; }
                                    btnTwoName:AMLocalizedString(@"업데이트") btnTwoAction:^{ [self installApp]; }];
            } else {
                // 업데이트 없음
                [self autoLogin];
            }
        }
    }];
}

- (void)installApp {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"itms-services://?action=download-manifest&url=%@", [PLIST_URL absoluteString]]];
    [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
    [[UIApplication sharedApplication] performSelector:@selector(suspend)];
}

- (void)autoLogin {
    void (^loginThenMain)(NSString *) = ^(NSString *message){
        dispatch_async(dispatch_get_main_queue(), ^{
            saveUserID(nil);
            saveUserPassword(nil);
            if ([message isKindOfClass:[NSString class]] && [message length]) {
                [SVProgressHUD showErrorWithStatus:message];
            }
            [Navigator modalPresentLoginViewControllerWithCompletionBlock:^{
                [Navigator toMainViewController];
            }];
        });
    };
    [GlobalVariables singleton].email = appUserID();
    if (appUserID() && appUserPassword()) {
        [QINetwork loginWithUserID:appUserID() password:appUserPassword() successBlock:^(NSDictionary *responseDictionary) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([QINetwork manager].shouldSucceed(responseDictionary)) {
                    [[QIContainer sharedContainer] setUserInfo:@{@"userInfo":responseDictionary[@"userInfo"], @"workInOutList":responseDictionary[@"workInOutList"]}];
                    [Navigator toMainViewController];
                } else {
                    DDLogWarn(@"[Intro] Auto login failed with success false : %@", responseDictionary);
                    loginThenMain(responseDictionary[@"message"]);
                }
            });
        } failureBlock:^(NSError *error) {
            DDLogWarn(@"[Intro] Auto login failed with error : %@", error);
            //loginThenMain(error.localizedDescription);
            [Navigator toMainViewControllerAfterRetryLogin];
        }];
    } else {
        loginThenMain(nil);
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
