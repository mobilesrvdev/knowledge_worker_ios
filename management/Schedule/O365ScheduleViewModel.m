//
//  O365ScheduleViewModel.m
//  management
//
//  Created by 이주한 on 2020/05/20.
//  Copyright © 2020 Pulmuone. All rights reserved.
//

#import "O365ScheduleViewModel.h"

@implementation O365ScheduleViewModel

+ (instancetype)o365ScheduleViewModelFromSchedule:(NSDictionary *)schedule {
    return [[[self class] alloc] initWithSchedule:schedule];
}

- (instancetype)init {
    if (self = [self initWithSchedule:nil]) {
        
    }
    return self;
}

- (instancetype)initWithSchedule:(NSDictionary *)schedule {
    if (self = [super init]) {
        _identifier = schedule[@"id"];
        _startDt = schedule[@"startDt"];
        _startTime = schedule[@"startTime"];
        _endDt = schedule[@"endDt"];
        _endTime = schedule[@"endTime"];
//        _taskNm = schedule[@"taskNm"];
//        _dutyCdNm = schedule[@"dutyCdNm"];
//        _wrkComnt = schedule[@"wrkComnt"];
        _title = schedule[@"title"];
    }
    return self;
}

- (nonnull id<NSObject>)diffIdentifier {
    return self.identifier;
}

- (BOOL)isEqualToDiffableObject:(nullable id<IGListDiffable>)object {
    return [[(NSObject *)object class] isKindOfClass:[self class]]
        && [self.identifier isEqualToString:[(O365ScheduleViewModel *)object identifier]]
        && [self.startDt isEqualToString:[(O365ScheduleViewModel *)object startDt]]
        && [self.startTime isEqualToString:[(O365ScheduleViewModel *)object startTime]]
        && [self.endDt isEqualToString:[(O365ScheduleViewModel *)object endDt]]
        && [self.endTime isEqualToString:[(O365ScheduleViewModel *)object endTime]]
//        && [self.taskNm isEqualToString:[(O365ScheduleViewModel *)object taskNm]]
//        && [self.dutyCdNm isEqualToString:[(O365ScheduleViewModel *)object dutyCdNm]]
//        && [self.wrkComnt isEqualToString:[(O365ScheduleViewModel *)object wrkComnt]]
        && [self.title isEqualToString:[(O365ScheduleViewModel *)object title]];
}
@end
