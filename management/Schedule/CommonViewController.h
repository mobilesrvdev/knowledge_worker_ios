//
//  CommonViewController.h
//  management
//
//  Created by 이주한 on 2020/05/08.
//  Copyright © 2020 Pulmuone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GlobalVariables.h"

NS_ASSUME_NONNULL_BEGIN

@interface CommonViewController : UIViewController
- (void)flashAnimationComponent:(NSArray<UIView*>*)components completion:(void(^)(BOOL finished))completion;
- (void)beginNetworking;
- (void)endNetworking;
- (void)failedNetworkingWithError:(NSError *)error;
- (void)warnNetworkingWithError:(NSError *)error;
- (void)showCommonAlertWithTitle:(NSString *)title message:(NSString *)msg
                      btnOneName:(NSString *)btnOneTitle btnOneAction:(void (^)(void))btnOneAction
                      btnTwoName:(nullable NSString *)btnTwoTitle btnTwoAction:(void (^)(void))btnTwoAction;
- (void)dismissCommonAlert;

@property (nonatomic, strong) NSMutableArray <NSError *> __block *failedNetworkMessages;
@property (nonatomic, strong) NSMutableArray <NSError *> __block *warningNetworkMessages;

@property (nonatomic) NSInteger __block networkActivityCount;
@property (nonatomic) NSInteger __block networkActivityCountMax;

@property (nonatomic) BOOL isValidTask;

@end

NS_ASSUME_NONNULL_END
