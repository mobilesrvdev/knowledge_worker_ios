//
//  ScheduleViewModel.h
//  management
//
//  Created by Quintet on 27/06/2018.
//  Copyright © 2018 Pulmuone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <IGListKit/IGListKit.h>

@interface ScheduleViewModel : NSObject <IGListDiffable>
@property (nonatomic, strong, readonly) NSString *identifier;
@property (nonatomic, strong, readonly) NSString *startDt;
@property (nonatomic, strong, readonly) NSString *startTime;
@property (nonatomic, strong, readonly) NSString *endDt;
@property (nonatomic, strong, readonly) NSString *endTime;
@property (nonatomic, strong, readonly) NSString *taskNm;
@property (nonatomic, strong, readonly) NSString *dutyCdNm;
@property (nonatomic, strong, readonly) NSString *wrkComnt;
@property (nonatomic, strong, readonly) NSString *wrkDesc;

+ (instancetype)scheduleViewModelFromSchedule:(NSDictionary *)schedule;

@end
