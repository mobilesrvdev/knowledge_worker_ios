//
//  ScheduleSectionController.m
//  management
//
//  Created by Quintet on 28/06/2018.
//  Copyright © 2018 Pulmuone. All rights reserved.
//

#import "ScheduleSectionController.h"
#import "Schedule.h"
#import "ScheduleViewModel.h"
#import "O365ScheduleViewModel.h"
#import "O365Schedule.h"
#import "ScheduleCell.h"
#import "O365ScheduleCell.h"
#import "Navigator.h"

@interface CustomTapGestureRecognizer : UILongPressGestureRecognizer

@property (nonatomic, strong) ScheduleViewModel *viewModel;

@end


// customTapGestureRecognizer.m

@implementation CustomTapGestureRecognizer

@end


@interface ScheduleSectionController () <IGListBindingSectionControllerDataSource, IGListBindingSectionControllerSelectionDelegate, UIGestureRecognizerDelegate>

@end

@implementation ScheduleSectionController

- (instancetype)init {
    if (self = [super init]) {
        self.dataSource = self;
        self.selectionDelegate = self;
    }
    return self;
}

- (nonnull UICollectionViewCell<IGListBindable> *)sectionController:(nonnull IGListBindingSectionController *)sectionController cellForViewModel:(nonnull id)viewModel atIndex:(NSInteger)index {
    NSLog(@"viewModel: %@, index: %ld", viewModel, (long)sectionController.section);
    if ([viewModel isKindOfClass:[ScheduleViewModel class]]) {
        ScheduleCell *cell = [sectionController.collectionContext dequeueReusableCellFromStoryboardWithIdentifier:@"ScheduleCell" forSectionController:self atIndex:index];
        cell.orderLabel.text = [NSString stringWithFormat:@"%@", @([sectionController section]+1)];
        
        CustomTapGestureRecognizer *longpressGesture = [[CustomTapGestureRecognizer alloc] initWithTarget:self action:@selector(longPressHandler:)];
        longpressGesture.viewModel = viewModel;
        //longpressGesture.minimumPressDuration = 2;
        [longpressGesture setDelegate:self];
        [cell addGestureRecognizer:longpressGesture];
        
        return cell;
    } else  if ([viewModel isKindOfClass:[O365ScheduleViewModel class]]) {
        O365ScheduleCell *cell = [sectionController.collectionContext dequeueReusableCellFromStoryboardWithIdentifier:@"O365ScheduleCell" forSectionController:self atIndex:index];
        ((O365ScheduleViewModel*)viewModel).index = sectionController.section;
        cell.checkBox.on = ((O365ScheduleViewModel*)viewModel).isChecked;
        return cell;
    }
    return (UICollectionViewCell<IGListBindable> *)[UICollectionViewCell new];
}

- (void)longPressHandler:(UILongPressGestureRecognizer *)gestureRecognizer {
    NSLog(@"gestureRecognizer.state: %ld", (long)gestureRecognizer.state);
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {}
    else if (gestureRecognizer.state == UIGestureRecognizerStateBegan){
        feedbackSelection();

        NSLog(@"longPressHandler, viewModel: %@", ((CustomTapGestureRecognizer*)gestureRecognizer).viewModel);
        NSDictionary *notiDic = [[NSDictionary alloc] initWithObjectsAndKeys:((CustomTapGestureRecognizer*)gestureRecognizer).viewModel, SCHEDULE_DID_REMOVE, nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:SCHEDULE_DID_REMOVE object:nil userInfo:notiDic];
    }
}

- (CGSize)sectionController:(nonnull IGListBindingSectionController *)sectionController sizeForViewModel:(nonnull id)viewModel atIndex:(NSInteger)index {
    return CGSizeMake([sectionController collectionContext].insetContainerSize.width, 71.0f);//65.0f);
}

- (nonnull NSArray<id<IGListDiffable>> *)sectionController:(nonnull IGListBindingSectionController *)sectionController viewModelsForObject:(nonnull id)object {
    NSLog(@"object: %@", object);
    if ([object isKindOfClass:[Schedule class]]) {
        Schedule *schedule = (Schedule *)object;
        ScheduleViewModel *viewModel = [ScheduleViewModel scheduleViewModelFromSchedule:[schedule dictionary]];
        return @[viewModel];
    }
    else if ([object isKindOfClass:[O365Schedule class]]) {
        O365Schedule *schedule = (O365Schedule *)object;
        O365ScheduleViewModel *viewModel = [O365ScheduleViewModel o365ScheduleViewModelFromSchedule:[schedule dictionary]];
        return @[viewModel];
    }
    return @[];
}

- (void)sectionController:(IGListBindingSectionController *)sectionController
     didSelectItemAtIndex:(NSInteger)index
                viewModel:(id)viewModel {
    //다음과 같이 체크한 이유는 O365일정화면, 즉 테이블에서 cell이 선택되어도 상세 화면으로 이동하지 않도록 하기 위해서 타이틀명을 체크
    UIViewController *viewController = topMostController();
    if ([viewController.title isEqualToString:@"Office365 일정"]) {
        return;
    }
    if ([viewModel isKindOfClass:[ScheduleViewModel class]]) {
        ScheduleViewModel *scheduleViewModel = (ScheduleViewModel *)viewModel;
        feedbackImpact();
        [Navigator toScheduleDetailViewControllerWithID:scheduleViewModel.identifier];
    }
}

@end
