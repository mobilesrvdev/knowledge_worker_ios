//
//  ScheduleDetailViewController.m
//  management
//
//  Created by Quintet on 25/06/2018.
//  Copyright © 2018 Pulmuone. All rights reserved.
//

#import "ScheduleDetailViewController.h"
#import "ComponentStyler.h"
#import "TextFieldLikeButton.h"
#import "Navigator.h"
#import "QINetwork+Pulmuone.h"
#import "RMDateSelectionViewController.h"
#import "SVProgressHUD.h"
#import "IQKeyboardManager.h"
/**
 
 Show the specific schedule
 Show empty schedule (for editing)
 Save schedule (new/edit)
 Delete schedule (edit)
 
 **/

@interface ScheduleDetailViewController () <UITextViewDelegate>
@property (nonatomic, strong) IBOutlet UILabel *startDateTitleLabel;
@property (nonatomic, strong) IBOutlet UILabel *endDateTitleLabel;
@property (nonatomic, strong) IBOutlet UILabel *taskTitleLabel;
@property (nonatomic, strong) IBOutlet UILabel *dutyTitleLabel;
@property (nonatomic, strong) IBOutlet UILabel *descTitleLabel;
@property (nonatomic, strong) IBOutlet UILabel *comntTitleLabel;
@property (nonatomic, strong) IBOutlet TextFieldLikeButton *startDateButton;
@property (nonatomic, strong) IBOutlet TextFieldLikeButton *startTimeButton;
@property (nonatomic, strong) IBOutlet TextFieldLikeButton *endDateButton;
@property (nonatomic, strong) IBOutlet TextFieldLikeButton *endTimeButton;
@property (nonatomic, strong) IBOutlet TextFieldLikeButton *taskButton;
@property (nonatomic, strong) IBOutlet TextFieldLikeButton *dutyButton;
@property (nonatomic, strong) IBOutlet UITextView *descTextView;
@property (nonatomic, strong) IBOutlet UITextView *comntTextView;
@property (nonatomic, strong) IBOutlet UIButton *saveButton;
@property (nonatomic, strong) IBOutlet UIButton *cancelButton;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *descTitleVertical;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *descTitleHorizontal;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *descTextViewHeight;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *comntTextViewHeight;
@property (nonatomic, strong) IBOutletCollection(NSLayoutConstraint) NSArray<NSLayoutConstraint*> *comntVerticalSpacings;

@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) NSDateFormatter *timeFormatter;
@property (nonatomic, strong) NSMutableDictionary *modifiedSchedule;
@property (nonatomic, copy) NSDate *startDate;
@property (nonatomic, copy) NSDate *endDate;
//@property (nonatomic, strong) NSArray <NSDictionary *> *taskList;
//@property (nonatomic, strong) NSArray <NSDictionary *> *dutyList;
//@property (nonatomic) BOOL isValidTask;
//@property (nonatomic) NSInteger __block networkActivityCount;
//@property (nonatomic) NSInteger __block networkActivityCountMax;
//@property (nonatomic, strong) NSMutableArray <NSError *> __block *failedNetworkMessages;
//@property (nonatomic, strong) NSMutableArray <NSError *> __block *warningNetworkMessages;
@end

@implementation ScheduleDetailViewController

- (BOOL)isNew {
    return !([self.scheduleID isKindOfClass:[NSString class]] || [self.scheduleID length]);
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSLog(@"[ScheduleDetailViewController] %s, scheduleID: %@", __PRETTY_FUNCTION__, _scheduleID);

    self.title = self.scheduleID ? AMLocalizedString(@"내 일정 상세") : AMLocalizedString(@"내 일정 등록");
    
    [self initializeFormatter];
    [self initializeSubviews];
    [self textViewInitialScroll];
    [self configureKeyboard];
    
    [self fetchSchedule];
    
    //wooya510@20200506:ADD
    //O365일정 가져오기 위한 버튼 추가
    [self configureNavRightButton];
}

// MARK: - O365일정 버튼 터치 이벤트
- (void)buttonIsPressed:(id)sender {    
    DDLogDebug(@"[ScheduleDetailViewController] %s", __PRETTY_FUNCTION__);
    feedbackImpact();
    [Navigator toScheduleListViewController:YES date:nil];
}

// MARK: - O365일정 버튼 설정
- (void)configureNavRightButton {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    button.frame = CGRectMake(0, 0, 70, 30);
    [button addTarget:self action:@selector(buttonIsPressed:) forControlEvents:UIControlEventTouchDown];
    [ComponentStyler scheduleAddButton:button];
    
    if (self.scheduleID) {
        [button setHidden:YES];
        return;
    }
    [button setTitle:AMLocalizedString(@"O365일정") forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    [button invalidateIntrinsicContentSize];
    [button setNeedsLayout];
}

- (void)initializeFormatter {
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateFormat:@"yyyy.MM.dd"];
    self.timeFormatter = [[NSDateFormatter alloc] init];
    [self.timeFormatter setDateFormat:@"HH:mm"];
}

- (void)initializeSubviews {
    self.startDateTitleLabel.text = AMLocalizedString(@"시작일시");
    self.endDateTitleLabel.text = AMLocalizedString(@"종료일시");
    self.taskTitleLabel.text = AMLocalizedString(@"TASK");
    self.dutyTitleLabel.text = AMLocalizedString(@"업무유형");
    self.descTitleLabel.text = AMLocalizedString(@"근무상세");
    self.comntTitleLabel.text = AMLocalizedString(@"Comment");
    
    self.descTextView.delegate = self;
    
    [self.saveButton setTitle:AMLocalizedString(@"등록") forState:UIControlStateNormal];
    [self.cancelButton setTitle:AMLocalizedString(@"삭제") forState:UIControlStateNormal];
    
    [ComponentStyler defaultNavigationBar:self.navigationController.navigationBar];
    
    [ComponentStyler scheduleSubTitleLabel:self.startDateTitleLabel required:YES];
    [ComponentStyler scheduleSubTitleLabel:self.endDateTitleLabel required:YES];
    [ComponentStyler scheduleSubTitleLabel:self.taskTitleLabel required:YES];
    [ComponentStyler scheduleSubTitleLabel:self.dutyTitleLabel required:YES];
    [ComponentStyler scheduleSubTitleLabel:self.descTitleLabel required:NO];
    [ComponentStyler scheduleSubTitleLabel:self.comntTitleLabel required:NO];
    
    [ComponentStyler defaultButtonLikeTextField:self.startDateButton];
    [ComponentStyler defaultButtonLikeTextField:self.startTimeButton];
    [ComponentStyler defaultButtonLikeTextField:self.endDateButton];
    [ComponentStyler defaultButtonLikeTextField:self.endTimeButton];
    [ComponentStyler defaultButtonLikeTextField:self.taskButton];
    [ComponentStyler defaultButtonLikeTextField:self.dutyButton];
    
    [ComponentStyler defaultTextView:self.descTextView];
    [ComponentStyler attentionTextView:self.comntTextView];
    
    [ComponentStyler defaultButtonStylePositiveMedium:self.saveButton];
    [ComponentStyler defaultButtonStyleCancelMedium:self.cancelButton];
}

- (void)textViewInitialScroll {
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.35f delay:0.0f options:UIViewAnimationOptionAllowUserInteraction|UIViewAnimationOptionAllowAnimatedContent|UIViewAnimationOptionBeginFromCurrentState animations:^{
        [weakSelf.descTextView setContentOffset:CGPointMake(0.0f, -17.0f)];
        [weakSelf.comntTextView setContentOffset:CGPointMake(0.0f, -17.0f)];
    } completion:nil];
}

- (void)configureKeyboard {
    [IQKeyboardManager sharedManager].keyboardDistanceFromTextField = 50.0f;
}

- (NSDate *)defaultStartTimeFromDate:(NSDate *)date {
    NSDateComponents *dateComponenet = [[NSCalendar autoupdatingCurrentCalendar] components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute fromDate:date];
    [dateComponenet setCalendar:[NSCalendar autoupdatingCurrentCalendar]];
    
    const NSInteger tick = 30;
    const NSInteger offset = 15;
    NSInteger tock = floor((dateComponenet.minute+offset)/tick);
    NSInteger upMinutes = tick * tock;
    NSInteger upHour = floor((dateComponenet.hour + upMinutes/60) / 24);
    [dateComponenet setHour:dateComponenet.hour - upHour];
    [dateComponenet setMinute:upMinutes];
    
    return [dateComponenet date];
}

- (NSDate *)defaultEndTimeFromDate:(NSDate *)date {
    NSDateComponents *dateComponenet = [[NSCalendar autoupdatingCurrentCalendar] components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute fromDate:date];
    [dateComponenet setCalendar:[NSCalendar autoupdatingCurrentCalendar]];
    [dateComponenet setHour:18];
    [dateComponenet setMinute:0];
    return [dateComponenet date];
}

- (void)beginNetworking {
    self.networkActivityCount++;
    self.networkActivityCountMax = MAX(self.networkActivityCountMax, self.networkActivityCount);
    if (self.networkActivityCount > 0) {
        if (self.networkActivityCountMax == self.networkActivityCount) {
            [SVProgressHUD show];
        } else {
            [SVProgressHUD showProgress:(self.networkActivityCountMax - self.networkActivityCount)/(float)self.networkActivityCountMax];
        }
    }
}

- (void)endNetworking {
    self.networkActivityCount--;
    if ([self.failedNetworkMessages count]) {
        [[QINetwork manager] cancel];
        NSString *__block failedMessage = @"";
        NSUInteger lastIndex = [self.failedNetworkMessages count] - 1;
        [self.failedNetworkMessages enumerateObjectsUsingBlock:^(NSError * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            failedMessage = [failedMessage stringByAppendingString:obj.localizedDescription];
            if (idx != lastIndex) {
                failedMessage = [failedMessage stringByAppendingString:@"\n"];
            }
        }];
        [SVProgressHUD showErrorWithStatus:failedMessage];
        [self.navigationController popViewControllerAnimated:YES];
        self.networkActivityCountMax = 0;
        //self.failedNetworkMessages = nil;
    } else if ([self.warningNetworkMessages count]) {
        NSString *__block failedMessage = @"";
        NSUInteger lastIndex = [self.warningNetworkMessages count] - 1;
        [self.warningNetworkMessages enumerateObjectsUsingBlock:^(NSError * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            failedMessage = [failedMessage stringByAppendingString:obj.localizedDescription];
            if (idx != lastIndex) {
                failedMessage = [failedMessage stringByAppendingString:@"\n"];
            }
        }];
        [SVProgressHUD showErrorWithStatus:failedMessage];
        self.networkActivityCountMax = 0;
        //self.warningNetworkMessages = nil;
    } else if (self.networkActivityCount > 0) {
        [SVProgressHUD showProgress:(self.networkActivityCountMax - self.networkActivityCount)/(float)self.networkActivityCountMax];
    } else {
        [SVProgressHUD dismiss];
        self.networkActivityCountMax = 0;
    }
}

- (void)failedNetworkingWithError:(NSError *)error {
    self.failedNetworkMessages = self.failedNetworkMessages ?: [NSMutableArray array];
    [self.failedNetworkMessages addObject:error];
    [self endNetworking];
}

- (void)warnNetworkingWithError:(NSError *)error {
    self.warningNetworkMessages = self.warningNetworkMessages ?: [NSMutableArray array];
    [self.warningNetworkMessages addObject:error];
    [self endNetworking];
}

- (void)fetchSchedule {
    
    [self.taskButton setTitle:nil forState:UIControlStateApplication];
    [self.dutyButton setTitle:nil forState:UIControlStateApplication];
    
    if ([self isNew]) {
        [self hideUnnecessaryViews];
        NSLog(@"initialDate: %@", self.initialDate);
        [self setStartDate:self.initialDate];
        [self setEndDate:[self.startDate dateByAddingTimeInterval:600 * 3]];
    } else {
        [self beginNetworking];
        __weak typeof(self) weakSelf = self;
        [QINetwork scheduleDetailByID:self.scheduleID successBlock:^(NSDictionary *responseDictionary) {
            NSDictionary *scheduleDictionary = responseDictionary[@"workScheduleInfo"];
            if ([scheduleDictionary isKindOfClass:[NSDictionary class]] && [scheduleDictionary count]) {
                weakSelf.startDate = [dateComponentFromString(scheduleDictionary[@"startDt"], scheduleDictionary[@"startTime"]) date];
                weakSelf.endDate = [dateComponentFromString(scheduleDictionary[@"endDt"], scheduleDictionary[@"endTime"]) date];
                
                [weakSelf.taskButton setTitle:scheduleDictionary[@"taskNm"] forState:UIControlStateNormal];
                [weakSelf.taskButton setTitle:scheduleDictionary[@"taskId"] forState:UIControlStateApplication];
                [weakSelf.dutyButton setTitle:scheduleDictionary[@"dutyCdNm"] forState:UIControlStateNormal];
                [weakSelf.dutyButton setTitle:scheduleDictionary[@"dutyCd"] forState:UIControlStateApplication];
                
                weakSelf.descTextView.text = [scheduleDictionary[@"wrkDesc"] isKindOfClass:[NSString class]] ? scheduleDictionary[@"wrkDesc"] : @"";
                weakSelf.comntTextView.text = [scheduleDictionary[@"wrkComnt"] isKindOfClass:[NSString class]] ? scheduleDictionary[@"wrkComnt"] : @"";
                
                [weakSelf endNetworking];
            } else {
                [SVProgressHUD showErrorWithStatus:defaultErrorString()];
                [weakSelf.navigationController popViewControllerAnimated:YES];
            }
        } failureBlock:^(NSError *error) {
            [weakSelf failedNetworkingWithError:error];
        } authFailedBlock:^(void (^completionBlock)(void)) {
            [weakSelf endNetworking];
            [[QINetwork manager] cancel];
            //[Navigator modalPresentLoginViewControllerWithCompletionBlock:completionBlock];
            [Navigator toMainViewControllerAfterRetryLogin];
        }];
    }
    
    //[self fetchCommonCodeAndShow:NO];
}

- (void)hideUnnecessaryViews {
    self.comntTitleLabel.text = @"";
    self.comntTextViewHeight.constant = 0.0f;
    [self.comntVerticalSpacings enumerateObjectsUsingBlock:^(NSLayoutConstraint * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        obj.constant = 0.0f;
    }];
    [self updateTextViewLabel:@""];
    [self.cancelButton setTitle:AMLocalizedString(@"취소") forState:UIControlStateNormal];
}

- (void)setStartDate:(NSDate *)date {
    if (![self.startDateButton.titleLabel.text isEqualToString:[self.dateFormatter stringFromDate:date]] && [self isValidStartDate:date endDate:self.endDate]) {
    ///zero9 시작일자 종료일자 유효성 검사 사용안함- 시작일자변경시 종료일자 변경
        _startDate = date;
        _endDate = date;
        if ([self isNew] && ![self.startTimeButton.titleLabel.text length]) {
            [self setStartTime:[self defaultStartTimeFromDate:date]];
            [self setEndTime:[self defaultEndTimeFromDate:date]];
            
        } else {
            [self updateScheduleDate];
        }
    }
}

- (void)setStartTime:(NSDate *)date {
    if (![self.startTimeButton.titleLabel.text isEqualToString:[self.timeFormatter stringFromDate:date]] && [self isValidStartDate:date endDate:self.endDate]) {
        _startDate = date;
        [self updateScheduleDate];
    }
}

- (void)setEndDate:(NSDate *)date {
    if (/*![self.endDateButton.titleLabel.text isEqualToString:[self.dateFormatter stringFromDate:date]] &&*/ [self isValidStartDate:self.startDate endDate:date]) {
        _endDate = date;
        
        if ([self isNew] && ![self.endTimeButton.titleLabel.text length]) {
            [self setEndTime:[self defaultEndTimeFromDate:date]];
        } else {
            [self updateScheduleDate];
        }
    }
}

- (void)setEndTime:(NSDate *)date {
    if (![self.endTimeButton.titleLabel.text isEqualToString:[self.timeFormatter stringFromDate:date]] && [self isValidStartDate:self.startDate endDate:date]) {
        _endDate = date;
        [self updateScheduleDate];
    }
}

- (BOOL)isValidStartDate:(NSDate *)startDate endDate:(NSDate *)endDate {
    if (![self.startDateButton.titleLabel.text length] || ![self.startTimeButton.titleLabel.text length] || ![self.endDateButton.titleLabel.text length] || ![self.endTimeButton.titleLabel.text length]) {
        return YES;
    }
    return [self isValidStartDate:startDate endDate:endDate withWarning:YES];
}

- (BOOL)isValidStartDate:(NSDate *)startDate endDate:(NSDate *)endDate withWarning:(BOOL)warning{
    BOOL isValid = [startDate ?: [NSDate distantPast] compare:endDate ?: [NSDate distantFuture]] == NSOrderedAscending;
    if (!isValid && warning) {
        [self flashAnimationComponent:@[self.startDateButton, self.startTimeButton, self.endDateButton, self.endTimeButton] completion:^(BOOL finished) {
            [SVProgressHUD showInfoWithStatus:AMLocalizedString(@"시작일시가 종료일시보다 나중일수 없습니다.")];
        }];
    }
    return isValid;
}

//- (void)fetchTaskFromDate:(NSDate *)startDate toDate:(NSDate *)endDate show:(BOOL)show {
//    if (!self.isValidTask) {
//        [self beginNetworking];
//        __weak typeof(self) weakSelf = self;
//        [QINetwork taskListBetweenStartDate:stringFromDate(startDate) endDate:stringFromDate(endDate) successBlock:^(NSDictionary *responseDictionary) {
//            weakSelf.taskList = responseDictionary[@"taskList"];
//            if (show) {
//                [weakSelf showTaskActionSheet];
//            }
//            [weakSelf endNetworking];
//            weakSelf.isValidTask = YES;
//        } failureBlock:^(NSError *error) {
//            [weakSelf warnNetworkingWithError:error];
//        } authFailedBlock:^(void (^completionBlock)(void)) {
//            [weakSelf endNetworking];
//            [Navigator modalPresentLoginViewControllerWithCompletionBlock:completionBlock];
//        }];
//    } else if (show) {
//        [self showTaskActionSheet];
//    }
//}
//
//- (void)fetchCommonCodeAndShow:(BOOL)show {
//    if (!self.dutyList) {
//        self.dutyList = @[];
//        [self beginNetworking];
//        __weak typeof(self) weakSelf = self;
//        [QINetwork commonCodeWithGroupCode:@"WORK_TYPE_CD" successBlock:^(NSDictionary *responseDictionary) {
//            weakSelf.dutyList = responseDictionary[@"commCodeList"];
//            if (show) {
//                [weakSelf showDutyActionSheet];
//            }
//            [weakSelf endNetworking];
//        } failureBlock:^(NSError *error) {
//            //weakSelf.dutyList = nil;
//            [weakSelf warnNetworkingWithError:error];
//        }];
//    } else if (show) {
//        [self showDutyActionSheet];
//    }
//}

- (void)updateScheduleDate_s {
    [self.startDateButton setTitle:[self.dateFormatter stringFromDate:self.startDate] forState:UIControlStateNormal];
    [self.startTimeButton setTitle:[self.timeFormatter stringFromDate:self.startDate] forState:UIControlStateNormal];
    [self.endDateButton setTitle:[self.dateFormatter stringFromDate:self.endDate] forState:UIControlStateNormal];
    
    
    self.isValidTask = NO;
    //self.taskList = nil;
    [self.taskButton setTitle:nil forState:UIControlStateNormal];
    [self.taskButton setTitle:nil forState:UIControlStateApplication];
}


- (void)updateScheduleDate {
    [self.startDateButton setTitle:[self.dateFormatter stringFromDate:self.startDate] forState:UIControlStateNormal];
    [self.startTimeButton setTitle:[self.timeFormatter stringFromDate:self.startDate] forState:UIControlStateNormal];
    [self.endDateButton setTitle:[self.dateFormatter stringFromDate:self.endDate] forState:UIControlStateNormal];
    [self.endTimeButton setTitle:[self.timeFormatter stringFromDate:self.endDate] forState:UIControlStateNormal];
    
    self.isValidTask = NO;
    //self.taskList = nil;
    [self.taskButton setTitle:nil forState:UIControlStateNormal];
    [self.taskButton setTitle:nil forState:UIControlStateApplication];
}

- (void)showTaskActionSheet {
    NSArray <NSDictionary *> *taskList = [GlobalVariables singleton].taskList;
    [self showActionSheetForButton:self.taskButton withTitle:self.taskTitleLabel.text from:taskList titleKey:@"taskNm" codeKey:@"id"];
}

- (void)showDutyActionSheet {
    NSArray <NSDictionary *> *dutyList = [GlobalVariables singleton].dutyList;
    [self showActionSheetForButton:self.dutyButton withTitle:self.dutyTitleLabel.text from:dutyList titleKey:@"markName" codeKey:@"codeName"];
}

- (void)showActionSheetForButton:(UIButton *)button withTitle:(NSString *)title from:(NSArray *)array titleKey:(NSString *)titleKey codeKey:(NSString *)codeKey {
    [SVProgressHUD dismiss];
    NSMutableAttributedString *_title = [[NSMutableAttributedString alloc] initWithString:AMLocalizedString(title)];
    [_title addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:20.0] range:NSMakeRange(0, [_title length])];
    
    UIAlertController *__block actionSheet = [UIAlertController alertControllerWithTitle:_title.mutableString message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [actionSheet setValue:_title forKey:@"attributedTitle"];
    
    [array enumerateObjectsUsingBlock:^(NSDictionary * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *titleLabel = [NSString stringWithFormat:@"%@%@", [obj[codeKey] isEqualToString:[button titleForState:UIControlStateApplication]] && !canSetChecked() ? @"✓ " : @"", obj[titleKey]];
        UIAlertAction *action = [UIAlertAction actionWithTitle:titleLabel style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [button setTitle:obj[titleKey] forState:UIControlStateNormal];
            [button setTitle:obj[codeKey] forState:UIControlStateApplication];
        }];
        setCheckedOnAlertAction(action, [obj[codeKey] isEqualToString:[button titleForState:UIControlStateApplication]]);
        [actionSheet addAction:action];
    }];
    [actionSheet addAction:[UIAlertAction actionWithTitle:AMLocalizedString(@"취소") style:UIAlertActionStyleCancel handler:nil]];
    [self setModalPresentationStyle: UIModalPresentationFullScreen];
    [self presentViewController:actionSheet animated:YES completion:nil];
}

//- (void)flashAnimationComponent:(NSArray<UIView*>*)components completion:(void(^)(BOOL finished))completion {
//    NSArray <UIView *>*__block array = [components copy];
//    if ([components count]) {
//        feedbackNotificationError();
//        [UIView animateWithDuration:0.15f delay:0.0f usingSpringWithDamping:0.75f initialSpringVelocity:0.0f options:UIViewAnimationOptionAllowUserInteraction|UIViewAnimationOptionBeginFromCurrentState animations:^{
//            for (UIView *view in array) {
//                view.layer.cornerRadius = view.frame.size.height / 3.0f;
//                view.layer.backgroundColor = [ColorRGBA(198, 141, 63, 0.2) CGColor];
//            }
//        } completion:^(BOOL finished) {
//            [UIView animateWithDuration:0.15f delay:0.15f usingSpringWithDamping:0.75f initialSpringVelocity:0.0f options:UIViewAnimationOptionAllowUserInteraction|UIViewAnimationOptionBeginFromCurrentState animations:^{
//                for (UIView *view in array) {
//                    view.layer.cornerRadius = 0.0f;
//                    view.layer.backgroundColor = [ColorRGBA(255, 255, 255, 1) CGColor];
//                }
//            } completion:completion];
//        }];
//    }
//}

- (IBAction)dateButtonDidPress:(UIButton *)sender {
    feedbackSelection();
    BOOL isStart = sender == self.startDateButton;
    NSString *title = isStart ? self.startDateTitleLabel.text : self.endDateTitleLabel.text;
    __block NSDate *date = isStart ? self.startDate : self.endDate;
    
    __weak typeof(self) weakSelf = self;
    RMDateSelectionViewController *dateSelectionController = [RMDateSelectionViewController actionControllerWithStyle:RMActionControllerStyleDefault];
    dateSelectionController.title = title;
    RMAction<UIDatePicker *> *selectAction = [RMAction<UIDatePicker *> actionWithTitle:AMLocalizedString(@"선택") style:RMActionStyleDone andHandler:^(RMActionController<UIDatePicker *> *controller) {
        if (isStart) {
            [weakSelf setStartDate:controller.contentView.date];
        } else {
            [weakSelf setEndDate:controller.contentView.date];
        }
    }];
    RMAction<UIDatePicker *> *cancelAction = [RMAction<UIDatePicker *> actionWithTitle:AMLocalizedString(@"취소") style:RMActionStyleCancel andHandler:nil];
    [dateSelectionController addAction:selectAction];
    [dateSelectionController addAction:cancelAction];
    //You can access the actual UIDatePicker via the datePicker property
    dateSelectionController.datePicker.datePickerMode = UIDatePickerModeDate;
    //MARK: - iOS14에서는 기본으로 Datepicker 스타일이 auto, 그러면 아래 링크에서 설명한 화면 처럼 나옴. 이것을 기존 wheels 스타일로 설정해야 함.
    //https://zeddios.tistory.com/1066
    if (@available(iOS 13.4, *)) {
        dateSelectionController.datePicker.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    dateSelectionController.datePicker.date = date;
    dateSelectionController.disableBlurEffectsForActions = YES;
    dateSelectionController.disableBlurEffectsForContentView = YES;
    dateSelectionController.disableBouncingEffects = YES;
    [self setModalPresentationStyle: UIModalPresentationFullScreen];
    [self presentViewController:dateSelectionController animated:YES completion:nil];
}

- (IBAction)timeButtonDidPress:(UIButton *)sender {
    feedbackSelection();
    BOOL isStart = sender == self.startTimeButton;
    NSString *title = isStart ? self.startDateTitleLabel.text : self.endDateTitleLabel.text;
    __block NSDate *date = isStart ? self.startDate : self.endDate;
    
    __weak typeof(self) weakSelf = self;
    RMDateSelectionViewController *dateSelectionController = [RMDateSelectionViewController actionControllerWithStyle:RMActionControllerStyleDefault];
    dateSelectionController.title = title;
    RMAction<UIDatePicker *> *selectAction = [RMAction<UIDatePicker *> actionWithTitle:AMLocalizedString(@"선택") style:RMActionStyleDone andHandler:^(RMActionController<UIDatePicker *> *controller) {
        if (isStart) {
            [weakSelf setStartTime:controller.contentView.date];
        } else {
            [weakSelf setEndTime:controller.contentView.date];
        }
    }];
    RMAction<UIDatePicker *> *cancelAction = [RMAction<UIDatePicker *> actionWithTitle:AMLocalizedString(@"취소") style:RMActionStyleCancel andHandler:nil];
    [dateSelectionController addAction:selectAction];
    [dateSelectionController addAction:cancelAction];
    //You can access the actual UIDatePicker via the datePicker property
    dateSelectionController.datePicker.datePickerMode = UIDatePickerModeTime;
    //MARK: - iOS14에서는 기본으로 Datepicker 스타일이 auto, 그러면 아래 링크에서 설명한 화면 처럼 나옴. 이것을 기존 wheels 스타일로 설정해야 함.
    //https://zeddios.tistory.com/1066
    if (@available(iOS 13.4, *)) {
        dateSelectionController.datePicker.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    dateSelectionController.datePicker.minuteInterval = 30;
    dateSelectionController.datePicker.date = date;
    dateSelectionController.disableBlurEffectsForActions = YES;
    dateSelectionController.disableBlurEffectsForContentView = YES;
    dateSelectionController.disableBouncingEffects = YES;
    [self setModalPresentationStyle: UIModalPresentationFullScreen];
    [self presentViewController:dateSelectionController animated:YES completion:nil];
}

- (IBAction)taskButtonDidPress:(UIButton *)sender {
    feedbackSelection();
    //[self fetchTaskFromDate:self.startDate toDate:self.endDate show:YES];
    [self showTaskActionSheet];
}

- (IBAction)dutyButtonDidPress:(UIButton *)sender {
    feedbackSelection();
    //[self fetchCommonCodeAndShow:YES];
    [self showDutyActionSheet];
}

- (IBAction)cancelButtonDidPress:(id)sender {
    feedbackNotificationWarning();
    if ([self isNew]) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:AMLocalizedString(@"삭제") message:AMLocalizedString(@"정말 삭제 하시겠습니까?") preferredStyle:UIAlertControllerStyleActionSheet];
        __weak typeof(self) weakSelf = self;
        [actionSheet addAction:[UIAlertAction actionWithTitle:AMLocalizedString(@"삭제") style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            [SVProgressHUD show];
            [QINetwork removeScheduleByID:weakSelf.scheduleID successBlock:^(NSDictionary *responseDictionary) {
                [SVProgressHUD dismiss];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:SCHEDULE_DID_UPDATE_ON_DATE object:stringFromDate(weakSelf.startDate)];
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                });
            } failureBlock:^(NSError *error) {
                [SVProgressHUD showErrorWithStatus:error.localizedDescription];
            } authFailedBlock:^(void (^completionBlock)(void)) {
                [SVProgressHUD dismiss];
                //[Navigator modalPresentLoginViewControllerWithCompletionBlock:completionBlock];
                [Navigator toMainViewControllerAfterRetryLogin];
            }];
        }]];
        [actionSheet addAction:[UIAlertAction actionWithTitle:AMLocalizedString(@"취소") style:UIAlertActionStyleCancel handler:nil]];
        [self setModalPresentationStyle: UIModalPresentationFullScreen];
        [self presentViewController:actionSheet animated:YES completion:nil];
    }
}

- (IBAction)saveButtonDidPress:(id)sender {
    feedbackNotificationSuccess();
    
    [self.view endEditing:YES];
    
    if ([self isValidSchedule]) {
        __weak typeof(self) weakSelf = self;
        UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:AMLocalizedString(@"등록") message:AMLocalizedString(@"등록 하시겠습니까?") preferredStyle:UIAlertControllerStyleActionSheet];
        [actionSheet addAction:[UIAlertAction actionWithTitle:AMLocalizedString(@"등록") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [SVProgressHUD show];
            [QINetwork saveScheduleWithDictionary:[weakSelf scheduleDictionary] successBlock:^(NSDictionary *responseDictionary) {
                [SVProgressHUD showSuccessWithStatus:AMLocalizedString(@"등록되었습니다.")];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:SCHEDULE_DID_UPDATE_ON_DATE object:stringFromDate([weakSelf.startDate copy])];
                    [Navigator toMainViewControllerWithoutUIAnimation];
                    [Navigator toScheduleListViewController:NO date:weakSelf.startDate];
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                });
            } failureBlock:^(NSError *error) {
                [SVProgressHUD showErrorWithStatus:error.localizedDescription];
            } authFailedBlock:^(void (^completionBlock)(void)) {
                [SVProgressHUD dismiss];
                //[Navigator modalPresentLoginViewControllerWithCompletionBlock:completionBlock];
                [Navigator toMainViewControllerAfterRetryLogin];
            }];
        }]];
        [actionSheet addAction:[UIAlertAction actionWithTitle:AMLocalizedString(@"취소") style:UIAlertActionStyleCancel handler:nil]];
        [self setModalPresentationStyle: UIModalPresentationFullScreen];
        [self presentViewController:actionSheet animated:YES completion:nil];
    }
}

- (NSDate *)getDateFromString:(NSString *)string {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY.MM.dd HH:mm"];
    NSDate *date = [dateFormatter dateFromString:string];
    return date;
}

- (BOOL)isValidSchedule {
    self.saveButton.enabled = NO;
    
    NSMutableArray<UIView *> *flashComponents = [NSMutableArray array];
    NSMutableArray<NSString *> *notiStrings = [NSMutableArray array];
    
    _startDate = [self getDateFromString:[NSString stringWithFormat:@"%@ %@", self.startDateButton.titleLabel.text, self.startTimeButton.titleLabel.text]];
    _endDate = [self getDateFromString:[NSString stringWithFormat:@"%@ %@", self.endDateButton.titleLabel.text, self.endTimeButton.titleLabel.text]];
    //NSLog(@"self.startDate: %@, self.endDate: %@", self.startDate, self.endDate);
    
    if (![self isValidStartDate:self.startDate endDate:self.endDate withWarning:NO]) {
        [flashComponents addObject:self.endDateButton];
        [notiStrings addObject:AMLocalizedString(@"시작일시가 종료일시보다 나중일수 없습니다.")];
    }
    
    if (![[self.taskButton titleForState:UIControlStateApplication] length]) {
        [flashComponents addObject:self.taskButton];
        [notiStrings addObject:AMLocalizedString(@"Task 를 선택하세요.")];
    }
    
    if (![[self.dutyButton titleForState:UIControlStateApplication] length]) {
        [flashComponents addObject:self.dutyButton];
        [notiStrings addObject:AMLocalizedString(@"업무유형을 선택하세요.")];
    }
    
    NSString *notiMessage = [notiStrings componentsJoinedByString:@"\n"];
    if ([notiMessage length]) {
        [SVProgressHUD showInfoWithStatus:notiMessage];
    }
    
    if ([flashComponents count]) {
        __weak typeof(self) weakSelf = self;
        [self flashAnimationComponent:flashComponents completion:^(BOOL finished) {
            weakSelf.saveButton.enabled = YES;
        }];
        
        return NO;
    }
    
    self.saveButton.enabled = YES;
    return YES;
}

- (NSDictionary *)scheduleDictionary {
    return @{
        @"id" : self.scheduleID ?: [NSNull null],
        @"startDt" : stringFromDate(self.startDate) ?: [NSNull null],
        @"startTime" : stringTimeFromDate(self.startDate) ?: [NSNull null],
        @"endDt" : stringFromDate(self.endDate) ?: [NSNull null],
        @"endTime" : stringTimeFromDate(self.endDate) ?: [NSNull null],
        @"taskId" : [self.taskButton titleForState:UIControlStateApplication] ?: [NSNull null],
        @"dutyCd" : [self.dutyButton titleForState:UIControlStateApplication] ?: [NSNull null],
        @"wrkDesc" : [self.descTextView text] ?: [NSNull null],
        @"wrkComnt" : [self.comntTextView text] ?: [NSNull null],
    };
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if (textView == self.descTextView) {
        BOOL shouldChange = YES;
        NSUInteger newLength = [textView.text length] + [text length] - range.length;
        
        NSUInteger maxLength = MAX_DESCRIPTION_LENGTH;
        
        if([text length] && newLength > maxLength)
        {
            shouldChange = NO;
        } else {
            [self updateTextViewLabel:[textView.text stringByReplacingCharactersInRange:range withString:text]];
        }
        
        return shouldChange;
    }
    
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    feedbackSelection();
    self.descTitleLabel.textColor = [textView text].length ? ColorRGBA(122, 172, 53, 1) : ColorRGBA(153, 153, 153, 1);
    [self updateTextViewLabel:[textView text]];
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    self.descTitleLabel.textColor = ColorRGBA(153, 153, 153, 1);
    [self updateTextViewLabel:[textView text]];
}

- (void)updateTextViewLabel:(NSString *)text {
    if ([text length] && self.descTitleVertical.constant != 40.0f) {
        __weak typeof(self) weakSelf = self;
        [UIView animateWithDuration:0.35f animations:^{
            weakSelf.descTitleLabel.textColor = ColorRGBA(122, 172, 53, 1);
            weakSelf.descTitleLabel.text = AMLocalizedString(@"근무상세");
            weakSelf.descTitleVertical.constant = 40.0f;
            weakSelf.descTitleHorizontal.constant = 20.0f;
            [weakSelf.view layoutIfNeeded];
        }];
    } else if (![text length] && self.descTitleVertical.constant != 80.0f) {
        __weak typeof(self) weakSelf = self;
        [UIView animateWithDuration:0.35f animations:^{
            weakSelf.descTitleLabel.textColor = ColorRGBA(153, 153, 153, 1);
            weakSelf.descTitleLabel.text = AMLocalizedString(@"근무상세를 입력하세요.");
            weakSelf.descTitleVertical.constant = 78.0f;
            weakSelf.descTitleHorizontal.constant = 25.0f;
            [weakSelf.view layoutIfNeeded];
        }];
    }
    
    CGRect rect = [self.descTextView.attributedText boundingRectWithSize:CGSizeMake(self.descTextView.frame.size.width, FLT_MAX) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    CGFloat height = ceilf(MIN(MAX(CGRectGetHeight(rect), self.descTextView.font.lineHeight +  self.descTextView.textContainer.lineFragmentPadding + self.descTextView.textContainerInset.top + self.descTextView.textContainerInset.bottom), (self.descTextView.font.lineHeight) * 3 + self.descTextView.textContainer.lineFragmentPadding +  self.descTextView.textContainerInset.top + self.descTextView.textContainerInset.bottom));
    if (ceilf(self.descTextViewHeight.constant) != height) {
        __weak typeof(self) weakSelf = self;
        weakSelf.descTextView.scrollEnabled = NO;
        [UIView animateWithDuration:0.35f animations:^{
            weakSelf.descTextViewHeight.constant = height;
            [weakSelf.view layoutIfNeeded];
            weakSelf.descTextView.scrollEnabled = YES;
        }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
