//
//  O365ScheduleViewModel.h
//  management
//
//  Created by 이주한 on 2020/05/20.
//  Copyright © 2020 Pulmuone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <IGListKit/IGListKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface O365ScheduleViewModel : NSObject <IGListDiffable>
@property (nonatomic, strong, readonly) NSString *identifier;
@property (nonatomic, strong, readonly) NSString *startDt;
@property (nonatomic, strong, readonly) NSString *startTime;
@property (nonatomic, strong, readonly) NSString *endDt;
@property (nonatomic, strong, readonly) NSString *endTime;
//@property (nonatomic, strong, readonly) NSString *taskNm;
//@property (nonatomic, strong, readonly) NSString *dutyCdNm;
//@property (nonatomic, strong, readonly) NSString *wrkComnt;
@property (nonatomic, strong, readonly) NSString *title;
@property (nonatomic, strong) NSString *tastId;
@property (nonatomic, strong) NSString *dutyCd;
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, assign) Boolean isChecked;

+ (instancetype)o365ScheduleViewModelFromSchedule:(NSDictionary *)schedule;
@end

NS_ASSUME_NONNULL_END
