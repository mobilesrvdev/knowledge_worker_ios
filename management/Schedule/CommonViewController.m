//
//  CommonViewController.m
//  management
//
//  Created by 이주한 on 2020/05/08.
//  Copyright © 2020 Pulmuone. All rights reserved.
//

#import "CommonViewController.h"
#import "ScheduleViewModel.h"
#import "SVProgressHUD.h"
#import "QINetwork.h"

@interface CommonViewController ()

@property (nonatomic, strong) UIAlertController *commonPopup; // 공통 팝업

@end

@implementation CommonViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)flashAnimationComponent:(NSArray<UIView*>*)components completion:(void(^)(BOOL finished))completion {
    NSArray <UIView *>*__block array = [components copy];
    if ([components count]) {
        feedbackNotificationError();
        [UIView animateWithDuration:0.15f delay:0.0f usingSpringWithDamping:0.75f initialSpringVelocity:0.0f options:UIViewAnimationOptionAllowUserInteraction|UIViewAnimationOptionBeginFromCurrentState animations:^{
            for (UIView *view in array) {
                view.layer.cornerRadius = view.frame.size.height / 3.0f;
                view.layer.backgroundColor = [ColorRGBA(198, 141, 63, 0.2) CGColor];
            }
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.15f delay:0.15f usingSpringWithDamping:0.75f initialSpringVelocity:0.0f options:UIViewAnimationOptionAllowUserInteraction|UIViewAnimationOptionBeginFromCurrentState animations:^{
                for (UIView *view in array) {
                    view.layer.cornerRadius = 0.0f;
                    view.layer.backgroundColor = [ColorRGBA(255, 255, 255, 1) CGColor];
                }
            } completion:completion];
        }];
    }
}

- (void)beginNetworking {
    self.networkActivityCount++;
    self.networkActivityCountMax = MAX(self.networkActivityCountMax, self.networkActivityCount);
    if (self.networkActivityCount > 0) {
        if (self.networkActivityCountMax == self.networkActivityCount) {
            [SVProgressHUD show];
        } else {
            [SVProgressHUD showProgress:(self.networkActivityCountMax - self.networkActivityCount)/(float)self.networkActivityCountMax];
        }
    }
}

- (void)endNetworking {
    self.networkActivityCount--;
    if ([self.failedNetworkMessages count]) {
        [[QINetwork manager] cancel];
        NSString *__block failedMessage = @"";
        NSUInteger lastIndex = [self.failedNetworkMessages count] - 1;
        [self.failedNetworkMessages enumerateObjectsUsingBlock:^(NSError * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            failedMessage = [failedMessage stringByAppendingString:obj.localizedDescription];
            if (idx != lastIndex) {
                failedMessage = [failedMessage stringByAppendingString:@"\n"];
            }
        }];
        [SVProgressHUD showErrorWithStatus:failedMessage];
        UIViewController *viewController = topMostController();
        [viewController.navigationController popViewControllerAnimated:YES];
        self.networkActivityCountMax = 0;
        //self.failedNetworkMessages = nil;
    } else if ([self.warningNetworkMessages count]) {
        NSString *__block failedMessage = @"";
        NSUInteger lastIndex = [self.warningNetworkMessages count] - 1;
        [self.warningNetworkMessages enumerateObjectsUsingBlock:^(NSError * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            failedMessage = [failedMessage stringByAppendingString:obj.localizedDescription];
            if (idx != lastIndex) {
                failedMessage = [failedMessage stringByAppendingString:@"\n"];
            }
        }];
        [SVProgressHUD showErrorWithStatus:failedMessage];
        self.networkActivityCountMax = 0;
        //self.warningNetworkMessages = nil;
    } else if (self.networkActivityCount > 0) {
        [SVProgressHUD showProgress:(self.networkActivityCountMax - self.networkActivityCount)/(float)self.networkActivityCountMax];
    } else {
        [SVProgressHUD dismiss];
        self.networkActivityCountMax = 0;
    }
}

- (void)failedNetworkingWithError:(NSError *)error {
    self.failedNetworkMessages = self.failedNetworkMessages ?: [NSMutableArray array];
    [self.failedNetworkMessages addObject:error];
    [self endNetworking];
}

- (void)warnNetworkingWithError:(NSError *)error {
    self.warningNetworkMessages = self.warningNetworkMessages ?: [NSMutableArray array];
    [self.warningNetworkMessages addObject:error];
    [self endNetworking];
}

- (void)showCommonAlertWithTitle:(NSString *)title message:(NSString *)msg
                      btnOneName:(NSString *)btnOneTitle btnOneAction:(void (^)(void))btnOneAction
                      btnTwoName:(nullable NSString *)btnTwoTitle btnTwoAction:(void (^)(void))btnTwoAction {
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.commonPopup) {
            [self.commonPopup dismissViewControllerAnimated:false completion:nil];
        }
        self.commonPopup = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
        [self.commonPopup addAction:[UIAlertAction actionWithTitle:btnOneTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            btnOneAction();
        }]];
        if (btnTwoTitle != nil) {
            [self.commonPopup addAction:[UIAlertAction actionWithTitle:btnTwoTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                btnTwoAction();
            }]];
        }
        [weakSelf setModalPresentationStyle:UIModalPresentationFullScreen];
        [weakSelf presentViewController:self.commonPopup animated:YES completion:nil];
    });
}

- (void)dismissCommonAlert {
    if (self.commonPopup) {
        [self.commonPopup dismissViewControllerAnimated:false completion:nil];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
