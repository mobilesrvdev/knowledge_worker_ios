//
//  O365Schedule.m
//  management
//
//  Created by 이주한 on 2020/05/18.
//  Copyright © 2020 Pulmuone. All rights reserved.
//

#import "O365Schedule.h"

@implementation O365Schedule

+ (instancetype)o365ScheduleFromDictionary:(NSDictionary *)dictionary {
    return [[[self class] alloc] initWithDictionary:dictionary];
}

- (instancetype)init {
    if (self = [self initWithDictionary:nil]) {
        
    }
    return self;
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (self = [super init]) {
        _identifier = [[NSUUID UUID] UUIDString];//dictionary[@"id"];//[[NSUUID UUID] UUIDString];
        //NSLog(@"_identifier: %@", _identifier);
        _title = dictionary[@"title"];
        _startDt = dictionary[@"startDt"];
        if ([_startDt length] != 8) {
            DDLogError(@"startDt 8자리 아님: %@", _startDt);
        }
        _startTime = dictionary[@"startTime"];
        if ([_startTime length] != 4) {
           DDLogError(@"startTime 4자리 아님: %@", _startTime);
        }
        _endDt = dictionary[@"endDt"];
        if ([_endDt length] != 8) {
            DDLogError(@"endDt 8자리 아님: %@", _endDt);
        }
        _endTime = dictionary[@"endTime"];
        if ([_endTime length] != 4) {
            DDLogError(@"endTime 4자리 아님: %@", _endTime);
        }
    }
    return self;
}

- (NSDictionary *)dictionary {
    return @{
             @"id" : self.identifier ?: @"",
             @"title" : self.title ?: @"",
             @"startDt" : self.startDt ?: @"",
             @"startTime" : self.startTime ?: @"",
             @"endDt" : self.endDt ?: @"",
             @"endTime" : self.endTime ?: @"",
             };
}

- (nonnull id<NSObject>)diffIdentifier {
    return self.identifier;
}

- (BOOL)isEqualToDiffableObject:(nullable id<IGListDiffable>)object {
    return [[(NSObject *)object class] isKindOfClass:[self class]]
        && [self.identifier isEqualToString:[(O365Schedule *)object identifier]]
        && [self.startDt isEqualToString:[(O365Schedule *)object startDt]]
        && [self.startTime isEqualToString:[(O365Schedule *)object startTime]]
        && [self.endDt isEqualToString:[(O365Schedule *)object endDt]]
        && [self.endTime isEqualToString:[(O365Schedule *)object endTime]]
        && [self.title isEqualToString:[(O365Schedule *)object title]]
    ;
}

@end
