//
//  O365Schedule.h
//  management
//
//  Created by 이주한 on 2020/05/18.
//  Copyright © 2020 Pulmuone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <IGListKit/IGListKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface O365Schedule : NSObject <IGListDiffable>
@property (nonatomic, strong, readonly) NSString *identifier;
@property (nonatomic, strong, readonly) NSString *title;
@property (nonatomic, strong, readonly) NSString *startDt;
@property (nonatomic, strong, readonly) NSString *startTime;
@property (nonatomic, strong, readonly) NSString *endDt;
@property (nonatomic, strong, readonly) NSString *endTime;

+ (instancetype)o365ScheduleFromDictionary:(NSDictionary *)dictionary;
- (NSDictionary *)dictionary;

@end

NS_ASSUME_NONNULL_END
