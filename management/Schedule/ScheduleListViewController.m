//
//  ScheduleListViewController.m
//  management
//
//  Created by Quintet on 27/06/2018.
//  Copyright © 2018 Pulmuone. All rights reserved.
//

#import "ScheduleListViewController.h"
#import "ComponentStyler.h"
#import "ScheduleSectionController.h"
#import "Schedule.h"
#import "ScheduleCell.h"
#import "O365Schedule.h"
#import "O365ScheduleViewModel.h"
#import "O365ScheduleCell.h"
#import "Navigator.h"
#import "QINetwork+Pulmuone.h"
#import "SVProgressHUD.h"
#import <FSCalendar/FSCalendar.h>
#import <IGListKit/IGListKit.h>

/**
 
 Show schedule on date
 Show no-content if there is any
 Navigation to detail
 Refresh on visible. (back or chaning date)
 
 **/

@interface ScheduleListViewController () <IGListAdapterDataSource, FSCalendarDataSource, FSCalendarDelegate, UIGestureRecognizerDelegate>
@property (nonatomic, strong) IBOutlet UIView *headerView;
@property (nonatomic, strong) IBOutlet UIView *emptyView;
@property (nonatomic, strong) IBOutlet UILabel *emptyTitleLabel;
@property (nonatomic, strong) IBOutlet UILabel *emptyDetailLabel;
@property (nonatomic, strong) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) IGListAdapter *listAdapter;
@property (nonatomic, strong) IBOutlet UILabel *dateLabel;
@property (nonatomic, strong) IBOutlet UIButton *prevButton;
@property (nonatomic, strong) IBOutlet UIButton *nextButton;
@property (nonatomic, strong) IBOutlet UIButton *addButton;
@property (nonatomic, strong) IBOutlet FSCalendar *calendar;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *calendarHeightConstraint;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *headerVerticalSpacingConstraint;
@property (strong, nonatomic) UIPanGestureRecognizer *scopeGesture;
@property (nonatomic, strong) UIRefreshControl *__block refreshControl;
@property (nonatomic, strong) NSMutableArray<NSObject <IGListDiffable>*> *scheduleList;
@property (nonatomic) BOOL isLoaded;
@property (nonatomic) NSUInteger requestIndex;
@end

@implementation ScheduleListViewController
@synthesize targetDate = _targetDate;

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = self.isO365 ? AMLocalizedString(@"Office365 일정") : AMLocalizedString(@"내 일정 조회");
    
    if (self.isO365) {
        //초기화, 전에 체크된 항목이 있을 수 있으니
        [[GlobalVariables singleton].checked365Schedules removeAllObjects];
    }
    
    NSLog(@"[ScheduleListViewController] %s, isO365: %@", __PRETTY_FUNCTION__, self.isO365 ? @"YES" : @"NO");
    
    [self initializeSubviews];
    [self initializeCollectionView];
    [self registerNotificationForUpdate];
    
    [self updateTargetDate];
}

- (void)initializeSubviews {
    [ComponentStyler defaultNavigationBar:self.navigationController.navigationBar];
    [ComponentStyler scheduleHeaderDateLabel:self.dateLabel];
    [ComponentStyler scheduleAddButton:self.addButton];
    
    [self configureNavRightButton];
    [self configureCalendar];
    [self configureEmptyView];
    [self configureBlurView];
    [self configureGridentView];
    [self configureRefresher];
}

- (void)initializeCollectionView {
    self.listAdapter = [[IGListAdapter alloc] initWithUpdater:[IGListAdapterUpdater new]
                                               viewController:self
                                             workingRangeSize:0];
    
    self.listAdapter.dataSource = self;
    self.listAdapter.collectionView = self.collectionView;
    
    self.collectionView.delaysContentTouches = NO;
    
    if (@available(iOS 10.0, *)) {
        self.collectionView.prefetchingEnabled = NO;
    } else {
        // Fallback on earlier versions
    }
}

- (void)registerNotificationForUpdate {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(scheduleDidUpdate:) name:SCHEDULE_DID_UPDATE_ON_DATE object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeSchedule:) name:SCHEDULE_DID_REMOVE object:nil];
}

- (void)configureNavRightButton {
    [self.addButton setTitle:self.isO365 ? AMLocalizedString(@"등록") : AMLocalizedString(@"추가") forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.addButton];
    [self.addButton invalidateIntrinsicContentSize];
    [self.addButton setNeedsLayout];
}

- (void)configureCalendar {
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dateLabelDidTap:)];
    [self.dateLabel addGestureRecognizer:tapGestureRecognizer];
    
    UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(dateLabelDidLongPress:)];
    longPressGestureRecognizer.minimumPressDuration = 1.0f;
    [self.dateLabel addGestureRecognizer:longPressGestureRecognizer];
    
    self.dateLabel.userInteractionEnabled = YES;
    
    [[self.calendar appearance] setHeaderTitleColor:ColorRGBA(141, 198, 63, 1)];
    [[self.calendar appearance] setWeekdayTextColor:ColorRGBA(31, 70, 32, 1)];
    [[self.calendar appearance] setTitleWeekendColor:ColorRGBA(198, 141, 63, 1)];
    [self.calendar selectDate:self.targetDate scrollToDate:YES];
    
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self.calendar action:@selector(handleScopeGesture:)];
    panGesture.delegate = self;
    panGesture.minimumNumberOfTouches = 1;
    panGesture.maximumNumberOfTouches = 2;
    [self.headerView addGestureRecognizer:panGesture];
    self.scopeGesture = panGesture;
    
    // While the scope gesture begin, the pan gesture of tableView should cancel.
    [self.collectionView.panGestureRecognizer requireGestureRecognizerToFail:panGesture];
    
    self.calendar.scope = FSCalendarScopeWeek;
    self.calendar.alpha = 0.0f;
    self.headerVerticalSpacingConstraint.constant = 0;
    [self.view layoutIfNeeded];
}

- (void)configureEmptyView {
    self.emptyView.backgroundColor = ColorRGBA(247, 247, 247, 1);
    self.emptyTitleLabel.text = AMLocalizedString(@"조회내역이 없습니다.");
    self.emptyDetailLabel.text = AMLocalizedString(@"내 일정 등록에서 등록해주세요.");
    
    [ComponentStyler scheduleEmptyTitleLabel:self.emptyTitleLabel];
    [ComponentStyler scheduleEmptyDetailLabel:self.emptyDetailLabel];
}

- (void)configureBlurView {
    //only apply the blur if the user hasn't disabled transparency effects
    if (!UIAccessibilityIsReduceTransparencyEnabled()) {
        self.headerView.backgroundColor = [UIColor clearColor];
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        
        blurEffectView.frame = self.headerView.bounds;
        blurEffectView.translatesAutoresizingMaskIntoConstraints = NO;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self.headerView insertSubview:blurEffectView atIndex:0];
    }
}

- (void)configureGridentView {
    CAGradientLayer *gradient = [CAGradientLayer new];
    gradient.frame = CGRectMake(0, 0, self.view.frame.size.width, self.headerView.frame.size.height);
    gradient.colors = @[
                        (id)[UIColor blackColor].CGColor,
                        (id)[UIColor blackColor].CGColor,
                        (id)[UIColor clearColor].CGColor,
                        ];
    gradient.locations = @[
                           @(0),
                           @(0.7),
                           @(1),
                           ];
    self.headerView.layer.mask = gradient;
}

- (void)configureRefresher {
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(fetchSchedule) forControlEvents:UIControlEventValueChanged];
    
    if (@available(iOS 10.0, *)) {
        [self.collectionView setRefreshControl:self.refreshControl];
    } else {
        // Fallback on earlier versions
        [self.collectionView addSubview:self.refreshControl];
    }
    [self.refreshControl layoutIfNeeded];
}

- (void)scheduleDidUpdate:(NSNotification *)notification {
    NSLog(@"notification : %@", notification);
    if ([[notification object] isKindOfClass:[NSString class]] && [stringFromDate(self.targetDate) isEqualToString:(NSString *)[notification object]]) {
        [self fetchSchedule];
    }
}

- (void)updateTargetDate {
    NSLog(@"updateTargetDate");
    NSString *dateString = stringFromDateForDisplay(self.targetDate);
    if (![self.dateLabel.text isEqualToString:dateString]) {
        self.dateLabel.text = dateString;
        [self fetchSchedule];
    }
}

- (NSDate *)targetDate {
    if (!_targetDate) {
        _targetDate = [NSDate date];
    }
    return _targetDate;
}

- (void)setTargetDate:(NSDate *)targetDate {
    _targetDate = targetDate;
    NSLog(@"targetDate: %@", targetDate);
    [self.calendar selectDate:targetDate scrollToDate:YES];
    [self updateTargetDate];
}

- (void)fetchSchedule {
    NSLog(@"fetchSchedule");
    if (self.isO365) {
        NSString *email = [GlobalVariables singleton].email;
        [self fetchO365ScheduleOnDate:stringFromDate(self.targetDate) email:email];
        //[self fetchO365ScheduleOnDateForTest:stringFromDate(self.targetDate) email:email];
    } else {
        [self fetchScheduleOnDate:stringFromDate(self.targetDate)];
    }
}

- (void)enableDiableSelectDateButtons:(Boolean)isEanbled {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.nextButton setEnabled:isEanbled];
        [self.prevButton setEnabled:isEanbled];
    });
}

#pragma mark - only test
//NSURLSessionDataTask *dataTask;
//- (void)fetchO365ScheduleOnDateForTest:(NSString *)dateString email:(NSString *)email {
//    [self enableDiableSelectDateButtons:NO];
//
//    self.isLoaded = NO;
//    [self.scheduleList removeAllObjects];
//    [[GlobalVariables singleton].checked365Schedules removeAllObjects];
//
//    __weak typeof(self) weakSelf = self;
//    [self.listAdapter performUpdatesAnimated:YES completion:^(BOOL finished) {
//       if (!weakSelf.refreshControl.isRefreshing) {
//           weakSelf.refreshControl.hidden = NO;
//           [weakSelf.refreshControl layoutIfNeeded];
//           [weakSelf.refreshControl beginRefreshing];
//       }
//    }];
//
//    weakSelf.requestIndex++;
//    NSUInteger __block currentRequest = weakSelf.requestIndex;
//
//    NSError *error;
//
//    // Create the URLSession on the default configuration
//    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
//
//    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration];
//    // Setup the request with URL
//    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", SERVER_URL, URL_PATH_O365_SCHEDULE_LIST]];
//    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
//    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
//    // Convert POST string parameters to data using UTF8 Encoding
//    //NSString *postParams = [NSString stringWithFormat:@"email=%@&searchDate=%@", email, dateString];
//    //NSData *postData = [postParams dataUsingEncoding:NSUTF8StringEncoding];
//
//    NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: email, @"email", dateString, @"searchDate", nil];
//    NSData *postData = [NSJSONSerialization dataWithJSONObject:mapData options:0 error:&error];
//
//    // Convert POST string parameters to data using UTF8 Encoding
//    [urlRequest setHTTPMethod:@"POST"];
//    [urlRequest setHTTPBody:postData];
//
//    if (dataTask != nil && dataTask.state == NSURLSessionTaskStateRunning) {
//        NSLog(@"dataTask cancel");
//        [dataTask cancel];
//    }
//    // Create dataTask
//    dataTask = [defaultSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//        // Handle your response here
//        //NSLog(@"response: %@", response);
//        NSLog(@"data: %@", data);
//        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
//        if(httpResponse.statusCode == 200) {
//            NSError *parseError = nil;
//            NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
//            NSLog(@"The response is - %@", responseDictionary);
//
//            if (![responseDictionary[@"o365ScheduleList"] isKindOfClass:[NSArray class]]) {
//                feedbackNotificationError();
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    NSError *error = [QINetwork manager].generateError(nil, nil);
//                    [SVProgressHUD showErrorWithStatus:error.localizedDescription];
//                    [weakSelf.refreshControl endRefreshing];
//                });
//                return ;
//            }
//
//            NSMutableArray *__block schedules = [NSMutableArray array];
//            [responseDictionary[@"o365ScheduleList"] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//                [schedules addObject:[O365Schedule o365ScheduleFromDictionary:obj]];
//            }];
//
//            dispatch_async(dispatch_get_main_queue(), ^{
//                if (weakSelf.refreshControl.isRefreshing && currentRequest >= weakSelf.requestIndex) {
//                    [weakSelf.scheduleList addObjectsFromArray:[schedules copy]];
//                    [weakSelf.listAdapter performUpdatesAnimated:YES completion:^(BOOL finished) {
//                        feedbackNotificationSuccess();
//                    }];
//                    [weakSelf.refreshControl endRefreshing];
//                    weakSelf.isLoaded = YES;
//                }
//            });
//        }
//        else {
//            NSLog(@"Error");
//            feedbackNotificationError();
//            dispatch_async(dispatch_get_main_queue(), ^{
//                if (weakSelf.refreshControl.isRefreshing) {
//                    [weakSelf.refreshControl endRefreshing];
//                }
//            });
//            weakSelf.isLoaded = YES;
//            NSLog(@"error.description: %@", error.localizedDescription);
//            NSString *errorString = error.localizedDescription.length <= 0 ? error.description : error.localizedDescription;
//            [SVProgressHUD showErrorWithStatus:errorString];
//        }
//
//        [self enableDiableSelectDateButtons:YES];
//    }];
//    // Fire the request
//    [dataTask resume];
//}


#pragma mark - O365 일정 가져오기
- (void)fetchO365ScheduleOnDate:(NSString *)dateString email:(NSString *)email {
    self.isLoaded = NO;
    [self.scheduleList removeAllObjects];
    
    // MARK: - 테스트 용도
    /*
    [Navigator toMainViewControllerAfterRetryLogin];
    return;
    */
    
    __weak typeof(self) weakSelf = self;
    [self.listAdapter performUpdatesAnimated:YES completion:^(BOOL finished) {
        if (!weakSelf.refreshControl.isRefreshing) {
            weakSelf.refreshControl.hidden = NO;
            [weakSelf.refreshControl layoutIfNeeded];
            [weakSelf.refreshControl beginRefreshing];
        }
    }];
    
    weakSelf.requestIndex++;
    NSUInteger __block currentRequest = weakSelf.requestIndex;
    [QINetwork o365ScheduleListForDate:stringFromDate(self.targetDate) email:email successBlock:^(NSDictionary *responseDictionary) {
        if (![responseDictionary[@"o365ScheduleList"] isKindOfClass:[NSArray class]]) {
            feedbackNotificationError();
            dispatch_async(dispatch_get_main_queue(), ^{
                NSError *error = [QINetwork manager].generateError(nil, nil);
                [SVProgressHUD showErrorWithStatus:error.localizedDescription];
                [weakSelf.refreshControl endRefreshing];
            });
            return ;
        }

        NSMutableArray *__block schedules = [NSMutableArray array];
        [responseDictionary[@"o365ScheduleList"] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [schedules addObject:[O365Schedule o365ScheduleFromDictionary:obj]];
        }];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (weakSelf.refreshControl.isRefreshing && currentRequest >= weakSelf.requestIndex) {
                [weakSelf.scheduleList addObjectsFromArray:[schedules copy]];
                [weakSelf.listAdapter performUpdatesAnimated:YES completion:^(BOOL finished) {
                    feedbackNotificationSuccess();
                }];
                [weakSelf.refreshControl endRefreshing];
                weakSelf.isLoaded = YES;
            }
        });
    } failureBlock:^(NSError *error) {
        feedbackNotificationError();
        dispatch_async(dispatch_get_main_queue(), ^{
            if (weakSelf.refreshControl.isRefreshing) {
                [weakSelf.refreshControl endRefreshing];
            }
        });
        weakSelf.isLoaded = YES;
        NSLog(@"error.description: %@", error.localizedDescription);
        NSString *errorString = error.localizedDescription.length <= 0 ? error.description : error.localizedDescription;
        [SVProgressHUD showErrorWithStatus:errorString];
    } authFailedBlock:^(void (^completionBlock)(void)) {
        feedbackNotificationWarning();
        dispatch_async(dispatch_get_main_queue(), ^{
            if (weakSelf.refreshControl.isRefreshing) {
                [weakSelf.refreshControl endRefreshing];
            }
        });
        weakSelf.isLoaded = YES;
        //[Navigator modalPresentLoginViewControllerWithCompletionBlock:completionBlock];
        [Navigator toMainViewControllerAfterRetryLogin];
    }];
}

- (void)fetchScheduleOnDate:(NSString *)dateString {
    self.isLoaded = NO;
    [self.scheduleList removeAllObjects];
    
    __weak typeof(self) weakSelf = self;
    [self.listAdapter performUpdatesAnimated:YES completion:^(BOOL finished) {
        if (!weakSelf.refreshControl.isRefreshing) {
            weakSelf.refreshControl.hidden = NO;
            [weakSelf.refreshControl layoutIfNeeded];
            [weakSelf.refreshControl beginRefreshing];
        }
    }];
    
    weakSelf.requestIndex++;
    NSUInteger __block currentRequest = weakSelf.requestIndex;
    [QINetwork scheduleListForDate:stringFromDate(self.targetDate) successBlock:^(NSDictionary *responseDictionary) {
        if (![responseDictionary[@"scheduleList"] isKindOfClass:[NSArray class]]) {
            feedbackNotificationError();
            dispatch_async(dispatch_get_main_queue(), ^{
                NSError *error = [QINetwork manager].generateError(nil, nil);
                [SVProgressHUD showErrorWithStatus:error.localizedDescription];
                [weakSelf.refreshControl endRefreshing];
            });
            return ;
        }

        NSMutableArray *__block schedules = [NSMutableArray array];
        [responseDictionary[@"scheduleList"] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [schedules addObject:[Schedule scheduleFromDictionary:obj]];
        }];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (weakSelf.refreshControl.isRefreshing && currentRequest >= weakSelf.requestIndex) {
                [weakSelf.scheduleList addObjectsFromArray:[schedules copy]];
                [weakSelf.listAdapter performUpdatesAnimated:YES completion:^(BOOL finished) {
                    feedbackNotificationSuccess();
                }];
                [weakSelf.refreshControl endRefreshing];
                weakSelf.isLoaded = YES;
            }
        });
    } failureBlock:^(NSError *error) {
        feedbackNotificationError();
        dispatch_async(dispatch_get_main_queue(), ^{
            if (weakSelf.refreshControl.isRefreshing) {
                [weakSelf.refreshControl endRefreshing];
            }
        });
        weakSelf.isLoaded = YES;
        NSString *errorString = error.localizedDescription.length <= 0 ? error.description : error.localizedDescription;
        [SVProgressHUD showErrorWithStatus:errorString];
    } authFailedBlock:^(void (^completionBlock)(void)) {
        feedbackNotificationWarning();
        dispatch_async(dispatch_get_main_queue(), ^{
            if (weakSelf.refreshControl.isRefreshing) {
                [weakSelf.refreshControl endRefreshing];
            }
        });
        weakSelf.isLoaded = YES;
        //[Navigator modalPresentLoginViewControllerWithCompletionBlock:completionBlock];
        [Navigator toMainViewControllerAfterRetryLogin];
    }];
}

- (IBAction)scheduleAddButonDidPress:(id)sender {
    DDLogDebug(@"[ScheduleListViewController] %s", __PRETTY_FUNCTION__);
    //feedbackImpact();
    feedbackNotificationSuccess();
    if (self.isO365) {
        
        NSMutableArray *checked365Schedules = [GlobalVariables singleton].checked365Schedules;
        
        NSMutableArray<UIView *> *flashComponents = [NSMutableArray array];
        NSMutableArray<NSString *> *notiStrings = [NSMutableArray array];
        
        for (O365ScheduleViewModel *model in checked365Schedules) {
            DDLogDebug(@"id: %@, title: %@, index: %ld", model.identifier, model.title, (long)model.index);
            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:model.index]; //[NSIndexPath indexPathWithIndex:model.index];
            O365ScheduleCell *cell = (O365ScheduleCell*)[self.collectionView cellForItemAtIndexPath:indexPath];
            NSLog(@"cell.descLabel: %@", cell.descLabel.text);
            
            [notiStrings removeAllObjects];
            if ([cell.taskDropDownButton.titleLabel.text isEqualToString:@"TASK"]) {
                //[notiStrings addObject:AMLocalizedString(@"Task 를 선택하세요.")];
                [flashComponents addObject:cell.taskDropDownButton];
            }
            if ([cell.workTypeDropDownButton.titleLabel.text isEqualToString:@"업무유형"]) {
                //[notiStrings addObject:AMLocalizedString(@"업무유형을 선택하세요.")];
                [flashComponents addObject:cell.workTypeDropDownButton];
            }
        }
        
        if ([flashComponents count]) {
            NSString *notiMessage = @"Task 또는 업무유형을 선택해주세요.";
            if ([notiMessage length]) {
                [SVProgressHUD showInfoWithStatus:notiMessage];
                //return;
            }
            
            //__weak typeof(self) weakSelf = self;
            [self flashAnimationComponent:flashComponents completion:^(BOOL finished) {
                
            }];
            
            return;
            
        }

        if (checked365Schedules.count <= 0) {
            NSString *notiMessage = AMLocalizedString(@"선택한 일정이 없습니다.\n일정을 선택해주세요.");
            if ([notiMessage length]) {
                [SVProgressHUD showInfoWithStatus:notiMessage];
            }
            return;
        }
        
        NSMutableAttributedString *message = [[NSMutableAttributedString alloc] initWithString:AMLocalizedString(@"선택한 일정을 등록 하시겠습니까?")];
        [message addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:20.0] range:NSMakeRange(0, [message length])];
        
        
        __weak typeof(self) weakSelf = self;
        UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:AMLocalizedString(@"") message:message.mutableString preferredStyle:UIAlertControllerStyleActionSheet];
        
        [actionSheet setValue:message forKey:@"attributedMessage"];
        [actionSheet addAction:[UIAlertAction actionWithTitle:AMLocalizedString(@"등록") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [SVProgressHUD show];
            [QINetwork saveO365ScheduleWithDictionary:[weakSelf scheduleDictionary] successBlock:^(NSDictionary *responseDictionary) {
                [SVProgressHUD showSuccessWithStatus:AMLocalizedString(@"등록되었습니다.")];
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    self.isO365 = NO;
                    //[[NSNotificationCenter defaultCenter] postNotificationName:SCHEDULE_DID_UPDATE_ON_DATE object:stringFromDate([weakSelf.targetDate copy])];
                    [Navigator toMainViewControllerWithoutUIAnimation];
                    [Navigator toScheduleListViewController:NO date:weakSelf.targetDate];
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                });
            } failureBlock:^(NSError *error) {
                [SVProgressHUD showErrorWithStatus:error.localizedDescription];
            } authFailedBlock:^(void (^completionBlock)(void)) {
                [SVProgressHUD dismiss];
                //[Navigator modalPresentLoginViewControllerWithCompletionBlock:completionBlock];
                [Navigator toMainViewControllerAfterRetryLogin];
            }];
        }]];
        [actionSheet addAction:[UIAlertAction actionWithTitle:AMLocalizedString(@"취소") style:UIAlertActionStyleCancel handler:nil]];
        [self setModalPresentationStyle: UIModalPresentationFullScreen];
        [self presentViewController:actionSheet animated:YES completion:nil];

        
//        [Navigator toScheduleListViewController:NO completion:^{
//
//        }];
       
    } else {
        [Navigator toScheduleDetailViewControllerWithDate:_targetDate];
        /*
         NSDate *targetDate = self.targetDate;
         if ([self.scheduleList lastObject]) {
             NSArray *sortedArray = [self.scheduleList sortedArrayUsingComparator:^NSComparisonResult(O365ScheduleViewModel *a, O365ScheduleViewModel *b){
                 return [b.endTime compare:a.endTime]; //내림차순
              }];
             
             targetDate = [dateComponentFromString([[sortedArray objectAtIndex:0] endDt], [[sortedArray objectAtIndex:0] endTime]) date];
         }
         */
    }
}

- (NSDictionary *)scheduleDictionary {
    
    NSMutableArray *checked365Schedules = [GlobalVariables singleton].checked365Schedules;
    NSMutableArray *arrayParams = [[NSMutableArray alloc] init];
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    for (O365ScheduleViewModel *model in checked365Schedules) {
        NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
                              model.startDt, @"startDt",
                              model.startTime, @"startTime",
                              model.endDt, @"endDt",
                              model.endTime, @"endTime",
                              model.tastId, @"taskId",
                              model.dutyCd, @"dutyCd",
                              model.title, @"wrkDesc",
                              nil];
        [arrayParams addObject:dict];
    }
    [dictionary setValue:arrayParams forKey:@"scheduleList"];
    //NSDictionary *dictionary = [[NSDictionary alloc] init];
    //dictionary = [checked365Schedules mutableCopy]
    //[dictionary setValue:arrayParams forKey:@"scheduleList"];
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:nil];
    NSString* jsonDataStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    

    NSLog(@"jsonDataStr: %@", jsonDataStr);
    // convert your object to JSOn String
    //NSError *error = nil;
    //NSString *createJSON = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:&error] encoding:NSUTF8StringEncoding];
    //NSLog(@"jsonDataStr: %@", jsonDataStr);
//    NSDictionary *pardsams = @{@"scheduleList": createJSON};
//
//    NSLog(@"pardsams: %@", pardsams);
    
    return dictionary;
    
//    return @{
//        @"startDt" : stringFromDate(self.startDate) ?: [NSNull null],
//        @"startTime" : stringTimeFromDate(self.startDate) ?: [NSNull null],
//        @"endDt" : stringFromDate(self.endDate) ?: [NSNull null],
//        @"endTime" : stringTimeFromDate(self.endDate) ?: [NSNull null],
//        @"taskId" : [self.taskButton titleForState:UIControlStateApplication] ?: [NSNull null],
//        @"dutyCd" : [self.dutyButton titleForState:UIControlStateApplication] ?: [NSNull null],
//        @"wrkDesc" : [self.descTextView text] ?: [NSNull null]
//    };
}

- (void)dateLabelDidTap:(UITapGestureRecognizer *)gesture {
    if (gesture.state == UIGestureRecognizerStateRecognized) {
        feedbackImpact();
        __weak typeof(self) weakSelf = self;
        [UIView animateWithDuration:0.2f animations:^{
            if (weakSelf.calendar.alpha == 0) {
                weakSelf.calendar.alpha = 1.0f;
                weakSelf.calendar.scope = FSCalendarScopeWeek;
                weakSelf.headerVerticalSpacingConstraint.constant = weakSelf.calendarHeightConstraint.constant;
            }
            else if (weakSelf.calendar.scope == FSCalendarScopeWeek) {
                weakSelf.calendar.alpha = 0;
                weakSelf.headerVerticalSpacingConstraint.constant = 0;
            } else {
                [weakSelf.calendar setScope:FSCalendarScopeWeek animated:YES];
            }
            [weakSelf.view layoutIfNeeded];
        }];
    }
}

- (void)dateLabelDidLongPress:(UILongPressGestureRecognizer *)gesture {
    if (gesture.state == UIGestureRecognizerStateBegan) {
        feedbackImpact();
        [self setTargetDate:[NSDate date]];
    }
}

- (IBAction)nextButtonDidPress:(id)sender {
    [self calcDateByAddingDay:(+1)];
    feedbackImpact();
}

- (IBAction)prevButtonDidPress:(id)sender {
    [self calcDateByAddingDay:(-1)];
    feedbackImpact();
}

- (void)calcDateByAddingDay:(NSInteger)day {
    NSDateComponents *dateComponenet = [[NSCalendar autoupdatingCurrentCalendar] components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear|NSCalendarUnitWeekday fromDate:self.targetDate];
    [dateComponenet setCalendar:[NSCalendar autoupdatingCurrentCalendar]];
    [dateComponenet setDay:dateComponenet.day + day];
    
    NSDate *outputDate = [self mergeSelectedDateCurrentTime:[dateComponenet date]];
    [self setTargetDate:outputDate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)removeSchedule:(NSNotification *)noti {
    feedbackNotificationWarning();
    
    NSDictionary *notiDic = [noti userInfo];
    ScheduleViewModel *viewModel = [notiDic objectForKey:SCHEDULE_DID_REMOVE];

    NSLog(@"viewModel=%@",viewModel);
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:AMLocalizedString(@"삭제") message:AMLocalizedString(@"정말 삭제 하시겠습니까?") preferredStyle:UIAlertControllerStyleActionSheet];
    __weak typeof(self) weakSelf = self;
    [actionSheet addAction:[UIAlertAction actionWithTitle:AMLocalizedString(@"삭제") style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [SVProgressHUD show];
        [QINetwork removeScheduleByID:viewModel.identifier successBlock:^(NSDictionary *responseDictionary) {
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:SCHEDULE_DID_UPDATE_ON_DATE object:stringFromDate(weakSelf.targetDate)];
                //[weakSelf.navigationController popViewControllerAnimated:YES];
            });
        } failureBlock:^(NSError *error) {
            [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        } authFailedBlock:^(void (^completionBlock)(void)) {
            [SVProgressHUD dismiss];
            //[Navigator modalPresentLoginViewControllerWithCompletionBlock:completionBlock];
            [Navigator toMainViewControllerAfterRetryLogin];
        }];
    }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:AMLocalizedString(@"취소") style:UIAlertActionStyleCancel handler:nil]];
    [self setModalPresentationStyle: UIModalPresentationFullScreen];
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

#pragma mark - <UIGestureRecognizerDelegate>

// Whether scope gesture should begin
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    BOOL shouldBegin = self.collectionView.contentOffset.y <= -self.collectionView.contentInset.top;
    if (shouldBegin) {
        CGPoint velocity = [self.scopeGesture velocityInView:self.view];
        switch (self.calendar.scope) {
            case FSCalendarScopeMonth:
                return velocity.y < 0;
            case FSCalendarScopeWeek:
                return velocity.y > 0;
        }
    }
    return shouldBegin;
}

#pragma mark - 날짜를 선택할 경우 선택한 날짜 + 현재시간 조합으로 변환

/*
wooya510@20200623:ADD
다음과 같이 수정한 이유는 날짜가 과거 또는 미래이던 간에 무조건 시간이 00:00:00 이기 때문에 일정 등록을 할 때 무조건 시작시간이 00:00으로 된다.
그래서 날짜는 선택한 날짜이면서 시간은 현재 시간이 설정되도록 변경 (안드로이드와 동일한 정책으로 변경함)
*/
- (NSDate *)mergeSelectedDateCurrentTime:(NSDate *)date {
    NSLog(@"did select date: %@", date);
    //현재 시간을 가져온다.
    NSDate *currentDateAndTime = [NSDate date];

    NSDateFormatter* currentDateFormatter = [[NSDateFormatter alloc]init];
    [currentDateFormatter setDateFormat:@"hh:mm:ss"];
    NSString *currentTime = [currentDateFormatter stringFromDate:currentDateAndTime];
    NSLog(@"currentTime  %@ ", currentTime);

    //선택한 년도를 가지고 온다.
    NSDateFormatter* selectedYearFormatter = [[NSDateFormatter alloc]init];
    [selectedYearFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *selectedDate = [selectedYearFormatter stringFromDate:date];
    NSLog(@"selectedDate  %@ ", selectedDate);
        
    NSDateFormatter* dateformate = [[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd HH:mm:ss"]; //NSDate 기본 포맷

    //선택한 년도와 현재시간을 조합한다. 기본 NSDate 포맷으로
    NSDate *outputDate = [dateformate dateFromString:[NSString stringWithFormat:@"%@ %@", selectedDate, currentTime]];
    NSLog(@"outputDate  %@ ", outputDate);
    
    return outputDate;
}

#pragma mark - <FSCalendarDelegate>

- (void)calendar:(FSCalendar *)calendar boundingRectWillChange:(CGRect)bounds animated:(BOOL)animated
{
    self.calendarHeightConstraint.constant = CGRectGetHeight(bounds);
    self.headerVerticalSpacingConstraint.constant = calendar.alpha ? self.calendarHeightConstraint.constant : 0;
    [self.view layoutIfNeeded];
}

- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition
{
    NSDate *outputDate = [self mergeSelectedDateCurrentTime:date];
    
    feedbackSelection();
    [self setTargetDate:outputDate];
}

- (void)calendarCurrentPageDidChange:(FSCalendar *)calendar
{
    NSLog(@"%s %@", __FUNCTION__, calendar.currentPage);
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (NSMutableArray<NSObject <IGListDiffable>*> *)scheduleList {
    if (!_scheduleList) {
        _scheduleList = [NSMutableArray array];
    }
    NSLog(@"_scheduleList.firstObject: %@", _scheduleList.firstObject);
    return _scheduleList;
}

/**
 Asks the data source for the objects to display in the list.
 
 @param listAdapter The list adapter requesting this information.
 
 @return An array of objects for the list.
 */
- (NSArray<id <IGListDiffable>> *)objectsForListAdapter:(IGListAdapter *)listAdapter {
    NSLog(@"[self scheduleList]: %@", [self scheduleList]);
    return [self scheduleList];
}

/**
 Asks the data source for a section controller for the specified object in the list.
 
 @param listAdapter The list adapter requesting this information.
 @param object An object in the list.
 
 @return A new section controller instance that can be displayed in the list.
 
 @note New section controllers should be initialized here for objects when asked. You may pass any other data to
 the section controller at this time.
 
 Section controllers are initialized for all objects whenever the `IGListAdapter` is created, updated, or reloaded.
 Section controllers are reused when objects are moved or updated. Maintaining the `-[IGListDiffable diffIdentifier]`
 guarantees this.
 */
- (IGListSectionController *)listAdapter:(IGListAdapter *)listAdapter sectionControllerForObject:(id)object {
    return [ScheduleSectionController new];
}

/**
 Asks the data source for a view to use as the collection view background when the list is empty.
 
 @param listAdapter The list adapter requesting this information.
 
 @return A view to use as the collection view background, or `nil` if you don't want a background view.
 
 @note This method is called every time the list adapter is updated. You are free to return new views every time,
 but for performance reasons you may want to retain the view and return it here. The infra is only responsible for
 adding the background view and maintaining its visibility.
 */
- (nullable UIView *)emptyViewForListAdapter:(IGListAdapter *)listAdapter {
    if (self.isLoaded) {
        return self.emptyView;
    }
    return nil;
}

@end
