//
//  O365ScheduleCell.h
//  management
//
//  Created by 이주한 on 2020/05/08.
//  Copyright © 2020 Pulmuone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <IGListKit/IGListKit.h>
#import <BEMCheckBox.h>
#import "ScheduleViewModel.h"
#import "O365ScheduleViewModel.h"
#import "ComponentStyler.h"
#import "BEMCheckBox.h"
#import "GlobalVariables.h"
#import "QINetwork+Pulmuone.h"
#import "Navigator.h"
#import "SVProgressHUD.h"
#import "TextFieldLikeButton.h"

NS_ASSUME_NONNULL_BEGIN

@interface O365ScheduleCell : UICollectionViewCell <IGListBindable, BEMCheckBoxDelegate>
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet BEMCheckBox *checkBox;
@property (weak, nonatomic) IBOutlet TextFieldLikeButton *taskDropDownButton;
@property (weak, nonatomic) IBOutlet TextFieldLikeButton *workTypeDropDownButton;
@property (nonatomic, assign) O365ScheduleViewModel *o365ScheduleViewModel;
@end

NS_ASSUME_NONNULL_END
