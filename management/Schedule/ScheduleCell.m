//
//  ScheduleCell.m
//  management
//
//  Created by Quintet on 27/06/2018.
//  Copyright © 2018 Pulmuone. All rights reserved.
//

#import "ScheduleCell.h"
#import "ScheduleViewModel.h"
#import "ComponentStyler.h"

@interface ScheduleCell ()
@property (nonatomic, strong) IBOutlet UILabel *timeLabel;
@property (nonatomic, strong) IBOutlet UILabel *taskLabel;
@property (nonatomic, strong) IBOutlet UILabel *dutyLabel;
@end

@implementation ScheduleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self initializeSubviews];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self initializeSubviews];
    }
    return self;
}

- (void)initializeSubviews {
    [ComponentStyler scheduleCellOrderLabel:self.orderLabel];
    [ComponentStyler scheduleCellLabel:self.timeLabel];
    [ComponentStyler scheduleCellLabel:self.taskLabel];
    [ComponentStyler scheduleCellLabel:self.dutyLabel];
}

- (void)prepareForReuse {
    [super prepareForReuse];
    
    self.orderLabel.text = @"";
    self.timeLabel.text = @"";
    self.taskLabel.text = @"";
    self.dutyLabel.text = @"";
}

- (void)bindViewModel:(nonnull id)viewModel {
    if ([viewModel isKindOfClass:[ScheduleViewModel class]]) {
        ScheduleViewModel *scheduleViewModel = (ScheduleViewModel *)viewModel;
        self.timeLabel.text = [NSString stringWithFormat:@"%@ ~ %@", stringFromTimeNumber(scheduleViewModel.startTime), stringFromTimeNumber(scheduleViewModel.endTime)];
        self.taskLabel.text = scheduleViewModel.taskNm;
        self.dutyLabel.text = scheduleViewModel.dutyCdNm;
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    
    self.backgroundColor = ColorRGBA(242, 242, 242, 1);
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];
    
    self.backgroundColor = ColorRGBA(255, 255, 255, 1);
}

- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesCancelled:touches withEvent:event];
    
    self.backgroundColor = ColorRGBA(255, 255, 255, 1);
}

@end
