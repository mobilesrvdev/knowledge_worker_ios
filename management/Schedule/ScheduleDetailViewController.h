//
//  ScheduleDetailViewController.h
//  management
//
//  Created by Quintet on 25/06/2018.
//  Copyright © 2018 Pulmuone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonViewController.h"

@interface ScheduleDetailViewController : CommonViewController
@property (nonatomic, copy) NSDate *initialDate;
@property (nonatomic, copy) NSString *scheduleID;
@end
