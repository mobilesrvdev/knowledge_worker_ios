//
//  ScheduleListViewController.h
//  management
//
//  Created by Quintet on 27/06/2018.
//  Copyright © 2018 Pulmuone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonViewController.h"

@interface ScheduleListViewController : CommonViewController
@property (nonatomic, copy, setter=setTargetDate:) NSDate *targetDate;
@property (nonatomic, assign) Boolean isO365;
@end
