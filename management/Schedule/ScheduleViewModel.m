//
//  ScheduleViewModel.m
//  management
//
//  Created by Quintet on 27/06/2018.
//  Copyright © 2018 Pulmuone. All rights reserved.
//

#import "ScheduleViewModel.h"

@interface ScheduleViewModel ()

@end

@implementation ScheduleViewModel

+ (instancetype)scheduleViewModelFromSchedule:(NSDictionary *)schedule {
    return [[[self class] alloc] initWithSchedule:schedule];
}

- (instancetype)init {
    if (self = [self initWithSchedule:nil]) {
        
    }
    return self;
}

- (instancetype)initWithSchedule:(NSDictionary *)schedule {
    if (self = [super init]) {
        _identifier = schedule[@"id"];
        _startDt = schedule[@"startDt"];
        _startTime = schedule[@"startTime"];
        _endDt = schedule[@"endDt"];
        _endTime = schedule[@"endTime"];
        _taskNm = schedule[@"taskNm"];
        _dutyCdNm = schedule[@"dutyCdNm"];
        _wrkComnt = schedule[@"wrkComnt"];
        _wrkDesc = schedule[@"wrkDesc"];
    }
    return self;
}

- (nonnull id<NSObject>)diffIdentifier {
    return self.identifier;
}

- (BOOL)isEqualToDiffableObject:(nullable id<IGListDiffable>)object {
    return [[(NSObject *)object class] isKindOfClass:[self class]]
        && [self.identifier isEqualToString:[(ScheduleViewModel *)object identifier]]
        && [self.startDt isEqualToString:[(ScheduleViewModel *)object startDt]]
        && [self.startTime isEqualToString:[(ScheduleViewModel *)object startTime]]
        && [self.endDt isEqualToString:[(ScheduleViewModel *)object endDt]]
        && [self.endTime isEqualToString:[(ScheduleViewModel *)object endTime]]
        && [self.taskNm isEqualToString:[(ScheduleViewModel *)object taskNm]]
        && [self.dutyCdNm isEqualToString:[(ScheduleViewModel *)object dutyCdNm]]
        && [self.wrkComnt isEqualToString:[(ScheduleViewModel *)object wrkComnt]]
        && [self.wrkDesc isEqualToString:[(ScheduleViewModel *)object wrkDesc]];
}

@end
