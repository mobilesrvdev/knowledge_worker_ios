//
//  Schedule.m
//  management
//
//  Created by Quintet on 27/06/2018.
//  Copyright © 2018 Pulmuone. All rights reserved.
//

#import "Schedule.h"

@interface Schedule ()

@end

@implementation Schedule

+ (instancetype)scheduleFromDictionary:(NSDictionary *)dictionary {
    return [[[self class] alloc] initWithDictionary:dictionary];
}

- (instancetype)init {
    if (self = [self initWithDictionary:nil]) {
        
    }
    return self;
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (self = [super init]) {
        _identifier = dictionary[@"id"];
        _startDt = dictionary[@"startDt"];
        _startTime = dictionary[@"startTime"];
        _endDt = dictionary[@"endDt"];
        _endTime = dictionary[@"endTime"];
        _taskId = dictionary[@"taskId"];
        _taskNm = dictionary[@"taskNm"];
        _dutyCd = dictionary[@"dutyCd"];
        _dutyCdNm = dictionary[@"dutyCdNm"];
        _wrkDesc = dictionary[@"wrkDesc"];
        _wrkComnt = dictionary[@"wrkComnt"];
    }
    return self;
}

- (NSDictionary *)dictionary {
    return @{
             @"id" : self.identifier ?: @"",
             @"startDt" : self.startDt ?: @"",
             @"startTime" : self.startTime ?: @"",
             @"endDt" : self.endDt ?: @"",
             @"endTime" : self.endTime ?: @"",
             @"taskId" : self.taskId ?: @"",
             @"taskNm" : self.taskNm ?: @"",
             @"dutyCd" : self.dutyCd ?: @"",
             @"dutyCdNm" : self.dutyCdNm ?: @"",
             @"wrkDesc" : self.wrkDesc ?: @"",
             @"wrkComnt" : self.wrkComnt ?: @"",
             };
}

- (nonnull id<NSObject>)diffIdentifier {
    return self.identifier;
}

- (BOOL)isEqualToDiffableObject:(nullable id<IGListDiffable>)object {
    return [[(NSObject *)object class] isKindOfClass:[self class]]
        && [self.identifier isEqualToString:[(Schedule *)object identifier]]
        && [self.startDt isEqualToString:[(Schedule *)object startDt]]
        && [self.startTime isEqualToString:[(Schedule *)object startTime]]
        && [self.endDt isEqualToString:[(Schedule *)object endDt]]
        && [self.endTime isEqualToString:[(Schedule *)object endTime]]
        && [self.taskId isEqualToString:[(Schedule *)object taskId]]
        && [self.taskNm isEqualToString:[(Schedule *)object taskNm]]
        && [self.dutyCd isEqualToString:[(Schedule *)object dutyCd]]
        && [self.dutyCdNm isEqualToString:[(Schedule *)object dutyCdNm]]
    ;
}

@end
