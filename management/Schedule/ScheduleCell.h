//
//  ScheduleCell.h
//  management
//
//  Created by Quintet on 27/06/2018.
//  Copyright © 2018 Pulmuone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <IGListKit/IGListKit.h>

@interface ScheduleCell : UICollectionViewCell <IGListBindable>
@property (nonatomic, strong) IBOutlet UILabel *orderLabel;
@end
