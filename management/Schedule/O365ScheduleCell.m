//
//  O365ScheduleCell.m
//  management
//
//  Created by 이주한 on 2020/05/08.
//  Copyright © 2020 Pulmuone. All rights reserved.
//

#import "O365ScheduleCell.h"

@interface O365ScheduleCell ()
//@property (nonatomic, copy) NSDate *startDate;
//@property (nonatomic, copy) NSDate *endDate;
//@property (nonatomic) BOOL isValidTask;
//@property (nonatomic, strong) NSArray <NSDictionary *> *taskList;
//@property (nonatomic, strong) NSArray <NSDictionary *> *dutyList;
//@property (nonatomic) NSInteger __block networkActivityCount;
//@property (nonatomic) NSInteger __block networkActivityCountMax;
//
//@property (nonatomic, strong) NSMutableArray <NSError *> __block *failedNetworkMessages;
//@property (nonatomic, strong) NSMutableArray <NSError *> __block *warningNetworkMessages;

@end

@implementation O365ScheduleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self initializeSubviews];
//    self.startDate = [NSDate date];
//    self.endDate = [self.startDate dateByAddingTimeInterval:600];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self initializeSubviews];
    }
    return self;
}

- (void)initializeSubviews {
    [ComponentStyler defaultButtonLikeDropDown:self.taskDropDownButton];
    [ComponentStyler defaultButtonLikeDropDown:self.workTypeDropDownButton];
    
    [ComponentStyler scheduleCellLabel:self.timeLabel];
    
    self.checkBox.onTintColor = ColorRGBA(141, 198, 63, 1);
    self.checkBox.onCheckColor = [UIColor whiteColor];// ColorRGBA(141, 198, 63, 1);
    self.checkBox.onFillColor = ColorRGBA(141, 198, 63, 1);
    self.checkBox.animationDuration = 0.2f;
    self.checkBox.offAnimationType = BEMAnimationTypeFill;
    self.checkBox.delegate = self;
}

- (void)prepareForReuse {
    [super prepareForReuse];
        
    self.timeLabel.text = @"";
}

- (void)bindViewModel:(nonnull id)viewModel {
    if ([viewModel isKindOfClass:[O365ScheduleViewModel class]]) {
        self.o365ScheduleViewModel = (O365ScheduleViewModel *)viewModel;
        self.timeLabel.text = [NSString stringWithFormat:@"%@ ~ %@", stringFromTimeNumber(self.o365ScheduleViewModel.startTime), stringFromTimeNumber(self.o365ScheduleViewModel.endTime)];
        [self.descLabel setAdjustsFontSizeToFitWidth:NO]; //폰트 크기 고정
        self.descLabel.text = self.o365ScheduleViewModel.title;
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    
    self.backgroundColor = ColorRGBA(242, 242, 242, 1);
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];
    
    self.backgroundColor = ColorRGBA(255, 255, 255, 1);
}

- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesCancelled:touches withEvent:event];
    
    self.backgroundColor = ColorRGBA(255, 255, 255, 1);
}

- (void)didTapCheckBox:(BEMCheckBox *)checkBox {
    feedbackNotificationSuccess();
    //checkBox.on
    if (self.o365ScheduleViewModel != nil) {
//        NSString *taskDropDownButtonTitleLabel = [[self.taskDropDownButton titleLabel] text];
        //NSLog(@"taskDropDownButtonTitleLabel: %@", taskDropDownButtonTitleLabel);
        
//        NSString *workTypeDropDownButtonTitleLabel = [[self.workTypeDropDownButton titleLabel] text];
        //NSLog(@"workTypeDropDownButtonTitleLabel: %@", workTypeDropDownButtonTitleLabel);
        
//        NSMutableArray<NSString *> *notiStrings = [NSMutableArray array];
//        if ([taskDropDownButtonTitleLabel isEqualToString:@"TASK"]) {
//            [notiStrings addObject:AMLocalizedString(@"Task 를 선택하세요.")];
//        }
//
//        if ([workTypeDropDownButtonTitleLabel isEqualToString:@"업무유형"]) {
//            [notiStrings addObject:AMLocalizedString(@"업무유형을 선택하세요.")];
//        }
//
//        NSString *notiMessage = [notiStrings componentsJoinedByString:@"\n"];
//        if ([notiMessage length]) {
//            [SVProgressHUD showInfoWithStatus:notiMessage];
//            checkBox.on = NO;
//            self.o365ScheduleViewModel.isChecked = NO;
//            return;
//        }
        
        self.o365ScheduleViewModel.isChecked = checkBox.on;
        //Office365일정 목록에서 체크된 것만 저장
        //NSMutableArray * checked365Schedules = [GlobalVariables singleton].checked365Schedules;
        
        NSString *taskId = [self.taskDropDownButton titleForState:UIControlStateApplication];
        NSString *dutyCd = [self.workTypeDropDownButton titleForState:UIControlStateApplication];
        
        self.o365ScheduleViewModel.tastId = taskId;
        self.o365ScheduleViewModel.dutyCd = dutyCd;
        
        NSLog(@"taskId: %@, dutyCd: %@", taskId, dutyCd);
        
        if (checkBox.on) {
            DDLogDebug(@"id>>>> %@", self.o365ScheduleViewModel.identifier);
            [[GlobalVariables singleton].checked365Schedules addObject:self.o365ScheduleViewModel];
        } else {
            [[GlobalVariables singleton].checked365Schedules removeObject:self.o365ScheduleViewModel];
        }
    }
}

- (IBAction)touchTaskDropDown:(id)sender {
    feedbackSelection();
    //[self fetchTaskFromDate:self.startDate toDate:self.endDate show:YES];
    [self showTaskActionSheet];
}

- (IBAction)touchWorkTypeDropDown:(id)sender {
    feedbackSelection();
   //[self fetchCommonCodeAndShow:YES];
    [self showDutyActionSheet];
}

//- (void)fetchTaskFromDate:(NSDate *)startDate toDate:(NSDate *)endDate show:(BOOL)show {
//    if (!self.isValidTask) {
//        [self beginNetworking];
//        __weak typeof(self) weakSelf = self;
//        [QINetwork taskListBetweenStartDate:stringFromDate(startDate) endDate:stringFromDate(endDate) successBlock:^(NSDictionary *responseDictionary) {
//            weakSelf.taskList = responseDictionary[@"taskList"];
//            if (show) {
//                [weakSelf showTaskActionSheet];
//            }
//            [weakSelf endNetworking];
//            weakSelf.isValidTask = YES;
//        } failureBlock:^(NSError *error) {
//            [weakSelf warnNetworkingWithError:error];
//        } authFailedBlock:^(void (^completionBlock)(void)) {
//            [weakSelf endNetworking];
//            [Navigator modalPresentLoginViewControllerWithCompletionBlock:completionBlock];
//        }];
//    } else if (show) {
//        [self showTaskActionSheet];
//    }
//}
//
//- (void)fetchCommonCodeAndShow:(BOOL)show {
//    if (!self.dutyList) {
//        self.dutyList = @[];
//        [self beginNetworking];
//        __weak typeof(self) weakSelf = self;
//        [QINetwork commonCodeWithGroupCode:@"WORK_TYPE_CD" successBlock:^(NSDictionary *responseDictionary) {
//            weakSelf.dutyList = responseDictionary[@"commCodeList"];
//            if (show) {
//                [weakSelf showDutyActionSheet];
//            }
//            [weakSelf endNetworking];
//        } failureBlock:^(NSError *error) {
//            weakSelf.dutyList = nil;
//            [weakSelf warnNetworkingWithError:error];
//        }];
//    } else if (show) {
//        [self showDutyActionSheet];
//    }
//}

- (void)showTaskActionSheet {
    NSArray <NSDictionary *> *taskList = [GlobalVariables singleton].taskList;
    [self showActionSheetForButton:(UIButton *)self.taskDropDownButton withTitle:@"TASK" from:taskList titleKey:@"taskNm" codeKey:@"id"];
}

- (void)showDutyActionSheet {
    NSArray <NSDictionary *> *dutyList = [GlobalVariables singleton].dutyList;
    [self showActionSheetForButton:(UIButton *)self.workTypeDropDownButton withTitle:@"업무유형" from:dutyList titleKey:@"markName" codeKey:@"codeName"];
}

- (void)showActionSheetForButton:(UIButton *)button withTitle:(NSString *)title from:(NSArray *)array titleKey:(NSString *)titleKey codeKey:(NSString *)codeKey {
    //[SVProgressHUD dismiss];
    NSMutableAttributedString *_title = [[NSMutableAttributedString alloc] initWithString:AMLocalizedString(title)];
    [_title addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:20.0] range:NSMakeRange(0, [_title length])];
    
    UIAlertController *__block actionSheet = [UIAlertController alertControllerWithTitle:_title.mutableString message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [actionSheet setValue:_title forKey:@"attributedTitle"];
    
    [array enumerateObjectsUsingBlock:^(NSDictionary * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *titleLabel = [NSString stringWithFormat:@"%@%@", [obj[codeKey] isEqualToString:[button titleForState:UIControlStateApplication]] && !canSetChecked() ? @"✓ " : @"", obj[titleKey]];
        UIAlertAction *action = [UIAlertAction actionWithTitle:titleLabel style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [button setTitle:obj[titleKey] forState:UIControlStateNormal];
            [button setTitle:obj[codeKey] forState:UIControlStateApplication];
            /*
             20200609:ADD
             체크박스 선택을 먼저하고 TASK, 업무유형을 선택하면 해당 값이 적용이 안된다.
             그래서 TASK, 업무유형 선택할 때마다 값이 적용되도록 수정.
             */
            if ([codeKey isEqualToString:@"id"]) {
                NSString *taskId = [button titleForState:UIControlStateApplication];
                self.o365ScheduleViewModel.tastId = taskId;
            } else {
                NSString *dutyCd = [button titleForState:UIControlStateApplication];
                self.o365ScheduleViewModel.dutyCd = dutyCd;
            }
        }];
        setCheckedOnAlertAction(action, [obj[codeKey] isEqualToString:[button titleForState:UIControlStateApplication]]);
        [actionSheet addAction:action];
    }];
    [actionSheet addAction:[UIAlertAction actionWithTitle:AMLocalizedString(@"취소") style:UIAlertActionStyleCancel handler:nil]];
    
    UIViewController *viewController = topMostController();
    
    [viewController setModalPresentationStyle: UIModalPresentationFullScreen];
    [viewController presentViewController:actionSheet animated:YES completion:nil];
}

//- (void)beginNetworking {
//    self.networkActivityCount++;
//    self.networkActivityCountMax = MAX(self.networkActivityCountMax, self.networkActivityCount);
//    if (self.networkActivityCount > 0) {
//        if (self.networkActivityCountMax == self.networkActivityCount) {
//            [SVProgressHUD show];
//        } else {
//            [SVProgressHUD showProgress:(self.networkActivityCountMax - self.networkActivityCount)/(float)self.networkActivityCountMax];
//        }
//    }
//}
//
//- (void)endNetworking {
//    self.networkActivityCount--;
//    if ([self.failedNetworkMessages count]) {
//        [[QINetwork manager] cancel];
//        NSString *__block failedMessage = @"";
//        NSUInteger lastIndex = [self.failedNetworkMessages count] - 1;
//        [self.failedNetworkMessages enumerateObjectsUsingBlock:^(NSError * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//            failedMessage = [failedMessage stringByAppendingString:obj.localizedDescription];
//            if (idx != lastIndex) {
//                failedMessage = [failedMessage stringByAppendingString:@"\n"];
//            }
//        }];
//        [SVProgressHUD showErrorWithStatus:failedMessage];
//        UIViewController *viewController = topMostController();
//        [viewController.navigationController popViewControllerAnimated:YES];
//        self.networkActivityCountMax = 0;
//        self.failedNetworkMessages = nil;
//    } else if ([self.warningNetworkMessages count]) {
//        NSString *__block failedMessage = @"";
//        NSUInteger lastIndex = [self.warningNetworkMessages count] - 1;
//        [self.warningNetworkMessages enumerateObjectsUsingBlock:^(NSError * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//            failedMessage = [failedMessage stringByAppendingString:obj.localizedDescription];
//            if (idx != lastIndex) {
//                failedMessage = [failedMessage stringByAppendingString:@"\n"];
//            }
//        }];
//        [SVProgressHUD showErrorWithStatus:failedMessage];
//        self.networkActivityCountMax = 0;
//        self.warningNetworkMessages = nil;
//    } else if (self.networkActivityCount > 0) {
//        [SVProgressHUD showProgress:(self.networkActivityCountMax - self.networkActivityCount)/(float)self.networkActivityCountMax];
//    } else {
//        [SVProgressHUD dismiss];
//        self.networkActivityCountMax = 0;
//    }
//}
//
//- (void)failedNetworkingWithError:(NSError *)error {
//    self.failedNetworkMessages = self.failedNetworkMessages ?: [NSMutableArray array];
//    [self.failedNetworkMessages addObject:error];
//    [self endNetworking];
//}
//
//- (void)warnNetworkingWithError:(NSError *)error {
//    self.warningNetworkMessages = self.warningNetworkMessages ?: [NSMutableArray array];
//    [self.warningNetworkMessages addObject:error];
//    [self endNetworking];
//}

@end
