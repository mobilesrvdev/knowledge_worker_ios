//
//  LoginViewController.m
//  management
//
//  Created by Quintet on 25/06/2018.
//  Copyright © 2018 Pulmuone. All rights reserved.
//

#import "LoginViewController.h"
#import "management-Swift.h"
#import "ComponentStyler.h"
#import "SVProgressHUD.h"
#import "QINetwork+Pulmuone.h"
#import "GlobalVariables.h"
/**
 
 Login
 Validation
 Save login info into the keychain
 Save present info into the object
 Transition to Main
 
 **/

@interface LoginViewController () <UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet SkyFloatingLabelTextField *emailTextField;
@property (strong, nonatomic) IBOutlet SkyFloatingLabelTextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UIButton *loginButton;
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initializeSubviews];
}

- (void)initializeSubviews {
    [ComponentStyler defaultTextField:self.emailTextField];
    self.emailTextField.title = AMLocalizedString(@"이메일 아이디");
    self.emailTextField.placeholder = AMLocalizedString(@"이메일 아이디");
    self.emailTextField.keyboardType = UIKeyboardTypeEmailAddress;
    self.emailTextField.delegate = self;
    [self.emailTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    [ComponentStyler defaultTextField:self.passwordTextField];
    self.passwordTextField.title = AMLocalizedString(@"비밀번호");
    self.passwordTextField.placeholder = AMLocalizedString(@"비밀번호");
    self.passwordTextField.secureTextEntry = YES;
    self.passwordTextField.keyboardAppearance = UIKeyboardAppearanceDark;
    self.passwordTextField.delegate = self;
    [self.passwordTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    [ComponentStyler defaultButtonStylePositiveBig:self.loginButton];
    [self.loginButton setTitle:AMLocalizedString(@"로그인") forState:UIControlStateNormal];
    self.loginButton.enabled = NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    feedbackSelection();
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    BOOL shouldChange = YES;
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    
    NSUInteger maxLength = (textField == self.emailTextField ? MAX_EMAIL_LENGTH : MAX_PASSWORD_LENGTH);
    
    if([string length] && newLength > maxLength)
    {
        shouldChange = NO;
    } else {
        [self textFieldDidChange:textField];
    }
    
    return shouldChange;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.emailTextField) {
        return [self.passwordTextField becomeFirstResponder];
    } else if (textField == self.passwordTextField && [self.emailTextField.text length] > 0) {
        [self loginButtonDidPress:self.loginButton];
        return YES;
    }
    return [self.emailTextField becomeFirstResponder];
}

- (void)textFieldDidChange:(UITextField *)textField {
    self.loginButton.enabled = [self.emailTextField.text length] && [self.passwordTextField.text length];
}

- (IBAction)loginButtonDidPress:(id)sender {
    feedbackImpact();
    [SVProgressHUD show];
    [self.view endEditing:YES];
    NSString *userId = self.emailTextField.text;
    userId = [userId stringByAppendingString:EMAIL_POSTFIX];
    NSLog(@"[loginButtonDidPress] userId: %@", userId);
    
    //O365 일정 조회를 위해 필요
    [GlobalVariables singleton].email = userId;
    
    NSString *userPW = sha256HashString(self.passwordTextField.text);
    __weak typeof(self) weakSelf = self;
    [QINetwork loginWithUserID:userId password:userPW successBlock:^(NSDictionary *responseDictionary) {
        feedbackNotificationSuccess();
        [[QIContainer sharedContainer] setUserInfo:@{@"userInfo":responseDictionary[@"userInfo"], @"workInOutList":responseDictionary[@"workInOutList"]}];
        saveUserID(userId);
        saveUserPassword(userPW);
        
        [weakSelf dismissViewControllerAnimated:YES completion:^{
            __strong typeof(weakSelf) strongSelf = weakSelf;
            if (strongSelf.completionBlock) {
                strongSelf.completionBlock();
            }
        }];
        
        [SVProgressHUD dismiss];
    } failureBlock:^(NSError *error) {
        feedbackNotificationError();
        saveUserID(nil);
        saveUserPassword(nil);
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
