//
//  Navigator.h
//  management
//
//  Created by Quintet on 27/06/2018.
//  Copyright © 2018 Pulmuone. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Navigator : NSObject

+ (void)toMainViewController;
+ (void)toMainViewControllerWithoutUIAnimation;
+ (void)toScheduleListViewController:(Boolean)isO365 completion:(void (^)(void))completion;
+ (void)toScheduleListViewController:(Boolean)isO365 date:(NSDate *)date;
+ (void)toScheduleDetailViewControllerWithDate:(NSDate *)date;
+ (void)toScheduleDetailViewControllerWithID:(NSString *)scheduleID;
+ (void)modalPresentLoginViewControllerWithCompletionBlock:(void(^)(void))completionBlock;
+ (void)toMainViewControllerAfterRetryLogin;
@end
