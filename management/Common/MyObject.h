//
//  MyObject.h
//  management
//
//  Created by 이주한 on 2020/05/18.
//  Copyright © 2020 Pulmuone. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GlobalVars : NSObject
extern NSArray <NSDictionary *> *taskList;
extern NSArray <NSDictionary *> *dutyList;
@end

NS_ASSUME_NONNULL_END
