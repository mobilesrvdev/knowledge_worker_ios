//
//  GlobalVariables.m
//  management
//
//  Created by 이주한 on 2020/05/08.
//  Copyright © 2020 Pulmuone. All rights reserved.
//

#import "GlobalVariables.h"

@implementation GlobalVariables
+(GlobalVariables *)singleton {
    static dispatch_once_t pred;
    static GlobalVariables *shared = nil;
    dispatch_once(&pred, ^{
        shared = [[GlobalVariables alloc] init];
        shared.checked365Schedules = [[NSMutableArray alloc] init];
    });
    return shared;
}
@end
