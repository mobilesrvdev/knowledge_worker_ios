//
//  ButtonLikeTextField.m
//  management
//
//  Created by Quintet on 28/06/2018.
//  Copyright © 2018 Pulmuone. All rights reserved.
//

#import "TextFieldLikeButton.h"

@interface TextFieldLikeButton ()
@property (nonatomic, strong) UIView *lineView;
@end

@implementation TextFieldLikeButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder])
    {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    self.lineView = [[UIView alloc] initWithFrame:[self lineViewRectForBounds:self.bounds]];
    [self addSubview:self.lineView];
    
    self.titleLabel.numberOfLines = 1;
    self.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.exclusiveTouch = YES;
    self.adjustsImageWhenDisabled = NO;
    self.adjustsImageWhenHighlighted = NO;
    self.showsTouchWhenHighlighted = NO;
    
    self.lineView.backgroundColor = self.lineColor ?: [UIColor clearColor];
}

- (CGSize)intrinsicContentSize
{
    CGSize size = [super intrinsicContentSize];
    size.height += self.verticalMargin;
    size.width += self.horizontalMargin;
    return size;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.lineView.backgroundColor = self.highlighted ? self.lineHighlightedColor : self.lineColor;
    self.lineView.frame = [self lineViewRectForBounds:self.bounds];

    self.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.contentEdgeInsets = UIEdgeInsetsMake(0, self.horizontalMargin, 0, 0);

    self.imageEdgeInsets = UIEdgeInsetsMake(self.verticalMargin*0.5, self.frame.size.width-self.imageView.frame.size.width+self.horizontalMargin*0.5, self.verticalMargin*0.5, -self.titleLabel.frame.size.width+self.horizontalMargin*0.5);
}

- (CGRect)lineViewRectForBounds:(CGRect)bounds {
    return CGRectMake(0, self.bounds.size.height - self.lineHeight, self.bounds.size.width, self.lineHeight);
}

@end
