//
//  QINetwork+Pulmuone.h
//  management
//
//  Created by Quintet on 26/06/2018.
//  Copyright © 2018 Pulmuone. All rights reserved.
//

#import "QINetwork.h"

@interface QINetwork (Pulmuone)

+ (NSDictionary *)defaultParameterWithServiceParameter:(NSDictionary *)parameters;

+ (void)loginWithUserID:(NSString *)userID
               password:(NSString *)password
           successBlock:(void (^)(NSDictionary *responseDictionary))successBlock
           failureBlock:(void (^)(NSError *error))failureBlock;

+ (void)logOutWithSuccessBlock:(void (^)(NSDictionary *responseDictionary))successBlock
                  failureBlock:(void (^)(NSError *error))failureBlock;

+ (void)commonCodeWithGroupCode:(NSString *)groupCode
                   successBlock:(void (^)(NSDictionary *responseDictionary))successBlock
                   failureBlock:(void (^)(NSError *error))failureBlock;

+ (void)scheduleListForDate:(NSString *)searchDate
               successBlock:(void (^)(NSDictionary *responseDictionary))successBlock
               failureBlock:(void (^)(NSError *error))failureBlock
            authFailedBlock:(void(^)(void(^)(void)))authFailedBlock;

+ (void)scheduleDetailByID:(NSString *)scheduleID
              successBlock:(void (^)(NSDictionary *responseDictionary))successBlock
              failureBlock:(void (^)(NSError *error))failureBlock
           authFailedBlock:(void(^)(void(^)(void)))authFailedBlock;

+ (void)saveScheduleWithDictionary:(NSDictionary *)schedule
                      successBlock:(void (^)(NSDictionary *responseDictionary))successBlock
                      failureBlock:(void (^)(NSError *error))failureBlock
                   authFailedBlock:(void(^)(void(^)(void)))authFailedBlock;

+ (void)removeScheduleByID:(NSString *)scheduleID
              successBlock:(void (^)(NSDictionary *responseDictionary))successBlock
              failureBlock:(void (^)(NSError *error))failureBlock
           authFailedBlock:(void(^)(void(^)(void)))authFailedBlock;

+ (void)taskListBetweenStartDate:(NSString *)startDate
                         endDate:(NSString *)endDate
                    successBlock:(void (^)(NSDictionary *responseDictionary))successBlock
                    failureBlock:(void (^)(NSError *error))failureBlock
                 authFailedBlock:(void(^)(void(^)(void)))authFailedBlock;

+ (void)workInOutWithType:(NSString *)type
                 latitude:(double)latitude
                longitude:(double)longitude
                  address:(NSString *)address
             successBlock:(void (^)(NSDictionary *responseDictionary))successBlock
             failureBlock:(void (^)(NSError *error))failureBlock
          authFailedBlock:(void(^)(void(^)(void)))authFailedBlock;

#pragma mark - O365 일정조회
+ (void)o365ScheduleListForDate:(NSString *)searchDate
                          email:(NSString *)email
                   successBlock:(void (^)(NSDictionary *responseDictionary))successBlock
                   failureBlock:(void (^)(NSError *error))failureBlock
                authFailedBlock:(void(^)(void(^)(void)))authFailedBlock;

+ (void)saveO365ScheduleWithDictionary:(NSDictionary *)schedule
                          successBlock:(void (^)(NSDictionary *responseDictionary))successBlock
                          failureBlock:(void (^)(NSError *error))failureBlock
                       authFailedBlock:(void(^)(void(^)(void)))authFailedBlock;

@end
