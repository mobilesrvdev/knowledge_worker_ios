//
//  ConfigFactory.m
//  management
//
//  Created by Quintet on 25/06/2018.
//  Copyright © 2018 Pulmuone. All rights reserved.
//

#import "ConfigFactory.h"
#import "QIContainer+Pulmuone.h"
#import "QINetwork+Pulmuone.h"
#import "ComponentStyler.h"

#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "IQKeyboardManager.h"
#import "SVProgressHUD.h"

@import Firebase;

@implementation ConfigFactory

+ (void)configWithLaunchOptions:(NSDictionary *)launchOptions {
    [[QIContainer sharedContainer] setLaunchOptions:launchOptions];
    
    [FIRApp configure]; // Firebase
    
    [ConfigFactory startLogger];
    [ConfigFactory startCrashlytics];
    
    [ConfigFactory tagVersion];
    [ConfigFactory initializeNetwork];
    [ConfigFactory configureKeyboard];
    [ConfigFactory configProgressHUD];
    [ConfigFactory configNavigationBar];
}

+ (void)startLogger {
    if (@available(iOS 10, *)) {
        [DDLog addLogger:[DDOSLogger sharedInstance] withLevel:ddLogLevel];
    } else {
        [DDLog addLogger:[DDASLLogger sharedInstance] withLevel:ddLogLevel];
        [DDLog addLogger:[DDTTYLogger sharedInstance] withLevel:ddLogLevel];
    }
}

+ (void)startCrashlytics {
    [Fabric with:@[[Crashlytics class]]];
}

+ (void)tagVersion
{
#if DEBUG
    UILabel *versionLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 0, 0)];
    versionLabel.text = [NSString stringWithFormat:@"%@(%@)", appVersion(), appBuildVersion()];
    versionLabel.numberOfLines = 2;
    versionLabel.font = [UIFont systemFontOfSize:10.0f];
    versionLabel.shadowOffset = CGSizeMake(1, 1);
    versionLabel.shadowColor = [UIColor whiteColor];
    versionLabel.layer.zPosition = 1000;
    [versionLabel sizeToFit];
    versionLabel.frame = CGRectMake(10, [[UIScreen mainScreen] bounds].origin.y + [[UIScreen mainScreen] bounds].size.height - versionLabel.frame.size.height - 10, versionLabel.frame.size.width, versionLabel.frame.size.height);
    UIWindow *window = [[UIApplication sharedApplication] keyWindow] ?: [[[UIApplication sharedApplication] windows] lastObject];
    [window addSubview:versionLabel];
    [window bringSubviewToFront:versionLabel];
#endif
}

+ (void)initializeNetwork {
    NSLog(@"BASE_URL: %@", BASE_URL);
    QINetwork *network = [QINetwork managerWithBaseURL:BASE_URL pathBlock:^NSDictionary *{
        return @{
                 NSStringFromSelector(@selector(loginWithUserID:password:successBlock:failureBlock:)) : URL_PATH_LOGIN,
                 
                 NSStringFromSelector(@selector(logOutWithSuccessBlock:failureBlock:)) : URL_PATH_LOGOUT,
                 NSStringFromSelector(@selector(commonCodeWithGroupCode:successBlock:failureBlock:)) : URL_PATH_COMM_CODE,
                 NSStringFromSelector(@selector(scheduleListForDate:successBlock:failureBlock:authFailedBlock:)) : URL_PATH_SCHEDULE_LIST,
                 NSStringFromSelector(@selector(scheduleDetailByID:successBlock:failureBlock:authFailedBlock:)) : URL_PATH_SCHEDULE_DETAIL,
                 NSStringFromSelector(@selector(saveScheduleWithDictionary:successBlock:failureBlock:authFailedBlock:)) : URL_PATH_SCHEDULE_DETAIL_SAVE,
                 NSStringFromSelector(@selector(removeScheduleByID:successBlock:failureBlock:authFailedBlock:)) : URL_PATH_SCHEDULE_DETAIL_REMOVE,
                 NSStringFromSelector(@selector(taskListBetweenStartDate:endDate:successBlock:failureBlock:authFailedBlock:)) : URL_PATH_TASK_LIST,
                 NSStringFromSelector(@selector(workInOutWithType:latitude:longitude:address:successBlock:failureBlock:authFailedBlock:)) : URL_PATH_WORK_IN_OUT,
                 //O365 일정 API 추가
                 NSStringFromSelector(@selector(o365ScheduleListForDate:email:successBlock:failureBlock:authFailedBlock:)) : URL_PATH_O365_SCHEDULE_LIST,
                 NSStringFromSelector(@selector(saveO365ScheduleWithDictionary:successBlock:failureBlock:authFailedBlock:)) : URL_PATH_O365_SCHEDULE_DETAIL_SAVE
                 };
    }];
    [network setShouldSucceed:^BOOL(NSDictionary * _Nullable response) {
        return [response[@"success"] boolValue] || [response[@"errorType"] isEqualToNumber:@(200)];
    }];
    [network setErrorHandler:^(NSError *error) {
        switch (error.code)
        {
            default:
                DDLogError(@"[Network] error : %@", error);
                break;
        }
    }];
    [network setGenerateError:^NSError * _Nullable(NSDictionary * _Nullable response, NSError * _Nullable error) {
        if (error) {
            return error;
        }
        return [NSError errorWithDomain:QINetworkErrorDomain code:[response[@"errorType"] integerValue] userInfo:@{NSLocalizedDescriptionKey:response[@"message"]?:defaultErrorString()}];
    }];
    [network setRetryCountBlock:^NSUInteger{
        return NETWORK_RETRY_COUNT;
    }];
    [network setRetryIntervalBlock:^NSTimeInterval{
        return NETWORK_RETRY_INTERVAL;
    }];
    [network setTimeoutIntervalBlock:^NSTimeInterval{
        return NETWORK_RETRY_TIMEOUT;
    }];
    [network setAuthBlock:^(QIAuthCompleteHandler completeHandler) {
        if (appUserID() && appUserPassword()) {
            [QINetwork loginWithUserID:appUserID() password:appUserPassword() successBlock:^(NSDictionary *responseDictionary) {
                if ([QINetwork manager].shouldSucceed(responseDictionary)) {
                    [[QIContainer sharedContainer] setUserInfo:responseDictionary[@"userInfo"]];
                    completeHandler(QINetworkAuthResultSuccess);
                } else {
                    DDLogWarn(@"[Network] Auto login failed with success false : %@", responseDictionary);
                    completeHandler(QINetworkAuthResultFailed);
                }
            } failureBlock:^(NSError *error) {
                DDLogWarn(@"[Network] Auto login failed with error : %@", error);
                completeHandler(QINetworkAuthResultFailed);
            }];
        } else {
            DDLogWarn(@"[Network] Auto login not available due to lack of saved data");
            completeHandler(QINetworkAuthResultFailed);
        }
    }];
    [network setAuthChallengeExceptionPaths:@[URL_PATH_LOGIN]];
    [network setAuthChallengeBlock:^BOOL(NSDictionary * _Nullable response, NSError * _Nullable error) {
        return [response[@"errorType"] isEqualToNumber:@(200410)];
    }];
    [network setReachabilityStatusChangeBlock:^(QINetworkReachabilityStatus status) {
        if (status == QINetworkReachabilityStatusNotReachable || status == QINetworkReachabilityStatusUnknown)
        {
            [[QINetwork manager] suspend];
            DDLogDebug(@"[Network] suspended");
        }
        else
        {
            [[QINetwork manager] resume];
            DDLogDebug(@"[Network] resumed");
        }
    }];
}

+ (void)configureKeyboard {
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = YES;
    [IQKeyboardManager sharedManager].layoutIfNeededOnUpdate = YES;
}

+ (void)configProgressHUD {
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD setMaximumDismissTimeInterval:5.0f];
    [SVProgressHUD setMinimumDismissTimeInterval:1.0f];
}

+ (void)configNavigationBar {
    [ComponentStyler navigationBarCustomize];
}

@end
