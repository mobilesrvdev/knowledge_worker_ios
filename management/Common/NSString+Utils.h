//
//  NSString+Utils.h
//
//  Created by bk on 2022/03/30.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (Utils)

- (NSComparisonResult)compareVersion:(NSString *)version;

@end

NS_ASSUME_NONNULL_END
