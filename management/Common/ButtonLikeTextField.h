//
//  ButtonLikeTextField.h
//  management
//
//  Created by Quintet on 28/06/2018.
//  Copyright © 2018 Pulmuone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextFieldLikeButton : UIButton
@property (nonatomic) CGFloat lineHeight;
@property (nonatomic) CGFloat verticalMargin;
@property (nonatomic) CGFloat horizontalMargin;
@property (nonatomic, strong) UIColor *lineColor;
@property (nonatomic, strong) UIColor *lineHighlightedColor;
@end
