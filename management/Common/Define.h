//
//  Define.h
//  management
//
//  Created by Quintet on 25/06/2018.
//  Copyright © 2018 Pulmuone. All rights reserved.
//

#ifndef Define_h
#define Define_h

#import "CocoaLumberjack.h"

//#ifndef DEBUG
//static const DDLogLevel ddLogLevel = DDLogLevelVerbose;
//#define PLIST_URL [NSURL URLWithString:@"https://am.pulmuone.com/publishApp/nightly/management.plist"]
//#define SERVER_URL                              @"http://129.150.161.144:9073"
//#else
static const DDLogLevel ddLogLevel = DDLogLevelAll;
//#define PLIST_URL [NSURL URLWithString:@"https://am.pulmuone.com/publishApp/nightly/management.plist"]
#define PLIST_URL                               [NSURL URLWithString:@"https://am.pulmuone.com/publishApp/management.plist"]
#define SERVER_URL                              @"https://am.pulmuone.com"
#define SERVER_URL_DEV                          @"https://amdev.pulmuone.com"
//å#endif

#define USER_INFO_ID_KEY                        @"AMUserID"
#define USER_INFO_PW_KEY                        @"AMUserPassword"

#define BASE_URL                                [NSURL URLWithString:SERVER_URL]
#define NETWORK_RETRY_COUNT                     3
#define NETWORK_RETRY_INTERVAL                  20
#define NETWORK_RETRY_TIMEOUT                   50

#define URL_PATH_LOGIN                          @"/activityManager/api/common/loginConfirm.do"
#define URL_PATH_LOGOUT                         @"/activityManager/api/common/logout.do"
#define URL_PATH_COMM_CODE                      @"/activityManager/api/common/getCommCodeList.do"
#define URL_PATH_SCHEDULE_LIST                  @"/activityManager/api/workScheduleListByDate.do"
#define URL_PATH_SCHEDULE_DETAIL                @"/activityManager/api/workScheduleDtl.do"
#define URL_PATH_SCHEDULE_DETAIL_SAVE           @"/activityManager/api/saveWorkSchedule.do"
#define URL_PATH_SCHEDULE_DETAIL_REMOVE         @"/activityManager/api/removeWorkSchedule.do"
#define URL_PATH_TASK_LIST                      @"/activityManager/api/getTaskList.do"
#define URL_PATH_WORK_IN_OUT                    @"/activityManager/api/addWrkInOut.do"
//wooya510@20200518:ADD
#define URL_PATH_O365_SCHEDULE_LIST             @"/activityManager/employee/getMoffice365ScheduleList.do" //O365스케줄 목록, /activityManager/employee/getTestMoffice365ScheduleList.do
#define URL_PATH_O365_SCHEDULE_DETAIL_SAVE      @"/activityManager/workSchedule/addMEmpScheduleGrid.do" //O365스케쥴에서 선택한 일정등록

//#define REQUEST_PARAM_LANG                      @"lang"
#define RESPONSE_ERROR_TYPE                     @"errorType"

#define STORYBOARD_NAME_MAIN                    @"Main"
#define VIEW_CONTROLLER_NAME_MAIN               @"MainViewController"
#define VIEW_CONTROLLER_NAME_LOGIN              @"LoginViewController"
#define VIEW_CONTROLLER_NAME_SCHEDULE_LIST      @"ScheduleListViewController"
#define VIEW_CONTROLLER_NAME_SCHEDULE_DETAIL    @"ScheduleDetailViewController"

#define STORYBOARD_MAIN                         [UIStoryboard storyboardWithName:STORYBOARD_NAME_MAIN bundle:nil]
#define VIEW_CONTROLLER_MAIN                    [STORYBOARD_MAIN \
instantiateViewControllerWithIdentifier:VIEW_CONTROLLER_NAME_MAIN]
#define VIEW_CONTROLLER_LOGIN                   [STORYBOARD_MAIN \
instantiateViewControllerWithIdentifier:VIEW_CONTROLLER_NAME_LOGIN]
#define VIEW_CONTROLLER_SCHEDULE_LIST           [STORYBOARD_MAIN \
instantiateViewControllerWithIdentifier:VIEW_CONTROLLER_NAME_SCHEDULE_LIST]
#define VIEW_CONTROLLER_SCHEDULE_DETAIL         [STORYBOARD_MAIN \
instantiateViewControllerWithIdentifier:VIEW_CONTROLLER_NAME_SCHEDULE_DETAIL]

#define SCHEDULE_DID_UPDATE_ON_DATE             @"com.pulumone.schedule.notification.did.change"
#define SCHEDULE_DID_REMOVE                     @"com.pulumone.schedule.notification.did.remove"

#define MAX_EMAIL_LENGTH                        50
#define MAX_PASSWORD_LENGTH                     50
#define MAX_DESCRIPTION_LENGTH                  500
#define BIG_FLOAT_FOR_UI                        1000

#define BASE_FONT_NAME                          @"NotoSansCJKkr"
#define FONT_NORMAL_NAME                        @"Regular"
#define FONT_BOLD_NAME                          @"Medium"
#define FONT_COMBINE_FORMAT                     @"%@-%@"
#define FONT_NAME(font)                         [NSString stringWithFormat:FONT_COMBINE_FORMAT,BASE_FONT_NAME,font]

#define FONT_NORMAL_SIZE(arg)                   [UIFont fontWithName:FONT_NAME(FONT_NORMAL_NAME) size:arg]
#define FONT_BOLD_SIZE(arg)                     [UIFont fontWithName:FONT_NAME(FONT_BOLD_NAME) size:arg]

#define NAVIGATION_BACK_BUTTON                  @"btnBack"
#define NAVIGATION_BACK_BUTTON_IMAGE            [UIImage imageNamed:NAVIGATION_BACK_BUTTON]

#define ROUNDED_BOX_CORNER_RADIUS                   10.0f

#define BUTTON_LIKE_TEXTFIELD_LINE_HEIGHT           1.0f
#define BUTTON_LIKE_TEXTFIELD_LINE_COLOR            ColorRGBA(242, 242, 242, 1)
#define BUTTON_LIKE_TEXTFIELD_LINE_COLOR_PRESSED    ColorRGBA(141, 198, 63, 1)
#define BUTTON_LIKE_TEXTFIELD_ICON                  @"imgDropDown"
#define BUTTON_LIKE_TEXTFIELD_ICON_PRESSED          @"imgDropUp"

#define EMAIL_POSTFIX                           @"@pulmuone.com"

#endif /* Define_h */
