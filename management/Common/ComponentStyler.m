//
//  ComponentStyler.m
//  management
//
//  Created by Quintet on 26/06/2018.
//  Copyright © 2018 Pulmuone. All rights reserved.
//

#import "ComponentStyler.h"
#import "TextFieldLikeButton.h"
#import "CenterImageButton.h"
#import "management-Swift.h"

@implementation ComponentStyler

+ (void)navigationBarCustomize {
    UIImage *backImage = NAVIGATION_BACK_BUTTON_IMAGE;
    [[UINavigationBar appearance] setBackIndicatorImage:backImage];
    [[UINavigationBar appearance] setBackIndicatorTransitionMaskImage:backImage];
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(-BIG_FLOAT_FOR_UI, 0) forBarMetrics:UIBarMetricsDefault];
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(-BIG_FLOAT_FOR_UI, 0) forBarMetrics:UIBarMetricsCompact];
}

+ (SkyFloatingLabelTextField *)defaultTextField:(SkyFloatingLabelTextField *)textField {
    textField.titleFont = FONT_NORMAL_SIZE(16.0f);
    textField.titleColor = ColorRGBA(125, 125, 125, 1);
    textField.lineColor = ColorRGBA(242, 242, 242, 1);;
    textField.selectedTitleColor = ColorRGBA(141, 198, 63, 1);
    textField.placeholderFont = FONT_NORMAL_SIZE(18.0f);
    textField.placeholderColor = ColorRGBA(178, 178, 178, 1);
    textField.font = FONT_NORMAL_SIZE(18.0f);
    textField.textColor = ColorRGBA(51, 51, 51, 1);
    textField.selectedLineColor = ColorRGBA(141, 198, 63, 1);
    return textField;
}

+ (UIButton *)defaultButtonStylePositiveBig:(UIButton *)button {
    [button setBackgroundImage:imageFromColor(ColorRGBA(141, 198, 63, 1)) forState:UIControlStateNormal];
    [button setBackgroundImage:imageFromColor(ColorRGBA(229, 229, 229, 1)) forState:UIControlStateDisabled];
    [button setTitleColor:ColorRGBA(255, 255, 255, 1) forState:UIControlStateNormal];
    [button setTitleColor:ColorRGBA(178, 178, 178, 1) forState:UIControlStateDisabled];
    [[button titleLabel] setFont:FONT_NORMAL_SIZE(21.0f)];
    return button;
}

+ (UIButton *)defaultButtonStylePositiveMedium:(UIButton *)button {
    [button setBackgroundImage:imageFromColor(ColorRGBA(141, 198, 63, 1)) forState:UIControlStateNormal];
    [button setTitleColor:ColorRGBA(255, 255, 255, 1) forState:UIControlStateNormal];
    [[button titleLabel] setFont:FONT_NORMAL_SIZE(17.0f)];
    return button;
}

+ (UIButton *)defaultButtonStyleCancelMedium:(UIButton *)button {
    [button setBackgroundImage:imageFromColor(ColorRGBA(229, 229, 229, 1)) forState:UIControlStateNormal];
    [button setTitleColor:ColorRGBA(126, 126, 126, 1) forState:UIControlStateNormal];
    [button setTitleColor:ColorRGBA(255, 255, 255, 1) forState:UIControlStateHighlighted];
    [[button titleLabel] setFont:FONT_NORMAL_SIZE(17.0f)];
    return button;
}

+ (UIView *)mainBackgroundColor:(UIView *)view {
    view.backgroundColor = ColorRGBA(141, 198, 63, 1);
    return view;
}

+ (UINavigationBar *)defaultNavigationBar:(UINavigationBar *)navigationBar {
    /**
     iOS 15.0  부터 네비게이션 기본색상 지정을 appearance를 통해 지정하도록 변경되어 수정함.
     modify date : 22/05/02 , 모바일개발팀 박희준
     */
    
    UIColor *barColor = ColorRGBA(141, 198, 63, 1); // 네비바 색상
    UIColor *textColor = ColorRGBA(255, 255, 255, 1); // 글자 색상
    NSDictionary *titleTextAttrs = @{
        NSFontAttributeName               :   FONT_BOLD_SIZE(18.0f),
        NSForegroundColorAttributeName    :   textColor};
    
    if (@available(iOS 15.0, *)) {
        UINavigationBarAppearance *navigationBarAppearance = [[UINavigationBarAppearance alloc] init];
        [navigationBarAppearance configureWithDefaultBackground];
        [navigationBarAppearance setBackgroundColor:barColor];
        [navigationBarAppearance setTitleTextAttributes:titleTextAttrs];
        
        navigationBar.topItem.backButtonDisplayMode = UINavigationItemBackButtonDisplayModeMinimal;
        
        [navigationBar setScrollEdgeAppearance:navigationBarAppearance]; // 스크롤 전
        [navigationBar setStandardAppearance:navigationBarAppearance]; // 스크롤 중
//        [navigationBar setCompactAppearance:navigationBarAppearance]; // 없어도 됨. 뭐하는 앤지 확인 필요
    }
    
    navigationBar.barTintColor = barColor;
    navigationBar.tintColor = textColor;
    navigationBar.titleTextAttributes = titleTextAttrs;
    navigationBar.translucent = NO;
    
    return navigationBar;
}

+ (UIView *)roundedView:(UIView *)view {
    view.layer.cornerRadius = ROUNDED_BOX_CORNER_RADIUS;
    return view;
}

+ (TextFieldLikeButton *)defaultButtonLikeTextField:(TextFieldLikeButton *)button {
    button.lineHeight = BUTTON_LIKE_TEXTFIELD_LINE_HEIGHT;
    button.lineColor = BUTTON_LIKE_TEXTFIELD_LINE_COLOR;
    button.lineHighlightedColor = BUTTON_LIKE_TEXTFIELD_LINE_COLOR_PRESSED;
    button.titleLabel.font = FONT_BOLD_SIZE(17.0f);
    [button setTitleColor:ColorRGBA(51, 51, 51, 1) forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:BUTTON_LIKE_TEXTFIELD_ICON] forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:BUTTON_LIKE_TEXTFIELD_ICON_PRESSED] forState:UIControlStateHighlighted];
    
    return button;
}

+ (TextFieldLikeButton *)defaultButtonLikeDropDown:(TextFieldLikeButton *)button {
    button.lineHeight = BUTTON_LIKE_TEXTFIELD_LINE_HEIGHT;
    button.lineColor = BUTTON_LIKE_TEXTFIELD_LINE_COLOR;
    button.lineHighlightedColor = BUTTON_LIKE_TEXTFIELD_LINE_COLOR_PRESSED;
    button.titleLabel.font = FONT_BOLD_SIZE(10.0f);
    [button setTitleColor:ColorRGBA(51, 51, 51, 1) forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:BUTTON_LIKE_TEXTFIELD_ICON] forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:BUTTON_LIKE_TEXTFIELD_ICON_PRESSED] forState:UIControlStateHighlighted];
    
    return button;
}

+ (UITextView *)defaultTextView:(UITextView *)textView {
    textView.font = FONT_NORMAL_SIZE(17.0f);
    textView.textColor = ColorRGBA(51, 51, 51, 1);
    return textView;
}

+ (UITextView *)attentionTextView:(UITextView *)textView {
    textView.font = FONT_NORMAL_SIZE(17.0f);
    textView.textColor = ColorRGBA(122, 172, 53, 1);
    textView.editable = NO;
    return textView;
}

+ (CenterImageButton *)mainScheduleButon:(CenterImageButton *)button {
    button.titleLabel.font = FONT_NORMAL_SIZE(15.0f);
    button.titleLabel.textColor = ColorRGBA(255, 255, 255, 1);
    return button;
}

+ (UILabel *)mainHeaderLabel:(UILabel *)label {
    label.font = FONT_BOLD_SIZE(19.0f);
    label.textColor = ColorRGBA(51, 51, 51, 1);
    return label;
}

+ (UILabel *)mainSubHeaderLabel:(UILabel *)label {
    label.font = FONT_NORMAL_SIZE(19.0f);
    label.textColor = ColorRGBA(125, 125, 125, 1);
    return label;
}

+ (UILabel *)mainWorkTimeLabel:(UILabel *)label {
    label.font = FONT_BOLD_SIZE(18.0f);
    label.textColor = ColorRGBA(51, 51, 51, 1);
    return label;
}

+ (UILabel *)mainMessageLabel:(UILabel *)label {
    label.font = FONT_NORMAL_SIZE(15.0f);
    label.textColor = ColorRGBA(31, 70, 32, 1);
    label.layer.cornerRadius = 0;
    label.layer.backgroundColor = [ColorRGBA(255, 255, 255, 1) CGColor];
    return label;
}

+ (UILabel *)scheduleHeaderDateLabel:(UILabel *)label {
    label.font = FONT_NORMAL_SIZE(18.0f);
    label.textColor = ColorRGBA(51, 51, 51, 1);
    return label;
}

+ (UIButton *)scheduleAddButton:(UIButton *)button {
    button.titleLabel.font = FONT_NORMAL_SIZE(14.0f);
    button.titleLabel.textColor = ColorRGBA(255, 255, 255, 1);
    button.layer.borderColor = [ColorRGBA(255, 255, 255, 1) CGColor];
    button.layer.borderWidth = 1.0f;
    return button;
}

+ (UILabel *)scheduleEmptyTitleLabel:(UILabel *)label {
    label.font = FONT_NORMAL_SIZE(18.0f);
    label.textColor = ColorRGBA(125, 125, 125, 1);
    return label;
}

+ (UILabel *)scheduleEmptyDetailLabel:(UILabel *)label {
    label.font = FONT_NORMAL_SIZE(15.0f);
    label.textColor = ColorRGBA(153, 153, 153, 1);
    return label;
}

+ (UILabel *)scheduleCellLabel:(UILabel *)label {
    label.font = FONT_NORMAL_SIZE(14.0f);
    label.textColor = ColorRGBA(125, 125, 125, 1);
    return label;
}

+ (UILabel *)scheduleCellOrderLabel:(UILabel *)label {
    label.font = FONT_NORMAL_SIZE(15.0f);
    label.textColor = ColorRGBA(125, 125, 125, 1);
    return label;
}

+ (UILabel *)scheduleSubTitleLabel:(UILabel *)label required:(BOOL)required {
    label.font = FONT_NORMAL_SIZE(15.0f);
    label.textColor = ColorRGBA(153, 153, 153, 1);
    
    if (required && ![label.text containsString:@"*"]) {
        NSString *originString = label.text;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:originString];
        [attributedString appendAttributedString:[[NSMutableAttributedString alloc] initWithString:@"*" attributes:@{NSFontAttributeName:FONT_NORMAL_SIZE(15.0f),NSForegroundColorAttributeName:ColorRGBA(141, 198, 63, 1)}]];
        label.attributedText = [attributedString copy];
    }
    
    return label;
}

@end
