//
//  QIContainer+Pulmuone.h
//  management
//
//  Created by Quintet on 25/06/2018.
//  Copyright © 2018 Pulmuone. All rights reserved.
//

#import "QIContainer.h"

@interface QIContainer (Pulmuone)

//usage
@property (nonatomic, weak) NSString *property;

- (NSString *)suiteName;
- (NSDictionary *)setupDefaults;
- (NSString *)transformKey:(NSString *)key;

@end
