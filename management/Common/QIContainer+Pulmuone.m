//
//  QIContainer+Pulmuone.m
//  management
//
//  Created by Quintet on 25/06/2018.
//  Copyright © 2018 Pulmuone. All rights reserved.
//

#import "QIContainer+Pulmuone.h"

NSString * const kAMPropertiesGroupName  = @"group.com.activity.management.properies";
NSString * const kAMPropertiesPrefix = @"AMProperties_";
NSString * const kAMSharedAccessGroupIdentifier = @"com.activity.management.container";
NSString * const kAMUserDefaultNameInPlist = @"AMProperties";

@implementation QIContainer (Pulmuone)

@dynamic property;

- (NSString *)suiteName {
    return kAMPropertiesGroupName;
}

- (NSDictionary *)setupDefaults {
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:kAMUserDefaultNameInPlist];
}

- (NSString *)transformKey:(NSString *)key {
    key = [key stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[key substringToIndex:1] uppercaseString]];
    NSString *transformedKey = [NSString stringWithFormat:@"%@%@", kAMPropertiesPrefix, key];
    return transformedKey;
}

@end
