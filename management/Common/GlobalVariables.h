//
//  GlobalVariables.h
//  management
//
//  Created by 이주한 on 2020/05/08.
//  Copyright © 2020 Pulmuone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "O365ScheduleViewModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface GlobalVariables : NSObject
//Office365일정 목록에서 체크된 것만 저장
@property (nonatomic, retain) NSMutableArray<O365ScheduleViewModel *> * checked365Schedules;
@property (nonatomic, strong) NSArray <NSDictionary *> *taskList;
@property (nonatomic, strong) NSArray <NSDictionary *> *dutyList;
@property (nonatomic, strong) NSString *email;
+(GlobalVariables*)singleton;
@end

NS_ASSUME_NONNULL_END
