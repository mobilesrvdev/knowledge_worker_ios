//
//  ComponentStyler.h
//  management
//
//  Created by Quintet on 26/06/2018.
//  Copyright © 2018 Pulmuone. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SkyFloatingLabelTextField;
@class TextFieldLikeButton;
@class CenterImageButton;

@interface ComponentStyler : NSObject

+ (void)navigationBarCustomize;

+ (SkyFloatingLabelTextField *)defaultTextField:(SkyFloatingLabelTextField *)textField;

+ (UIButton *)defaultButtonStylePositiveBig:(UIButton *)button;
+ (UIButton *)defaultButtonStylePositiveMedium:(UIButton *)button;
+ (UIButton *)defaultButtonStyleCancelMedium:(UIButton *)button;

+ (UIView *)mainBackgroundColor:(UIView *)view;

+ (UINavigationBar *)defaultNavigationBar:(UINavigationBar *)navigationBar;

+ (UIView *)roundedView:(UIView *)view;

+ (TextFieldLikeButton *)defaultButtonLikeTextField:(TextFieldLikeButton *)button;
+ (TextFieldLikeButton *)defaultButtonLikeDropDown:(TextFieldLikeButton *)button;

+ (UITextView *)defaultTextView:(UITextView *)textView;
+ (UITextView *)attentionTextView:(UITextView *)textView;

+ (CenterImageButton *)mainScheduleButon:(CenterImageButton *)button;

+ (UILabel *)mainHeaderLabel:(UILabel *)label;
+ (UILabel *)mainSubHeaderLabel:(UILabel *)label;
+ (UILabel *)mainWorkTimeLabel:(UILabel *)label;
+ (UILabel *)mainMessageLabel:(UILabel *)label;

+ (UILabel *)scheduleHeaderDateLabel:(UILabel *)label;
+ (UIButton *)scheduleAddButton:(UIButton *)button;
+ (UILabel *)scheduleEmptyTitleLabel:(UILabel *)label;
+ (UILabel *)scheduleEmptyDetailLabel:(UILabel *)label;

+ (UILabel *)scheduleCellLabel:(UILabel *)label;
+ (UILabel *)scheduleCellOrderLabel:(UILabel *)label;

+ (UILabel *)scheduleSubTitleLabel:(UILabel *)label required:(BOOL)required;

@end
