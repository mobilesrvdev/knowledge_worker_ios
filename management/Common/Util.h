//
//  Util.h
//  management
//
//  Created by Quintet on 25/06/2018.
//  Copyright © 2018 Pulmuone. All rights reserved.
//

#ifndef Util_h
#define Util_h

#define VariableName(arg)       (@#arg)

#define MaterialIconSize(arg)   [UIFont fontWithName:@"MaterialIcons-Regular" size:arg]

#define AMLocalizedString(arg)  NSLocalizedString(arg, nil)

#define ColorRGBA(r,g,b,a) [UIColor colorWithRed:(r)/255.f green:(g)/255.f blue:(b)/255.f alpha:(a)]

static inline NSString * versionStringFromPlist(NSData *data) {
    NSError *plistError;
    NSPropertyListFormat format;
    id plist = [NSPropertyListSerialization propertyListWithData:data options:NSPropertyListImmutable format:&format error:&plistError];
    if ([plist isKindOfClass:[NSDictionary class]]) {
        NSDictionary *metaDictionary = (NSDictionary *)plist;
        NSString *versionString = [metaDictionary[@"items"] lastObject][@"metadata"][@"bundle-version"];
        return versionString;
    }
    return nil;
}

static inline NSString * defaultErrorString() {
    return AMLocalizedString(@"네트워크 오류입니다. 잠시후에 접속하시기 바랍니다.");
}

#import <UIKit/UIKit.h>

static inline UIImage * imageFromColor(UIColor *color) {
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

static inline UIViewController* _topMostController(UIViewController *cont) {
    UIViewController *topController = cont;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    if ([topController isKindOfClass:[UINavigationController class]]) {
        UIViewController *visible = ((UINavigationController *)topController).visibleViewController;
        if (visible) {
            topController = visible;
        }
    }
    
    return (topController != cont ? topController : nil);
}

static inline UIViewController* topMostController() {
    UIViewController *topController = [[UIApplication sharedApplication] delegate].window.rootViewController;
    
    UIViewController *next = nil;
    
    while ((next = _topMostController(topController)) != nil) {
        topController = next;
    }
    
    return topController;
}

static inline BOOL canPushFromViewController(UIViewController *viewController) {
    return [viewController isKindOfClass:[UINavigationController class]] || [viewController.navigationController isKindOfClass:[UINavigationController class]];
}

static inline NSString * appVersion() {
    return ((NSString *)[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] ?: @"");
}

static inline NSString * appBuildVersion() {
    return ((NSString *)[[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey] ?: @"");
}

#import "QIContainer+Pulmuone.h"

static inline NSString * appUserID() {
    return [[QIContainer sharedKeychain] stringForKey:USER_INFO_ID_KEY];
}

static inline NSString * appUserPassword() {
    return [[QIContainer sharedKeychain] stringForKey:USER_INFO_PW_KEY];
}

static inline void saveOrRemoveStringForKey(NSString *string, NSString *key) {
    if ([string isKindOfClass:[NSString class]] && [string length]) {
        [[QIContainer sharedKeychain] setString:string forKey:key];
    } else {
        [[QIContainer sharedKeychain] removeObjectForKey:key];
    }
}

static inline void saveUserID(NSString *userID) {
    saveOrRemoveStringForKey(userID, USER_INFO_ID_KEY);
}

static inline void saveUserPassword(NSString *userPassword) {
    saveOrRemoveStringForKey(userPassword, USER_INFO_PW_KEY);
}

static inline void _feedbackNotification(int type) {
    if (@available(iOS 10.0, *)) {
        dispatch_async(dispatch_get_main_queue(), ^{
            UINotificationFeedbackGenerator *feedbackGenerator = [[UINotificationFeedbackGenerator alloc] init];
            [feedbackGenerator prepare];
            [feedbackGenerator notificationOccurred:type];
        });
    }
}

static inline void feedbackNotificationSuccess() {
    if (@available(iOS 10.0, *)) {
        _feedbackNotification(UINotificationFeedbackTypeSuccess);
    }
}

static inline void feedbackNotificationError() {
    if (@available(iOS 10.0, *)) {
       _feedbackNotification(UINotificationFeedbackTypeError);
    }
}

static inline void feedbackNotificationWarning() {
    if (@available(iOS 10.0, *)) {
        _feedbackNotification(UINotificationFeedbackTypeWarning);
    }
}

static inline void feedbackImpact() {
    if (@available(iOS 10.0, *)) {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIImpactFeedbackGenerator *feedbackGenerator = [[UIImpactFeedbackGenerator alloc] initWithStyle:UIImpactFeedbackStyleLight];
            [feedbackGenerator prepare];
            [feedbackGenerator impactOccurred];
        });
    }
}

static inline void feedbackSelection() {
    if (@available(iOS 10.0, *)) {
        dispatch_async(dispatch_get_main_queue(), ^{
            UISelectionFeedbackGenerator *feedbackGenerator = [[UISelectionFeedbackGenerator alloc] init];
            [feedbackGenerator prepare];
            [feedbackGenerator selectionChanged];
        });
    }
}

static inline BOOL canSetChecked() {
    @try {
        UIAlertAction *tryAction = [UIAlertAction actionWithTitle:nil style:UIAlertActionStyleDefault handler:nil];
        [tryAction setValue:@YES forKey:@"checked"];
    } @catch(id) {
        return NO;
    } @finally {
        return YES;
    }
}

static inline void setCheckedOnAlertAction(UIAlertAction *action, BOOL checked) {
    if (canSetChecked()) {
        [action setValue:@(checked) forKey:@"checked"];
    }
}

#import <CommonCrypto/CommonDigest.h>

static inline NSString * sha1HashString(NSString *string) {
    NSData *retData = [string dataUsingEncoding:NSUTF8StringEncoding];
    unsigned char *md;
    md = malloc(CC_SHA1_DIGEST_LENGTH);
    bzero(md, CC_SHA1_DIGEST_LENGTH);
    CC_SHA1(retData.bytes, (CC_LONG)retData.length, md);
    retData = [NSData dataWithBytes:md length:CC_SHA1_DIGEST_LENGTH];
    free(md);
    md = NULL;
    
    NSMutableString *result = [[NSMutableString alloc] initWithCapacity:retData.length * 2];
    for (size_t i = 0; i < retData.length; i++) {
        [result appendFormat:@"%02X", ((const uint8_t *) retData.bytes)[i]];
    }
    return [result copy];
}

static inline NSString * sha256HashString(NSString *string) {
    const char* utf8chars = [string UTF8String];
    unsigned char result[CC_SHA256_DIGEST_LENGTH];
    CC_SHA256(utf8chars, (CC_LONG)strlen(utf8chars), result);

    NSMutableString *ret = [NSMutableString stringWithCapacity:CC_SHA256_DIGEST_LENGTH*2];
    for(int i = 0; i<CC_SHA256_DIGEST_LENGTH; i++) {
        [ret appendFormat:@"%02x",result[i]];
    }
    return ret;
}

static NSDateFormatter *__dateFormatter = nil;
static NSDateFormatter *__dateFormatterDayOfWeek = nil;
static NSDateFormatter *__dateFormatterTime = nil;
static NSDateFormatter *__dateFormatterDisplay = nil;

static inline NSDateFormatter * _initDateFormatter() {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyyMMdd"];
    return dateFormatter;
}

static inline NSDateFormatter * _initDateFormatterDayOfWeek() {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEE"];
    return dateFormatter;
}

static inline NSDateFormatter * _initDateFormatterTime() {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HHmm"];
    return dateFormatter;
}

static inline NSDateFormatter * _initDateFormatterDisplay() {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy.MM.dd(E)"];
    return dateFormatter;
}

static inline NSDate * dateFromString(NSString *string) {
    __dateFormatter = __dateFormatter ?: _initDateFormatter();
    return [__dateFormatter dateFromString:string];
}

static inline NSString * stringFromDate(NSDate *date) {
    __dateFormatter = __dateFormatter ?: _initDateFormatter();
    return [__dateFormatter stringFromDate:date?:[NSDate date]];
}

static inline NSString * stringTimeFromDate(NSDate *date) {
    __dateFormatterTime = __dateFormatterTime ?: _initDateFormatterTime();
    return [__dateFormatterTime stringFromDate:date?:[NSDate date]];
}

static inline NSString * stringFromDateForDisplay(NSDate *date) {
    __dateFormatterDisplay = __dateFormatterDisplay ?: _initDateFormatterDisplay();
    return [__dateFormatterDisplay stringFromDate:date?:[NSDate date]];
}

static inline NSString * localizedStringFromDate(NSDate *date) {
    return [NSDateFormatter localizedStringFromDate:date?:[NSDate date] dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterNoStyle];
}

static inline NSString * localizedStringDayOfWeekFromDate(NSDate *date) {
    __dateFormatterDayOfWeek = __dateFormatterDayOfWeek ?: _initDateFormatterDayOfWeek();
    return [__dateFormatterDayOfWeek stringFromDate:date?:[NSDate date]];
}

static inline NSString * stringFromTimeNumber(NSString *time) {
    if (![time isKindOfClass:[NSString class]] || [time length] < 4) {
        return @"";
    }
    NSString *hour = [time substringToIndex:2];
    NSString *minute = [time substringWithRange:NSMakeRange(2, MIN([time length] - 2, 2))];
    return [NSString stringWithFormat:@"%@:%@", hour, minute];
}

static inline NSString * hourFromTimeString(NSString *time) {
    return [[time componentsSeparatedByString:@":"] firstObject];
}

static inline NSString * minuteFromTimeString(NSString *time) {
    return [[time componentsSeparatedByString:@":"] lastObject];
}

static inline NSDateComponents * dateComponentFromString(NSString *dateString, NSString *timeString) {
    if ([dateString isKindOfClass:[NSString class]] && [timeString isKindOfClass:[NSString class]]) {
        NSDate *date = dateFromString(dateString);
        NSString *time = stringFromTimeNumber(timeString);
        NSDateComponents *dateComponenet = [[NSCalendar autoupdatingCurrentCalendar] components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute fromDate:date];
        [dateComponenet setCalendar:[NSCalendar autoupdatingCurrentCalendar]];
        [dateComponenet setHour:[hourFromTimeString(time) integerValue]];
        [dateComponenet setMinute:[minuteFromTimeString(time) integerValue]];
        
        return dateComponenet;
    } else {
        return nil;
    }
}

#endif /* Util_h */
