//
//  QINetwork+Pulmuone.m
//  management
//
//  Created by Quintet on 26/06/2018.
//  Copyright © 2018 Pulmuone. All rights reserved.
//

#import "QINetwork+Pulmuone.h"

@implementation QINetwork (Pulmuone)

+ (NSDictionary *)defaultParameterWithServiceParameter:(NSDictionary *)parameters {
    NSMutableDictionary *defaultParam = [NSMutableDictionary
                                         dictionaryWithDictionary:
                                            @{
//                                              REQUEST_PARAM_LANG : @"ko",
                                              }];
    if ([parameters count]) {
        [defaultParam addEntriesFromDictionary:parameters];
    }
    
    return [defaultParam copy];
}

+ (void)loginWithUserID:(NSString *)userID
               password:(NSString *)password
           successBlock:(void (^)(NSDictionary *responseDictionary))successBlock
           failureBlock:(void (^)(NSError *error))failureBlock {
    NSDictionary *param = @{
                            @"userId"   :   userID ?: @"",
                            @"userPW"   :   password ?: @"",
                            };
    [[QINetwork manager] postReqestWithParameter:[QINetwork defaultParameterWithServiceParameter:param]
                                  selectorString:NSStringFromSelector(_cmd)
                                  concurrentMode:QINetworkConcurrentModeByParameter
                                    successBlock:successBlock
                                    failureBlock:failureBlock
                                 authFailedBlock:nil];
}

+ (void)logOutWithSuccessBlock:(void (^)(NSDictionary *responseDictionary))successBlock
                  failureBlock:(void (^)(NSError *error))failureBlock {
    NSDictionary *param = @{
                            };
    [[QINetwork manager] postReqestWithParameter:[QINetwork defaultParameterWithServiceParameter:param]
                                  selectorString:NSStringFromSelector(_cmd)
                                  concurrentMode:QINetworkConcurrentModeByPath
                                    successBlock:successBlock
                                    failureBlock:failureBlock
                                 authFailedBlock:nil];
}

+ (void)commonCodeWithGroupCode:(NSString *)groupCode
                   successBlock:(void (^)(NSDictionary *responseDictionary))successBlock
                   failureBlock:(void (^)(NSError *error))failureBlock {
    NSDictionary *param = @{
                            @"groupCode":groupCode?:[NSNull null],
                            };
    [[QINetwork manager] postReqestWithParameter:[QINetwork defaultParameterWithServiceParameter:param]
                                  selectorString:NSStringFromSelector(_cmd)
                                  concurrentMode:QINetworkConcurrentModeByParameter
                                    successBlock:successBlock
                                    failureBlock:failureBlock
                                 authFailedBlock:nil];
}

+ (void)scheduleListForDate:(NSString *)searchDate
               successBlock:(void (^)(NSDictionary *responseDictionary))successBlock
               failureBlock:(void (^)(NSError *error))failureBlock
            authFailedBlock:(void(^)(void(^)(void)))authFailedBlock {
    NSDictionary *param = @{
                            @"searchDate":searchDate?:[NSNull null],
                            };
    [[QINetwork manager] postReqestWithParameter:[QINetwork defaultParameterWithServiceParameter:param]
                                  selectorString:NSStringFromSelector(_cmd)
                                  concurrentMode:QINetworkConcurrentModeByParameter
                                    successBlock:successBlock
                                    failureBlock:failureBlock
                                 authFailedBlock:authFailedBlock];
}

#pragma mark - O365 일정조회
+ (void)o365ScheduleListForDate:(NSString *)searchDate
                          email:(NSString *)email
                   successBlock:(void (^)(NSDictionary *responseDictionary))successBlock
                   failureBlock:(void (^)(NSError *error))failureBlock
                authFailedBlock:(void(^)(void(^)(void)))authFailedBlock {
    NSDictionary *param = @{
                            @"searchDate":searchDate?:[NSNull null],
                            @"email":email ?: @"",
                            };
    [[QINetwork manager] postReqestWithParameter:[QINetwork defaultParameterWithServiceParameter:param]
                                  selectorString:NSStringFromSelector(_cmd)
                                  concurrentMode:QINetworkConcurrentModeByParameter
                                    successBlock:successBlock
                                    failureBlock:failureBlock
                                 authFailedBlock:authFailedBlock];
}

+ (void)saveO365ScheduleWithDictionary:(NSDictionary *)schedule
                      successBlock:(void (^)(NSDictionary *responseDictionary))successBlock
                      failureBlock:(void (^)(NSError *error))failureBlock
                   authFailedBlock:(void(^)(void(^)(void)))authFailedBlock {
    NSDictionary *param = schedule;
    [[QINetwork manager] postReqestWithParameter:[QINetwork defaultParameterWithServiceParameter:param]
                                  selectorString:NSStringFromSelector(_cmd)
                                  concurrentMode:QINetworkConcurrentModeByParameter
                                    successBlock:successBlock
                                    failureBlock:failureBlock
                                 authFailedBlock:authFailedBlock];
}

+ (void)scheduleDetailByID:(NSString *)scheduleID
              successBlock:(void (^)(NSDictionary *responseDictionary))successBlock
              failureBlock:(void (^)(NSError *error))failureBlock
           authFailedBlock:(void(^)(void(^)(void)))authFailedBlock {
    NSDictionary *param = @{
                            @"id":scheduleID?:[NSNull null],
                            };
    [[QINetwork manager] postReqestWithParameter:[QINetwork defaultParameterWithServiceParameter:param]
                                  selectorString:NSStringFromSelector(_cmd)
                                  concurrentMode:QINetworkConcurrentModeByParameter
                                    successBlock:successBlock
                                    failureBlock:failureBlock
                                 authFailedBlock:authFailedBlock];
}

+ (void)saveScheduleWithDictionary:(NSDictionary *)schedule
                      successBlock:(void (^)(NSDictionary *responseDictionary))successBlock
                      failureBlock:(void (^)(NSError *error))failureBlock
                   authFailedBlock:(void(^)(void(^)(void)))authFailedBlock {
    NSDictionary *param = schedule;
    [[QINetwork manager] postReqestWithParameter:[QINetwork defaultParameterWithServiceParameter:param]
                                  selectorString:NSStringFromSelector(_cmd)
                                  concurrentMode:QINetworkConcurrentModeByParameter
                                    successBlock:successBlock
                                    failureBlock:failureBlock
                                 authFailedBlock:authFailedBlock];
}

+ (void)removeScheduleByID:(NSString *)scheduleID
              successBlock:(void (^)(NSDictionary *responseDictionary))successBlock
              failureBlock:(void (^)(NSError *error))failureBlock
           authFailedBlock:(void(^)(void(^)(void)))authFailedBlock {
    NSDictionary *param = @{
                            @"id":scheduleID?:[NSNull null],
                            };
    [[QINetwork manager] postReqestWithParameter:[QINetwork defaultParameterWithServiceParameter:param]
                                  selectorString:NSStringFromSelector(_cmd)
                                  concurrentMode:QINetworkConcurrentModeByParameter
                                    successBlock:successBlock
                                    failureBlock:failureBlock
                                 authFailedBlock:authFailedBlock];
}

+ (void)taskListBetweenStartDate:(NSString *)startDate
                         endDate:(NSString *)endDate
                    successBlock:(void (^)(NSDictionary *responseDictionary))successBlock
                    failureBlock:(void (^)(NSError *error))failureBlock
                 authFailedBlock:(void(^)(void(^)(void)))authFailedBlock {
    NSDictionary *param = @{
                            @"startDt":startDate?:[NSNull null],
                            @"endDt":endDate?:[NSNull null],
                            };
    [[QINetwork manager] postReqestWithParameter:[QINetwork defaultParameterWithServiceParameter:param]
                                  selectorString:NSStringFromSelector(_cmd)
                                  concurrentMode:QINetworkConcurrentModeByParameter
                                    successBlock:successBlock
                                    failureBlock:failureBlock
                                 authFailedBlock:authFailedBlock];
}

+ (void)workInOutWithType:(NSString *)type
                 latitude:(double)latitude
                longitude:(double)longitude
                  address:(NSString *)address
             successBlock:(void (^)(NSDictionary *responseDictionary))successBlock
             failureBlock:(void (^)(NSError *error))failureBlock
          authFailedBlock:(void(^)(void(^)(void)))authFailedBlock {
    NSDictionary *param = @{
                            @"wrkInOutStatus":type?:[NSNull null],
                            @"latitudeNum":latitude ? @(latitude) : [NSNull null],
                            @"longitudeNum":longitude ? @(longitude) : [NSNull null],
                            @"addressInfo":address?:[NSNull null],
                            };
    [[QINetwork manager] postReqestWithParameter:[QINetwork defaultParameterWithServiceParameter:param]
                                  selectorString:NSStringFromSelector(_cmd)
                                  concurrentMode:QINetworkConcurrentModeByParameter
                                    successBlock:successBlock
                                    failureBlock:failureBlock
                                 authFailedBlock:authFailedBlock];
}

@end
