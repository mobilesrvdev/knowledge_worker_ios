//
//  Navigator.m
//  management
//
//  Created by Quintet on 27/06/2018.
//  Copyright © 2018 Pulmuone. All rights reserved.
//

#import "Navigator.h"
#import "LoginViewController.h"
#import "ScheduleDetailViewController.h"
#import "ScheduleListViewController.h"
#import "QINetwork+Pulmuone.h"
#import "SVProgressHUD.h"

@implementation Navigator

+ (void)toMainViewController {
    UIViewController *viewController = VIEW_CONTROLLER_MAIN;
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:viewController];
    [navController setModalPresentationStyle: UIModalPresentationFullScreen];
    dispatch_async(dispatch_get_main_queue(), ^{
        UIWindow *rootWindow = [[[UIApplication sharedApplication] delegate] window];
        [rootWindow layoutIfNeeded];
        [navController.view layoutIfNeeded];
        [UIView transitionWithView:rootWindow duration:0.35f options:UIViewAnimationOptionAllowAnimatedContent|UIViewAnimationOptionTransitionCrossDissolve animations:^{
            [[[UIApplication sharedApplication] keyWindow] setRootViewController:navController];
            [rootWindow layoutIfNeeded];
        } completion:^(BOOL finished) {
            [rootWindow layoutIfNeeded];
        }];
    });
}

+ (void)toMainViewControllerWithoutUIAnimation {
    UIViewController *viewController = topMostController();
    //NSArray *viewControllers = [viewController.navigationController viewControllers];
    //DDLogDebug(@"viewControllers.count: %lu", (unsigned long)viewControllers.count);
    //[viewController.navigationController popToViewController:viewControllers[viewControllers.count-3] animated:YES];
    [viewController.navigationController popToRootViewControllerAnimated:NO];
}

+ (void)toScheduleListViewController:(Boolean)isO365 completion:(void (^)(void))completion {
    UIViewController *viewController = topMostController();
    if (canPushFromViewController(viewController)) {
        ScheduleListViewController *listViewController =  VIEW_CONTROLLER_SCHEDULE_LIST;
        listViewController.isO365 = isO365;
        [CATransaction begin];
        [CATransaction setCompletionBlock:completion];
        [viewController.navigationController setModalPresentationStyle: UIModalPresentationFullScreen];
        [viewController.navigationController pushViewController:listViewController animated:YES];
        [CATransaction commit];
    }
}

+ (void)toScheduleListViewController:(Boolean)isO365 date:(NSDate *)date {
    UIViewController *viewController = topMostController();
    if (canPushFromViewController(viewController)) {
        ScheduleListViewController *listViewController =  VIEW_CONTROLLER_SCHEDULE_LIST;
        listViewController.isO365 = isO365;
        listViewController.targetDate = (date != nil ? date : [NSDate date]);
        [viewController.navigationController setModalPresentationStyle: UIModalPresentationFullScreen];
        [viewController.navigationController pushViewController:listViewController animated:YES];
    }
}

+ (void)toScheduleDetailViewControllerWithDate:(NSDate *)date {
    UIViewController *viewController = topMostController();
    if (canPushFromViewController(viewController)) {
        ScheduleDetailViewController *detailViewController =  VIEW_CONTROLLER_SCHEDULE_DETAIL;
        detailViewController.initialDate = date;
        [viewController.navigationController setModalPresentationStyle: UIModalPresentationFullScreen];
        [viewController.navigationController pushViewController:detailViewController animated:YES];
    }
}

+ (void)toScheduleDetailViewControllerWithID:(NSString *)scheduleID {
    UIViewController *viewController = topMostController();
    if (canPushFromViewController(viewController)) {
        ScheduleDetailViewController *detailViewController =  VIEW_CONTROLLER_SCHEDULE_DETAIL;
        detailViewController.scheduleID = scheduleID;
        [viewController.navigationController setModalPresentationStyle: UIModalPresentationFullScreen];
        [viewController.navigationController pushViewController:detailViewController animated:YES];
    }
}

+ (void)modalPresentLoginViewControllerWithCompletionBlock:(void(^)(void))completionBlock {
    UIViewController *viewController = topMostController();
    //UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:viewController];
    if (![viewController isKindOfClass:[LoginViewController class]]) {
        LoginViewController *loginViewController = VIEW_CONTROLLER_LOGIN;
        [loginViewController setModalPresentationStyle: UIModalPresentationFullScreen];
        loginViewController.completionBlock = completionBlock;
        dispatch_async(dispatch_get_main_queue(), ^{
            //UIWindow *window = (UIWindow *)[[UIApplication sharedApplication].windows firstObject];
            //window.rootViewController = loginViewController;
            [viewController presentViewController:loginViewController animated:YES completion:^{
               
//                if ([viewController.navigationController.viewControllers count] > 0) {
//                    [viewController.navigationController setViewControllers:@[loginViewController]];
//                }
            }];
        });
    }
}

bool _isToMainViewController = false;
+ (void)toMainViewControllerAfterRetryLogin {
    _isToMainViewController = false;
    [SVProgressHUD showErrorWithStatus:@"앱실행 후 세션 시간이 경과하였습니다. 초기 화면으로 이동합니다."];
    void (^loginThenMain)(NSString *) = ^(NSString *message){
        dispatch_async(dispatch_get_main_queue(), ^{
            saveUserID(nil);
            saveUserPassword(nil);
            if ([message isKindOfClass:[NSString class]] && [message length]) {
                [SVProgressHUD showErrorWithStatus:message];
            }
            [Navigator modalPresentLoginViewControllerWithCompletionBlock:^{
                // MARK: - 반복 실행 방지
                if (!_isToMainViewController) {
                    [Navigator toMainViewController];
                    _isToMainViewController = true;
                }
            }];
        });
    };
    [GlobalVariables singleton].email = appUserID();
    if (appUserID() && appUserPassword()) {
        [QINetwork loginWithUserID:appUserID() password:appUserPassword() successBlock:^(NSDictionary *responseDictionary) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([QINetwork manager].shouldSucceed(responseDictionary)) {
                    [[QIContainer sharedContainer] setUserInfo:@{@"userInfo":responseDictionary[@"userInfo"], @"workInOutList":responseDictionary[@"workInOutList"]}];
                    // MARK: - 반복 실행 방지
                    if (!_isToMainViewController) {
                        [Navigator toMainViewController];
                        _isToMainViewController = true;
                    }
                } else {
                    DDLogWarn(@"[Intro] Auto login failed with success false : %@", responseDictionary);
                    loginThenMain(responseDictionary[@"message"]);
                }
            });
        } failureBlock:^(NSError *error) {
            DDLogWarn(@"[Intro] Auto login failed with error : %@", error);
            loginThenMain(error.localizedDescription);
        }];
    } else {
        loginThenMain(nil);
    }
}

@end
