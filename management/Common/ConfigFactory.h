//
//  ConfigFactory.h
//  management
//
//  Created by Quintet on 25/06/2018.
//  Copyright © 2018 Pulmuone. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConfigFactory : NSObject

+ (void)configWithLaunchOptions:(NSDictionary *)launchOptions;

@end
