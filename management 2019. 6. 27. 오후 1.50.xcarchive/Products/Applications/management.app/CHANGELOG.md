# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).


### 1.0.0 - 2018-08-28
- Product release.
- Changed:
    - Changed initial time on creation of schedule. 


### 0.5.6 - 2018-08-09
- Fixed:
    - Fixed placeholder of id text field to employee number from email.  


### 0.5.5 - 2018-08-08
- Fixed:
    - Fixed wrong context path. (Changed 'pulmuone' to '') 


### 0.5.4 - 2018-08-08
- Fixed:
    - Fixed typo in manifest plist.


### 0.5.3 - 2018-08-08
- Changed:
    - Changed the target server.


### 0.5.2 - 2018-07-17
- Changed:
    - Changed logout button time label font size.


### 0.5.1 - 2018-07-13
- Removed:
    - Removed unnecessary codes.


### 0.5.0 - 2018-07-12
- Changed:
    - Changed action sheet to be more natural style with another 3rd party lib.


### 0.4.2 - 2018-07-11
- Changed:
    - Changed action sheet tint color back to default.


### 0.4.1 - 2018-07-10
- Fixed:
    - Fixed auto updating not working when its newest.


### 0.4.0 - 2018-07-10
- Added:
    - Added confirm action sheets.
    - Added info popups on validation.
- Changed:
    - Changed date format for displaying.
- Fixed:
    - Fixed broken UI on low resolution.
    - Fixed too fast dismissing location alert.
    - Fixed work hour displaying.
    - Fixed auto updating to wait user input.


### 0.3.3 - 2018-07-09
- Fixed:
    - Fixed name in main view sometimes disappearing issue.
    - Fixed some UI mis-typed strings.
    - Fixed crash when there is null schedule with success response.


### 0.3.2 - 2018-07-09
- Fixed:
    - Fixed app title under Korean language by adding explicitly.


### 0.3.1 - 2018-07-09
- Fixed:
    - Fixed margins on main view.
    - Fixed app title under Korean language.


### 0.3.0 - 2018-07-07
- Fixed:
    - Fixed wrong date on loading schedule.
    - Fixed squeezed buttons on smaller device.


### 0.2.0 - 2018-07-06
- Fixed:
    - Fixed some other UI gliches. 
- Added:
    - Added open source license notice.
    - Added fabric / crashlytics.
- Removed:
    - Removed unnecessary resources.


### 0.1.1 - 2018-07-05
- Fixed:
  - Fixed UI gliches.
  - Fixed wrong appending on too fast requests.


### 0.1.0 - 2018-07-04
- Initial Release

